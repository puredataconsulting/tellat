﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Tellat2.Azure" generation="1" functional="0" release="0" Id="06193d8a-ce2d-497f-8a5a-eccc39d7db15" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Tellat2.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="Tellat2:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/LB:Tellat2:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="Tellat2:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/MapTellat2:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Tellat2Instances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/MapTellat2Instances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:Tellat2:Endpoint1">
          <toPorts>
            <inPortMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapTellat2:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapTellat2Instances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2Instances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="Tellat2" generation="1" functional="0" release="0" software="C:\Development\Tellat2\Tellat2.Azure\csx\Release\roles\Tellat2" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="1792" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Tellat2&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Tellat2&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2Instances" />
            <sCSPolicyUpdateDomainMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2UpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2FaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="Tellat2UpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="Tellat2FaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="Tellat2Instances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="9f27dfe9-201c-4e3a-90f6-323ba4dea504" ref="Microsoft.RedDog.Contract\ServiceContract\Tellat2.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="35c0cd3f-d0bb-4d45-8edf-7fa9cab3d26e" ref="Microsoft.RedDog.Contract\Interface\Tellat2:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/Tellat2.Azure/Tellat2.AzureGroup/Tellat2:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>