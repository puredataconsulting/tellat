﻿// Update a container via AJAX based on a user drop-down selection
function updateSelection(container, url, data) {
    $.get(url, data).success(function (response) {
        container.replaceWith(response);
    });
}