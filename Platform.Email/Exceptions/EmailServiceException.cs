﻿using System;
using System.Runtime.Serialization;

namespace Platform.Email.Exceptions
{
    /// <summary>
    /// A set of custom exceptions relating to the email service classes
    /// </summary>
    [Serializable]
    public class EmailServiceException : Exception
    {
        public EmailServiceException() { }

        public EmailServiceException(string message)
            : base(message) { }

        public EmailServiceException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public EmailServiceException(string message, Exception innerException)
            : base(message, innerException) { }

        public EmailServiceException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected EmailServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
