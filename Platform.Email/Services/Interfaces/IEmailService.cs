﻿using Platform.Email.ViewModels.Base;

namespace Platform.Email.Services.Interfaces
{
    /// <summary>
    /// Wraps an external email sending service
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// Send an email using the underlying provider and a specified template
        /// </summary>
        bool SendEmail(string emailaddress, string subject, string templatePath, BaseEmailViewModel viewModel);
    }
}