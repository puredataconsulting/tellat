﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Web;
using Platform.Email.Exceptions;
using Platform.Email.Services.Interfaces;
using Platform.Email.Utils;
using Platform.Email.ViewModels.Base;
using RazorEngine;
using SendGrid;
using SendGrid.Transport;
using Microsoft.AspNet.Identity;
using Framework2.Core.Models.Base;

namespace Platform.Email.Services
{
    /// <summary>
    /// Wraps an email sending service, in this case Email
    /// Following http://www.novolocus.com/2013/12/20/sending-email-from-azure-with-sendgrid/
    /// </summary>
    public class EmailService : IEmailService, IIdentityMessageService
    {
        // Email SMTP API transport
        private readonly SMTP _transportSmtp;

        /// <summary>
        /// Default constructor
        /// </summary>
        public EmailService()
        {
            // Create credentials, specifying the username and password.
            var credentials = new NetworkCredential(Config.Email.Username, Config.Email.Password);
            _transportSmtp = SMTP.GetInstance(credentials);
        }

        /// <summary>
        /// Send an email using the underlying provider and a specified template
        /// </summary>
        public bool SendEmail(string emailaddress, string subject, string templatePath, BaseEmailViewModel viewModel)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(emailaddress))
            {
                throw new EmailServiceException("The provided email address is empty or invalid");
            }

            if (string.IsNullOrEmpty(subject))
            {
                throw new EmailServiceException("The provided subject line is empty or invalid");
            }

            // Resolve the complete path to the file on the server
            var serverPath = HttpContext.Current.Server.MapPath(templatePath);

            // Does the file exist?
            if (File.Exists(serverPath) == false)
            {
                throw new EmailServiceException("The requested template does not exist");
            }

            // Use Razor to parse the template
            var content = Template.ReadTemplate(serverPath);
            var htmlString = Razor.Parse(content, viewModel);

            // Send the email via sendgrid
            var message = Mail.GetInstance(); 
            
            try
            {
                // Construct the message
                message.AddTo(emailaddress);
                message.From = new MailAddress(Config.Email.FromAddress, Config.Email.FromName);
                message.Subject = subject;
                message.Html = htmlString;

                // Send the message
                _transportSmtp.Deliver(message);
            }
            catch (Exception ex)
            {
                throw new EmailServiceException("Unable to send the email", ex.InnerException);
            }

            return true;
        }

        public static string FillTemplateFromModel(string templatePath, BaseEmailViewModel viewModel)
        {
            // Resolve the complete path to the file on the server
            var serverPath = HttpContext.Current.Server.MapPath(templatePath);

            // Does the file exist?
            if (File.Exists(serverPath) == false)
            {
                throw new EmailServiceException("The requested template does not exist");
            }

            // Use Razor to parse the template
            var content = Template.ReadTemplate(serverPath);
            return Razor.Parse<BaseEmailViewModel>(content, viewModel);

        }

        public async System.Threading.Tasks.Task SendAsync(IdentityMessage identityMessage)
        {

            // Validate the arguments
            if (string.IsNullOrEmpty(identityMessage.Destination))
            {
                throw new EmailServiceException("The provided email address is empty or invalid");
            }

            if (string.IsNullOrEmpty(identityMessage.Subject))
            {
                throw new EmailServiceException("The provided subject line is empty or invalid");
            }


            // Send the email via sendgrid
            var message = Mail.GetInstance();
            try
            {
                // Construct the message
                message.AddTo(identityMessage.Destination);
                message.From = new MailAddress(Config.Email.FromAddress, Config.Email.FromName);
                message.Subject = identityMessage.Subject;
                message.Html = identityMessage.Body;

                // Send the message
                _transportSmtp.Deliver(message);
            }
            catch (Exception ex)
            {
                throw new EmailServiceException("Unable to send the email", ex.InnerException);
            }

            return;
        }
    }
}
