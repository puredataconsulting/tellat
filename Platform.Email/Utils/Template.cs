﻿using System;
using System.IO;

namespace Platform.Email.Utils
{
    public static class Template
    {
        public static string ReadTemplate(string fullServerPath)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(fullServerPath))
            {
                throw new ArgumentException("The provided template path is empty or invalid");
            }

            // Does the template exist?
            if (File.Exists(fullServerPath) == false)
            {
                throw new FileNotFoundException("The provided template path does not exist");
            }

            // Read the template content
            string content;
            using (var reader = new StreamReader(fullServerPath))
            {
                content = reader.ReadToEnd();
            }

            return content;
        }
    }
}
