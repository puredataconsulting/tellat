﻿namespace Platform.Email.Config
{
    /// <summary>
    /// Wrappers all the email specific configurations settings.  We're currently using SendGrid to
    /// handle all email sending as an Azure webservice has no concept of an SMTP service.  More
    /// information can be found at http://sendgrid.com/
    /// </summary>
    public static class Email
    {
        /// <summary>
        /// This is the username of the account as registered with the SendGrid. It's
        /// created as a part of the SendGrid registration process
        /// </summary>
        public static string Username
        {
            get
            {
                return "azure_a701048f41dd8a4a3db35029261071c9@azure.com";
            }
        }

        /// <summary>
        /// This is the password of the account as registered by SendGrid.  It's
        /// ceated as a part of the SendGrid registration process
        /// </summary>
        public static string Password
        {
            get
            {
                return "3rR6wf3UU3Aoqa3";
            }
        }

        public static string SmtpServer
        {
            get
            {
                return "smtp.sendgrid.net";
            }
        }

        /// <summary>
        /// This is the from address used for every email sent
        /// </summary>
        public static string FromAddress
        {
            get
            {
                return "no-reply@tellat.com";
            }
        }

        /// <summary>
        /// This is the from name used for every email sent
        /// </summary>
        public static string FromName
        {
            get
            {
                return "Tellat Administrator";
            }
        }
    }
}