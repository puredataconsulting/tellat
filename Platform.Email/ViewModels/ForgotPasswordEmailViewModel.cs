﻿using Framework2.Core.Models;
using Framework2.Core.Models.Core;
using Platform.Email.ViewModels.Base;

namespace Platform.Email.ViewModels
{
    /// <summary>
    /// The view model for the registration email template
    /// </summary>
    public class ForgotPasswordEmailViewModel : BaseEmailViewModel
    {
        public ApplicationUser User { get; set; }

        public string Link { get; set; }
    }
}
