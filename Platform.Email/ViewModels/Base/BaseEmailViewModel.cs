﻿
namespace Platform.Email.ViewModels.Base
{
    /// <summary>
    /// This is the base view model for all email templates.  Override this to create template
    /// specific email view models
    /// </summary>
    public abstract class BaseEmailViewModel
    {
        public string Username { get; set; }

        public string EmailAddress { get; set; }
    }
}
