﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Framework2.Utils
{
    public class ImageProcessing
    {
        /// <summary>
        /// EXIF orientations
        /// </summary>
        private const int OrientationId = 0x0112;
        public enum ExifOrientations : byte
        {
            Unknown = 0,
            TopLeft = 1,
            TopRight = 2,
            BottomRight = 3,
            BottomLeft = 4,
            LeftTop = 5,
            RightTop = 6,
            RightBottom = 7,
            LeftBottom = 8,
        }

        /// <summary>
        /// Resizes an Media whilst preserving aspect ratio
        /// THE RETURNED IMAGE MUST BE EXPLICITLY DISPOSED WHEN NO LONGER REQUIRED
        /// </summary>
        public static Image Resize(Image image, Size size, bool preserveAspectRatio = true)
        {
            int newWidth;
            int newHeight;

            if (preserveAspectRatio)
            {
                var originalWidth = image.Width;
                var originalHeight = image.Height;
                var percentWidth = size.Width / (float)originalWidth;
                var percentHeight = size.Height / (float)originalHeight;
                var percent = percentHeight < percentWidth ? percentHeight : percentWidth;
                newWidth = (int)(originalWidth * percent);
                newHeight = (int)(originalHeight * percent);
            }
            else
            {
                newWidth = size.Width;
                newHeight = size.Height;
            }

            Image newImage = new Bitmap(newWidth, newHeight);
            using (var graphicsHandle = Graphics.FromImage(newImage))
            {
                graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
            }

            return newImage;
        }

        /// <summary>
        /// Get an Media's EXIF rotation
        /// </summary>
        public static ExifOrientations GetExifOrientation(Image image)
        {
            // Get the index of the orientation property.
            var orientation_index = Array.IndexOf(image.PropertyIdList, OrientationId);

            // If there is no such property, return Unknown.
            if (orientation_index < 0)
            {
                return ExifOrientations.Unknown;
            }

            // Return the orientation value.
            return (ExifOrientations)image.GetPropertyItem(OrientationId).Value[0];
        }

        /// <summary>
        /// Set the Media's EXIF orientation
        /// </summary>
        public static void SetExifOrientation(Image img, ExifOrientations orientation)
        {
            // Get the index of the orientation property.
            var orientation_index = Array.IndexOf(img.PropertyIdList, OrientationId);

            // If there is no such property, do nothing.
            if (orientation_index < 0)
            {
                return;
            }

            // Set the orientation value.
            var item = img.GetPropertyItem(OrientationId);
            item.Value[0] = (byte)orientation;
            img.SetPropertyItem(item);
        }

        /// <summary>
        /// Correctly orient an Media based on it's EXIF data.  Once we've corrected it
        /// write the correct value back to the metadata.
        /// </summary>
        /// <param name="img"></param>
        public static void OrientByExif(Image img)
        {
            // Get the Media's orientation.
            var orientation = GetExifOrientation(img);

            // Orient the Media.
            switch (orientation)
            {
                case ExifOrientations.Unknown:
                case ExifOrientations.TopLeft:
                    break;
                case ExifOrientations.TopRight:
                    img.RotateFlip(RotateFlipType.RotateNoneFlipX);
                    break;
                case ExifOrientations.BottomRight:
                    img.RotateFlip(RotateFlipType.Rotate180FlipNone);
                    break;
                case ExifOrientations.BottomLeft:
                    img.RotateFlip(RotateFlipType.RotateNoneFlipY);
                    break;
                case ExifOrientations.LeftTop:
                    img.RotateFlip(RotateFlipType.Rotate90FlipX);
                    break;
                case ExifOrientations.RightTop:
                    img.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    break;
                case ExifOrientations.RightBottom:
                    img.RotateFlip(RotateFlipType.Rotate90FlipY);
                    break;
                case ExifOrientations.LeftBottom:
                    img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    break;
            }

            // Set the Media's orientation to TopLeft.
            SetExifOrientation(img, ExifOrientations.TopLeft);
        }
    }
}
