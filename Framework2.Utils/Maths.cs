﻿
namespace Framework2.Utils
{
    public static class Numbers
    {
        /// <summary>
        /// Clamp an integer, ensure the input is between the provided range
        /// </summary>
        public static int Clamp(int value, int min, int max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
    }
}
