﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework2.Utils
{
    /// <summary>
    /// Used to provide pagaination behaviour to our views. 
    /// ** Important ** note that page indexs are 1 NOT 0 based
    /// </summary>
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public PaginatedList(IQueryable<T> source, int pageIndex, int pageSize)
        {
            PageSize = pageSize;

            // Calculte the number of pages
            TotalCount = source.Count();
            TotalPages = (int) Math.Ceiling(TotalCount/(double) PageSize);

            // Ensure our page number is within range
            PageIndex = Numbers.Clamp(pageIndex, 1, TotalPages);

            AddRange(source.Skip((PageIndex - 1) * PageSize).Take(PageSize));
        }

        /// <summary>
        /// Do we have a previous page?
        /// </summary>
        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        /// <summary>
        /// Do we have a next page?
        /// </summary>
        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }
    }
}
