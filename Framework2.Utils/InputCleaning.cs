﻿using System.Text.RegularExpressions;

namespace Framework2.Utils
{
    /// <summary>
    /// Methods related to stripping strings of HTML markup
    /// </summary>
    public static class InputCleaning
    {
        /// <summary>
        /// Compiled regular expression to remove all HTML tags
        /// </summary>
        static readonly Regex HtmlRegex = new Regex(@"<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression to remove all non aplha-numeric characters
        /// </summary>
        static readonly Regex AlphaNumericRegex = new Regex(@"[^a-zA-Z0-9 -]", RegexOptions.Compiled);

        /// <summary>
        /// Compiled regular expression to replace all whitespace with a single space
        /// </summary>
        private static readonly Regex WhiteSpaceRegex = new Regex(@"\s+", RegexOptions.Compiled);

        /// <summary>
        /// Remove all non alpha-numeric characters with a compiled Regex
        /// </summary>
        public static string StripNonAplhaNumericRegex(string source)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : AlphaNumericRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Replace all runs of white space with a single space
        /// </summary>
        public static string ReplaceWhiteSpace(string source)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : WhiteSpaceRegex.Replace(source, " ");
        }

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripHtmlRegex(string source)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : HtmlRegex.Replace(source, string.Empty);
        }
    }
}
