﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace Framework2.Utils
{
    /// <summary>
    /// Methods relating to passwords, salting, hashing and cryptography
    /// </summary>
    public static class Cryptography
    {
        private static readonly RNGCryptoServiceProvider CryptoProvider = new RNGCryptoServiceProvider();

        /// <summary>
        /// Convert a string to a byte[]
        /// </summary>
        private static byte[] GetBytes(string str)
        {
            var bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

            return bytes;
        }

        /// <summary>
        /// Generates a 64bit random salt via the RNGCryptoServiceProvider
        /// </summary>
        public static string GenerateSalt()
        {
            var bytes = new byte[64];
            CryptoProvider.GetBytes(bytes);

            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Generates a new GUID for use as a session token
        /// </summary>
        public static string GenerateSessionToken()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Hash a password with a salt value
        /// </summary>
        public static string HashPassword(string password, string salt)
        {
            var sha256 = SHA256.Create();
            var bytes = sha256.ComputeHash(GetBytes(password + salt));

            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Generate a new randon 6 character password 
        /// </summary>
        /// <returns></returns>
        public static string GenerateAplhaNumericString(int length)
        {
            const string validCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var random = new Random();
            var result = new string(
                Enumerable.Repeat(validCharacters, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }
    }
}