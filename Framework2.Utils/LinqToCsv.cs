﻿using System.Linq;
using System.Text;

namespace Framework2.Utils
{
    /// <summary>
    /// Methods relating to the converstion of queries in to comma seperated strings
    /// </summary>
    public static class LinqToCsv
    {
        /// <summary>
        /// Formats an individual line
        /// </summary>
        private static string FormatLine(string line)
        {
            return string.Format("\"{0}\"", line.Replace("\"", "\\\""));
        }

        /// <summary>
        /// Returns an IQueryable as a string in CSV format
        /// </summary>
        public static string QueryableToCsv(IQueryable items)
        {
            var stringBuilder = new StringBuilder();
            var properties = items.ElementType.GetProperties();

            // Build the header row
            var header = string.Join("|", properties.Select(p => p.Name).ToArray());
            stringBuilder.AppendLine(FormatLine(header));

            // Build the data rows
            foreach (var item in items)
            {
                var line = string.Join("|", properties.Select(p => p.GetValue(item, null)).ToArray());
                stringBuilder.AppendLine(FormatLine(line));
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Returns IQueryable as a byte[] in CSV format
        /// </summary>
        public static byte[] QueryableToByteArray(IQueryable items)
        {
            return Encoding.UTF8.GetBytes(QueryableToCsv(items));
        }
    }
}


