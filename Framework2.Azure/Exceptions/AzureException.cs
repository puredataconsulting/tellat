﻿using System;
using System.Runtime.Serialization;

namespace Framework2.Azure.Exceptions
{
    /// <summary>
    /// A set of custom exceptions relating to the Azure service layer
    /// </summary>
    [Serializable]
    public class AzureException : Exception
    {
        public AzureException() { }

        public AzureException(string message)
            : base(message) { }

        public AzureException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public AzureException(string message, Exception innerException)
            : base(message, innerException) { }

        public AzureException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected AzureException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
