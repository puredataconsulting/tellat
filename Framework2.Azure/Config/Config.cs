﻿using System.Collections.Generic;
using System.Configuration;
namespace Framework2.Azure.Config
{
    /// <summary>
    /// Enumeration of Azure container names, one for each CategoryModel of image
    /// </summary>
    public enum AzureContainers
    {
        Images = 0,
        Videos = 1,
        Teams = 2
    }

    /// <summary>
    /// Wrappers all the Azure specific configurations strings
    /// </summary>
    public static class AzureConfig
    {
        // Container names
        private const string _imagesContainerName                   = "images";
        private const string _videosContainerName                   = "videos";
        private const string _contentContainerName                  = "content";
        private const string _audiosContainerName                   = "audios";
        private const string _documentsContainerName                = "documents";

        #region Formats

        // List of supported Media file formats
        private static string[] _supportedImageFormats = { "jpg", "png" };

        // List of supported video file formats
        private static string[] _supportedVideoFormats = { "mp4", "m4v", "wmv", "mov", "avi" };

        /// <summary>
        /// Returns a list of supported Media file formats
        /// </summary>
        public static string[] SupportedImageFormats
        {
            get { return _supportedImageFormats; }
        }

        /// <summary>
        /// Returns a list of supported video file formats
        /// </summary>
        public static List<string> SupportedVideoFormats
        {
            get { return new List<string>(_supportedVideoFormats); }
        }
        #endregion

        #region Azure Blob Storage

        /// <summary>
        /// The complete storage account connection string
        /// </summary>
        static public string StorageConnectionString
        {
            get { return string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", StorageAccountName, StorageAccountPrimaryAccessKey); }
        }

        static private string StorageAccountUri
        {
            get
            {
                return ConfigurationManager.AppSettings["storageAccountUri"];
            }
        }

        /// <summary>
        /// Gets the storage account name
        /// </summary>
        static public string StorageAccountName
        {
            get 
            { 
                return ConfigurationManager.AppSettings["storageAccountName"]; 
            }
        }

        static public string MediaServicesAccountUri
        {
            get
            {
                return ConfigurationManager.AppSettings["mediaServicesAccountUri"];
            }
        }

        /// <summary>
        /// Gets the storage account primary access key
        /// </summary>
        static public string StorageAccountPrimaryAccessKey
        {
            get { 
                return ConfigurationManager.AppSettings["storageAccountPrimaryAccessKey"]; 
            }
        }

        /// <summary>
        /// Gets the Media Services account name
        /// </summary>
        public static string MediaServicesAccountName
        {
            get
            {
                return ConfigurationManager.AppSettings["mediaServicesAccountName"];
            }
        }

        /// <summary>
        /// Gets the Media Services account primary access key
        /// </summary>
        public static string MediaServicesAccountPrimaryAccessKey
        {
            get
            {
                return ConfigurationManager.AppSettings["mediaServicesAccountPrimaryAccessKey"];
            }
        }

        /// <summary>
        /// Gets the images container name, this is the Azure container used for all images
        /// </summary>
        static public string ImagesContainerUri
        {
            get { return string.Format("{0}/{1}", StorageAccountUri, _imagesContainerName); }
        }

        /// <summary>
        /// Gets the videos container name, this is the Azure container used for simple videos
        /// </summary>
        static public string VideosContainerUri
        {
            get { return string.Format("{0}/{1}", StorageAccountUri, _videosContainerName); }
        }

        /// <summary>
        /// Gets the videos container name, this is the Azure container used for simple videos
        /// </summary>
        static public string AudiosContainerUri
        {
            get { return string.Format("{0}/{1}", StorageAccountUri, _audiosContainerName); }
        }

        /// <summary>
        /// Gets the videos container name, this is the Azure container used for simple videos
        /// </summary>
        static public string DocumentsContainerUri
        {
            get { return string.Format("{0}/{1}", StorageAccountUri, _documentsContainerName); }
        }

        /// <summary>
        /// Gets the contents container name, this is the Azure container used for all static site content
        /// </summary>
        static public string ContentContainerUri
        {
            get { return string.Format("{0}/{1}", StorageAccountUri, _contentContainerName); }
        }

        static public string VideosContainerName
        {
            get
            {
                return _videosContainerName;
            }
        }

        static public string AudiosContainerName
        {
            get
            {
                return _audiosContainerName;
            }
        }

        static public string DocumentsContainerName
        {
            get
            {
                return _documentsContainerName;
            }
        }

        static public string ImagesContainerName
        {
            get
            {
                return _imagesContainerName;
            }
        }

        static public string ContentContainerName
        {
            get
            {
                return _contentContainerName;
            }
        }
        #endregion
    }
}