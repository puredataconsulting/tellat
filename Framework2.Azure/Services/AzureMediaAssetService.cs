﻿using Framework2.Azure.Config;
using Framework2.Azure.Services.Interfaces;
using Microsoft.WindowsAzure.MediaServices.Client;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Framework2.Azure.Services
{
    public class AzureMediaAssetService : IAzureMediaAssetService
    {
        private CloudMediaContext _context = null;

        // Number of days AFTER WHICH THE ASSETS WILL NEED A NEW LOCATOR if they're to continue to be readable
        private readonly int _defaultLockoutDays = 730;

        public AzureMediaAssetService()
        {
            try
            {
                _context = new CloudMediaContext(AzureConfig.MediaServicesAccountName, AzureConfig.MediaServicesAccountPrimaryAccessKey);
            }
            catch (Exception ex)
            {
                throw new Exception("AzureMediaAssetService failed to create context", ex);
            }
        }

        private IMediaProcessor GetLatestMediaProcessorByName(string mediaProcessorName)
        {
            // The possible strings that can be passed into the 
            // method for the mediaProcessor parameter:
            //   Windows Azure Media Encoder
            //   Windows Azure Media Packager
            //   Windows Azure Media Encryptor
            //   Storage Decryption

            var processor = _context.MediaProcessors.Where(p => p.Name == mediaProcessorName).
                ToList().OrderBy(p => new Version(p.Version)).LastOrDefault();

            if (processor == null)
                throw new ArgumentException(string.Format("Unknown media processor", mediaProcessorName));

            return processor;
        }

        private IJob GetJob(string jobId)
        {
            // Use a Linq select query to get an updated 
            // reference by Id. 
            var jobInstance =
                from j in _context.Jobs
                where j.Id == jobId
                select j;
            // Return the job reference as an Ijob. 
            IJob job = jobInstance.FirstOrDefault();

            return job;
        }

        public string CopyMediaAssetFromBlob(CloudBlobClient cloudBlobClient, AzureContainers containerName, string uniqueFilename, out IAsset returnedAsset)
        {
            try
            {
                _context = new CloudMediaContext(AzureConfig.MediaServicesAccountName, AzureConfig.MediaServicesAccountPrimaryAccessKey);
                var mediaBlobContainer = cloudBlobClient.GetContainerReference(containerName.ToString().ToLower());
                mediaBlobContainer.CreateIfNotExists();

                // Create a new asset.
                IAsset asset = _context.Assets.Create("mediaAsset_" + uniqueFilename, AssetCreationOptions.None);
                IAccessPolicy writePolicy = _context.AccessPolicies.Create("writePolicy", TimeSpan.FromMinutes(120), AccessPermissions.Write);
                ILocator destinationLocator = _context.Locators.CreateLocator(LocatorType.Sas, asset, writePolicy);

                // Get the asset container URI and copy blobs from mediaContainer to assetContainer.
                Uri uploadUri = new Uri(destinationLocator.Path);
                string assetContainerName = uploadUri.Segments[1];
                CloudBlobContainer assetContainer = cloudBlobClient.GetContainerReference(assetContainerName);

                var sourceCloudBlob = mediaBlobContainer.GetBlockBlobReference(uniqueFilename);
                sourceCloudBlob.FetchAttributes();

                if (sourceCloudBlob.Properties.Length > 0)
                {
                    IAssetFile assetFile = asset.AssetFiles.Create(uniqueFilename);
                    var destinationBlob = assetContainer.GetBlockBlobReference(uniqueFilename);

                    destinationBlob.DeleteIfExists();
                    destinationBlob.StartCopyFromBlob(sourceCloudBlob);

                    destinationBlob.FetchAttributes();
                    if (sourceCloudBlob.Properties.Length != destinationBlob.Properties.Length)
                        Console.WriteLine("Failed to copy");
                    else
                    {
                        //TODO TEMPORARY - needs to be smarter. Note that we no longer stream directly from this URL so this may now
                        // be redundant
                        destinationBlob.Properties.ContentType = "video/mp4";
                        destinationBlob.SetProperties();
                    }
                }

                destinationLocator.Delete();
                writePolicy.Delete();

                // Refresh the asset.
                asset = _context.Assets.Where(a => a.Id == asset.Id).FirstOrDefault();

                //At this point, you can create a job using your asset.

                returnedAsset = asset;

                // Now we create an On Demand origin locator http://msdn.microsoft.com/en-us/library/windowsazure/jj129575.aspx
                var streamingAssetId = asset.Id;
                var daysForWhichStreamingUrlIsActive = _defaultLockoutDays;
                var streamingAsset = _context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
                var accessPolicy = _context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                         AccessPermissions.Read | AccessPermissions.List);
                string streamingUrl = string.Empty;
                var assetFiles = streamingAsset.AssetFiles.ToList();
                var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("m3u8-aapl.ism")).FirstOrDefault();
                if (streamingAssetFile != null)
                {
                    var locator = _context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
                    Uri hlsUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest(format=m3u8-aapl)");
                    streamingUrl = hlsUri.ToString();
                }
                streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".ism")).FirstOrDefault();
                if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
                {
                    var locator = _context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
                    Uri smoothUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest");
                    streamingUrl = smoothUri.ToString();
                }
                if (streamingUrl.Length == 0)
                {
                    foreach (string format in AzureConfig.SupportedVideoFormats)
                    {
                        streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(format)).FirstOrDefault();
                        if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
                        {
                            var locator = _context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                            var uri = new UriBuilder(locator.Path);
                            uri.Path += "/" + streamingAssetFile.Name;
                            streamingUrl = uri.ToString();
                            break;
                        }
                    }
                }
                return streamingUrl;
            }
            catch (Exception ex)
            {
                throw new Exception("AzureMediaAssetService::CopyMediaAssetFromBlob", ex);
            }

        }

        /// <summary>
        /// Create a thumbnail
        /// http://msdn.microsoft.com/en-us/library/windowsazure/jj129580.aspx#create_thumbnail_task
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public string CreateThumbnailForVideoAsset(IAsset asset, string uniqueFilename)
        {
            // Declare a new job.
            IJob job = _context.Jobs.Create("thumbnailJob_" + uniqueFilename);
            // Get a media processor reference, and pass to it the name of the 
            // processor to use for the specific task.
            IMediaProcessor processor = GetLatestMediaProcessorByName("Windows Azure Media Encoder");
            ITask task = job.Tasks.AddNew("thumbnailTask_" + uniqueFilename,
                processor,
                "Thumbnails",
                TaskOptions.ProtectedConfiguration);
            // Specify the input asset to be encoded.
            task.InputAssets.Add(asset);
            // Add an output asset to contain the results of the job.
            task.OutputAssets.AddNew("thumbnailingOutputAsset_" + uniqueFilename, AssetCreationOptions.None);

            // Launch the job.
            job.Submit();

            // Check job execution and wait for job to finish. 
            Task progressJobTask = job.GetExecutionProgressTask(CancellationToken.None);
            progressJobTask.Wait();

            // Get an updated job reference after waiting for a job on 
            // a thread.
            job = GetJob(job.Id);

            // If job state is Error, the event handling 
            // method for job progress should log errors.  Here we check 
            // for error state and exit if needed.
            if (job.State == JobState.Error)
            {
                throw new Exception("CreateThumbnailForVideoAsset: job error.");
            }

            IAsset thumbnailAsset = job.OutputMediaAssets[0];

            //
            // Find the .jpg in the asset
            //
            IAssetFile jpgFile = (from f in thumbnailAsset.AssetFiles
                                  where f.Name.EndsWith("_1.jpg")
                                  select f).FirstOrDefault();
            if (jpgFile == null)
                throw new ApplicationException("Could not find a .jpg file in the asset.id: " + asset.Id);

            // The demo code goes through a huge loop to find existing locators for the media asset, which doesn't seem to quite work
            // This is more of a sledgehammer to get a locator for the specific thumb asset
            ILocator locator = null;
            var accessPolicyTimeout = TimeSpan.FromDays(_defaultLockoutDays);
            IAccessPolicy readPolicy = _context.AccessPolicies.Create("Read Policy " + thumbnailAsset.Name + "_thumb", accessPolicyTimeout, AccessPermissions.Read);
            var startTime = DateTime.UtcNow.AddMinutes(-5);
            locator = _context.Locators.CreateSasLocator(thumbnailAsset, readPolicy, startTime);

            //
            // Build Uri
            //
            string thumbnailUri = string.Empty;
            if (locator != null)
            {
                UriBuilder ub = new UriBuilder(locator.Path);
                ub.Path += "/" + jpgFile.Name;
                thumbnailUri = ub.Uri.ToString();
            }

            return thumbnailUri;
        }

        /// <summary>
        /// Encodes a video asset to "H264 Broadband 720p"
        /// There are dozens of alternatives - see http://msdn.microsoft.com/en-us/library/windowsazure/jj129582.aspx
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        public string EncodeVideoAssetDefault(IAsset asset, string uniqueFilename)
        {
            return EncodeMediaAsset("H264 Broadband 720p", asset, uniqueFilename);
        }

        private string EncodeMediaAsset(string encoding, IAsset asset, string uniqueFilename)
        {
            // Declare a new job.
            IJob job = _context.Jobs.Create("encodingJob_" + uniqueFilename);
            // Get a media processor reference, and pass to it the name of the 
            // processor to use for the specific task.
            IMediaProcessor processor = GetLatestMediaProcessorByName("Windows Azure Media Encoder");
            ITask task = job.Tasks.AddNew("encodingTask_" + uniqueFilename,
                processor,
                encoding,
                TaskOptions.ProtectedConfiguration);
            // Specify the input asset to be encoded.
            task.InputAssets.Add(asset);
            // Add an output asset to contain the results of the job.
            task.OutputAssets.AddNew("encodingOutputAsset_" + uniqueFilename, AssetCreationOptions.None);

            // Launch the job.
            job.Submit();

            // Check job execution and wait for job to finish. 
            Task progressJobTask = job.GetExecutionProgressTask(CancellationToken.None);
            progressJobTask.Wait();

            // Get an updated job reference after waiting for a job on a thread.
            job = GetJob(job.Id);

            // If job state is Error, the event handling method for job progress should log errors.  
            if (job.State == JobState.Error)
            {
                throw new Exception("EncodeMediaAsset: job error.");
            }

            IAsset videoAsset = job.OutputMediaAssets[0];

            // Find the .mp4 in the asset
            IAssetFile mp4File = (from f in videoAsset.AssetFiles
                                  where f.Name.EndsWith(".mp4")
                                  select f).FirstOrDefault();
            if (mp4File == null)
                throw new ApplicationException("Could not find a .mp4 file in the asset.id: " + asset.Id);

            ILocator locator = null;

            var accessPolicyTimeout = TimeSpan.FromDays(_defaultLockoutDays);
            IAccessPolicy readPolicy = _context.AccessPolicies.Create("Read Policy " + videoAsset.Name + "_encoded", accessPolicyTimeout, AccessPermissions.Read);
            var startTime = DateTime.UtcNow.AddMinutes(-5);
            locator = _context.Locators.CreateSasLocator(videoAsset, readPolicy, startTime);

            //
            // Build Uri
            //
            string videoUri = string.Empty;
            if (locator != null)
            {
                UriBuilder ub = new UriBuilder(locator.Path);
                ub.Path += "/" + mp4File.Name;
                videoUri = ub.Uri.ToString();
            }

            return videoUri;
        }

        public bool EncodeVideoAssetDefaultAndThumbnail(IAsset asset, string uniqueFilename, out string videoUrl, out string thumbnailUrl)
        {
            videoUrl = string.Empty;
            thumbnailUrl = string.Empty;

            // Declare a new job.
            IJob job = _context.Jobs.Create("encodingJob_" + uniqueFilename);
            // Get a media processor reference, and pass to it the name of the 
            // processor to use for the specific task.
            IMediaProcessor processor = GetLatestMediaProcessorByName("Windows Azure Media Encoder");
            ITask encodingTask = job.Tasks.AddNew("encodingTask_" + uniqueFilename,
                processor,
                "H264 Broadband 720p",
                TaskOptions.ProtectedConfiguration);
            // Specify the input asset to be encoded.
            encodingTask.InputAssets.Add(asset);
            // Add an output asset to contain the results of the first task.
            encodingTask.OutputAssets.AddNew("encodingOutputAsset_" + uniqueFilename, AssetCreationOptions.None);

            ITask thumbnailingTask = job.Tasks.AddNew("thumbnailTask_" + uniqueFilename,
                processor,
                "Thumbnails",
                TaskOptions.ProtectedConfiguration);
            // Specify the input asset to be thumbnailed.
            thumbnailingTask.InputAssets.Add(asset);
            // Add another output asset to contain the results of the second task.
            thumbnailingTask.OutputAssets.AddNew("thumbnailingOutputAsset_" + uniqueFilename, AssetCreationOptions.None);

            // Launch the job.
            job.Submit();

            // Check job execution and wait for job to finish. 
            Task progressJobTask = job.GetExecutionProgressTask(CancellationToken.None);
            progressJobTask.Wait();

            // Get an updated job reference after waiting for a job on a thread.
            job = GetJob(job.Id);

            // If job state is Error, the event handling method for job progress should log errors.  
            if (job.State == JobState.Error)
            {
                throw new Exception("EncodeMediaAsset: job error.");
            }

            IAsset videoAsset = job.OutputMediaAssets[0];

            // Find the .mp4 in the asset
            IAssetFile mp4File = (from f in videoAsset.AssetFiles
                                  where f.Name.EndsWith(".mp4")
                                  select f).FirstOrDefault();
            if (mp4File == null)
                throw new ApplicationException("Could not find a .mp4 file in the asset.id: " + asset.Id);

            ILocator videoLocator = null;

            var accessPolicyTimeout = TimeSpan.FromDays(_defaultLockoutDays);
            IAccessPolicy readPolicy = _context.AccessPolicies.Create("Read Policy " + videoAsset.Name + "_encoded", accessPolicyTimeout, AccessPermissions.Read);
            var startTime = DateTime.UtcNow.AddMinutes(-5);
            videoLocator = _context.Locators.CreateSasLocator(videoAsset, readPolicy, startTime);

            // Build video Uri
            if (videoLocator != null)
            {
                UriBuilder ub = new UriBuilder(videoLocator.Path);
                ub.Path += "/" + mp4File.Name;
                videoUrl = ub.Uri.ToString();
            }

            IAsset thumbnailAsset = job.OutputMediaAssets[1];

            // Find the .jpg in the asset
            IAssetFile jpgFile = (from f in thumbnailAsset.AssetFiles
                                  where f.Name.EndsWith("_1.jpg")
                                  select f).FirstOrDefault();
            if (jpgFile == null)
                throw new ApplicationException("Could not find a .jpg file in the asset.id: " + asset.Id);

            // The demo code goes through a huge loop to find existing locators for the media asset, which doesn't seem to quite work
            // This is more of a sledgehammer to get a locator for the specific thumb asset
            ILocator thumbLocator = null;
            accessPolicyTimeout = TimeSpan.FromDays(_defaultLockoutDays);
            readPolicy = _context.AccessPolicies.Create("Read Policy " + thumbnailAsset.Name + "_thumb", accessPolicyTimeout, AccessPermissions.Read);
            startTime = DateTime.UtcNow.AddMinutes(-5);
            thumbLocator = _context.Locators.CreateSasLocator(thumbnailAsset, readPolicy, startTime);

            // Build thumbnail Uri
            if (thumbLocator != null)
            {
                UriBuilder ub = new UriBuilder(thumbLocator.Path);
                ub.Path += "/" + jpgFile.Name;
                thumbnailUrl = ub.Uri.ToString();
            }
            return true;

        }

        public bool DeleteAssetByName(string assetName)
        {
            // Use a LINQ Select query to get an asset.
            var assetInstance =
                from a in _context.Assets
                where a.Name == assetName
                select a;
            // Reference the asset as an IAsset.
            IAsset asset = assetInstance.FirstOrDefault();

            if (asset != null)
            {
                asset.Delete();
                return true;
            }
            else return false;
        }

        /// <summary>
        /// Note that deleting these assets also deletes the storage they allocated, and even if the tasks didn't complete successfully 
        /// that cascading deletion still executes cleanly
        /// </summary>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        public bool DeleteEncodingAndThumbnailAssets(string uniqueFilename)
        {
            return DeleteAssetByName("encodingOutputAsset_" + uniqueFilename) && DeleteAssetByName("thumbnailingOutputAsset_" + uniqueFilename);
        }


    }
}

