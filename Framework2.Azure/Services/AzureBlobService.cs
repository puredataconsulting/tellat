﻿using Framework2.Azure.Config;
using Framework2.Azure.Exceptions;
using Framework2.Azure.Services.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

namespace Framework2.Azure.Services
{
    /// <summary>
    /// Wrappers the Azure image upload functionality
    /// </summary>
    public class AzureBlobService : IAzureBlobService
    {
        private readonly CloudBlobClient _cloudBlobClient;

        /// <summary>
        /// AzureBlobService constructor
        /// </summary>
        public AzureBlobService()
        {
            // Retrieve the storage account details
            CloudStorageAccount storageAccount;
            try
            {
                //var connectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1}", AzureConfig.StorageAccountName, AzureConfig.StorageAccountPrimaryAccessKey);
                var connectionString = string.Format("DefaultEndpointsProtocol=http;AccountName={0};AccountKey={1}", AzureConfig.StorageAccountName, AzureConfig.StorageAccountPrimaryAccessKey);
                storageAccount = CloudStorageAccount.Parse(connectionString);
            }
            catch (Exception ex)
            {
                throw new AzureException("The Azure Storage connection string looks to be invalid", ex);
            }

            // Register the client
            _cloudBlobClient = storageAccount.CreateCloudBlobClient();
        }

        public CloudBlobClient CloudBlobClient
        {
            get
            {
                return _cloudBlobClient;
            }
        }

        /// <summary>
        /// Uploads a stream to Azure BLOB storage
        /// </summary>
        public CloudBlockBlob UploadToContainer(Stream stream, AzureContainers containerName, string fileName)
        {
            // Validate the arguments
            if (stream == null)
            {
                throw new AzureException("The stream is null or invalid");
            }

            if (string.IsNullOrEmpty(fileName))
            {
                throw new AzureException("The filename is empty or invalid"); 
            }

            // Check the container for validity
            CloudBlobContainer container;
            try
            {
                container = _cloudBlobClient.GetContainerReference(containerName.ToString().ToLower());
            }
            catch (Exception ex)
            {
                throw new AzureException("An unknown problem has occurred with the Azure container " + containerName, ex);
            }

            CloudBlockBlob blob = null;
            // Upload the file
            try
            {
                // Retrieve reference for the new BLOB
                blob = container.GetBlockBlobReference(fileName);

                // Upload to Azure
                stream.Seek(0, SeekOrigin.Begin);
                blob.UploadFromStream(stream);

                // We have to set up the MIME type or jwplayer in IE fails
                string ext = Path.GetExtension(fileName).Substring(1);
                if (ext == "mp4")
                {
                    blob.Properties.ContentType = "video/mp4";
                }
                else if (ext == "m4v")
                {
                    blob.Properties.ContentType = "video/mp4";
                }
                else if (ext == "flv")
                {
                    blob.Properties.ContentType = "video/x-flv";
                }
                else if (ext == "mov")
                {
                    blob.Properties.ContentType = "video/quicktime";
                }
                else if (ext == "avi")
                {
                    blob.Properties.ContentType = "video/x-msvideo";
                }
                else if (ext == "wmv")
                {
                    blob.Properties.ContentType = "video/x-ms-wmv";
                }
                blob.SetProperties();
            }
            catch (Exception ex)
            {
                throw new AzureException("The Azure file upload has failed", ex);
            }
            return blob;
        }

        public bool RemoveFromContainer(AzureContainers containerName, string fileName)
        {
            return RemoveFromContainer(containerName.ToString(), fileName);
        }

        public bool RemoveFromContainer(string containerName, string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new AzureException("The filename is empty or invalid");
            }

            // Prevent the removal of fixed assets
            if (containerName.ToLower() == "content") return false;

            // Check the container for validity
            CloudBlobContainer container;
            try
            {
                container = _cloudBlobClient.GetContainerReference(containerName.ToLower());
            }
            catch (Exception ex)
            {
                throw new AzureException("An unknown problem has occurred with the Azure container " + containerName, ex);
            }

            // Delete the file
            try
            {
                var blob = container.GetBlockBlobReference(fileName);
                if (blob != null)
                {
                    blob.DeleteIfExists();
                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                throw new AzureException("The Azure blob delete has failed", ex);
            }
        }

        /// <summary>
        /// Delete a container if it contains no blobs
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        public bool DeleteContainerIfEmpty(string containerName)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new AzureException("The filename is empty or invalid");
            }

            // Check the container for validity
            CloudBlobContainer container;
            try
            {
                container = _cloudBlobClient.GetContainerReference(containerName.ToLower());
            }
            catch (Exception ex)
            {
                throw new AzureException("An unknown problem has occurred with the Azure container " + containerName, ex);
            }

            List<IListBlobItem> blobs = new List<IListBlobItem>(container.ListBlobs());
            if(blobs.Count > 0)return false;

            container.Delete();

            return true;
        }

        /// <summary>
        /// Designed to allow us to strip jpg files from a container designed for thumbnails
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        public bool RemoveContainerFilesByExtension(string containerName, string extension)
        {
            if (string.IsNullOrEmpty(containerName))
            {
                throw new AzureException("The filename is empty or invalid");
            }

            // Check the container for validity
            CloudBlobContainer container;
            try
            {
                container = _cloudBlobClient.GetContainerReference(containerName.ToLower());
            }
            catch (Exception ex)
            {
                throw new AzureException("An unknown problem has occurred with the Azure container " + containerName, ex);
            }

            try
            {
                foreach (CloudBlockBlob blob in container.ListBlobs())
                {
                    string[] segments = blob.Uri.Segments;
                    string itemName = segments[segments.Length - 1];
                    if (itemName.EndsWith(extension))
                    {
                        blob.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AzureException(string.Format("Failed to delete blobs from " + containerName), ex);
            }

            return true;
        }

        public bool RemoveBlob(CloudBlockBlob blob)
        {
            if (blob == null) return false;

            try
            {
                blob.DeleteIfExists();
            }
            catch(Exception ex)
            {
                throw new AzureException(string.Format("Failed to delete blob " + blob.Uri), ex);

            }
            return true;
        }

        public bool FindBlob(string containerName, string fileName, out CloudBlockBlob blob)
        {
            blob = null;
            if (string.IsNullOrEmpty(containerName))
            {
                throw new AzureException("The filename is empty or invalid");
            }

            // Check the container for validity
            CloudBlobContainer container = null;
            try
            {
                container = _cloudBlobClient.GetContainerReference(containerName.ToLower());
                blob = container.GetBlockBlobReference(fileName);
            }
            catch (Exception ex)
            {
                throw new AzureException("An unknown problem has occurred with the Azure container " + containerName, ex);
            }

            if (blob == null) return false;
            else return blob.Exists();
        }


    }
}
