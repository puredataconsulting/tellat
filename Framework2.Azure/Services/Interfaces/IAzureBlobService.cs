﻿using System.IO;
using Framework2.Azure.Config;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Framework2.Azure.Services.Interfaces
{
    /// <summary>
    /// Wrappers the Azure BLOB upload and storage functions
    /// </summary>
    public interface IAzureBlobService
    {

        CloudBlobClient CloudBlobClient
        {
            get;
        }

        /// <summary>
        /// Uploads a stream to Azure BLOB storage, returns the BLOB it created
        /// </summary>
        CloudBlockBlob UploadToContainer(Stream stream, AzureContainers containerName, string fileName);

        /// <summary>
        /// Seek a BLOB. Returns false if a blob of that name doesn't exist, but in that case the returned blob is still a placeholder reference
        /// otherwise returns true and the blob already exists.
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        bool FindBlob(string containerName, string fileName, out CloudBlockBlob blob);

        /// <summary>
        /// Remove an item from Azure storage by name
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        bool RemoveFromContainer(AzureContainers containerName, string fileName);

        bool RemoveFromContainer(string containerName, string fileName);

        /// <summary>
        /// Delete a container if it contains no blobs
        /// </summary>
        /// <param name="containerName"></param>
        /// <returns></returns>
        bool DeleteContainerIfEmpty(string containerName);

        /// <summary>
        /// Designed to allow us to strip jpg files from a container designed for thumbnails
        /// </summary>
        /// <param name="containerName"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        bool RemoveContainerFilesByExtension(string containerName, string extension);

        bool RemoveBlob(CloudBlockBlob blob);

    }
}
