﻿using Framework2.Azure.Config;
using Microsoft.WindowsAzure.MediaServices.Client;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Azure.Services.Interfaces
{
    public interface IAzureMediaAssetService
    {
        /// <summary>
        /// Copies a file from blob storage into Media Services and returns its public url
        /// </summary>
        /// <param name="cloudBlobClient"></param>
        /// <param name="containerName"></param>
        /// <param name="fileName"></param>
        /// <param name="returnedAsset">The Media Services asset that was created</param>
        /// <returns></returns>
        string CopyMediaAssetFromBlob(CloudBlobClient cloudBlobClient, AzureContainers containerName, string uniqueFilename, out IAsset returnedAsset);

        /// <summary>
        /// Returns a URL for ONE of the thumbnails that are created from video asset asset.
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        string CreateThumbnailForVideoAsset(IAsset asset, string uniqueFilename);

        /// <summary>
        /// Encodes a video asset to "H264 Broadband 720p"
        /// There are dozens of alternatives - see http://msdn.microsoft.com/en-us/library/windowsazure/jj129582.aspx
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        string EncodeVideoAssetDefault(IAsset asset, string uniqueFilename);

        /// <summary>
        /// Combines the EncodeVideoAssetDefault and CreateThumbnailForVideoAsset jobs into one, which is much more
        /// efficient - only one VM is spun up in Azure
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        bool EncodeVideoAssetDefaultAndThumbnail(IAsset asset, string uniqueFilename, out string videoUrl, out string thumbnailUrl);

        /// <summary>
        /// When we did the encoding and thumbnailing, we created assets with predictable names from a filename stem
        /// This is how we delete them subsequently
        /// </summary>
        /// <param name="uniqueFilename"></param>
        /// <returns></returns>
        bool DeleteEncodingAndThumbnailAssets(string uniqueFilename);

        bool DeleteAssetByName(string assetId);
       


    }
}
