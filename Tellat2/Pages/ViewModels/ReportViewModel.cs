﻿using System.Collections.Generic;
using System.Web.Mvc;
using Framework2.Core.Models.Core;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the Report page controller with data binding
    /// </summary>
    public class ReportViewModel : BaseViewModel
    {
        public class GroupLink
        {
            public string GroupName { get; set; }
            public bool IsLinked { get; set; }
            public int GroupId { get; set; }
        }

        public List<Category> Categories { get; set; } 

        public Report Report { get; set; }


        public List<string> MediaPaths { get; set; }

        public string SuggestedTags { get; set; }

        public List<SelectListItem> ExistingTags { get; set; }

        public List<Report> RelatedReports { get; set; }

        public List<bool> CategoryIds { get; set; }

        public List<GroupLink> GroupLinks { get; set; }

        public Report.Scope Scope { get; set; }

        public Report.LifeCycle LifeStage { get; set; }

        public bool IsPrivate { get; set; }
        public bool IsLimited { get; set; }
        public bool IsPublic { get; set; }

        public string UploadedVideoBlobName { get; set; }

        public ReportOwnerComponentViewModel ReportOwnerComponentViewModel { get; set; }
    }
}