﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the groups page controller with data binding
    /// </summary>
    public class GroupsViewModel : BaseViewModel
    {

        public Dictionary<Group, ApplicationUser> GroupsDictionary { get; set; }

    }
}