﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the users page controller with data binding
    /// </summary>
    public class UsersViewModel : SortablesViewModel
    {
        public ApplicationUser User { get; set; }
        public List<ApplicationUser> Users { get; set; }

        public bool IsEditableByThisUser { get; set; }
    }
}