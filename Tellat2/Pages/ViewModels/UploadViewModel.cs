﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the upload controller with data binding
    /// </summary>
    public class UploadViewModel : BaseViewModel
    {
        // Selectors
        public List<SelectListItem> Categories { get; set; }
        [Display(Name = @"Category")]
        public int CategoryId { get; set; }

        // Rquired details
        [Required(ErrorMessage = @"Upload a video")]
        [Display(Name = @"Filename")]
        public string Filename { get; set; }

        [Required(ErrorMessage = @"Enter a title for the report")]
        [Display(Name = @"Title")]
        public string Title { get; set; }

        [Required(ErrorMessage = @"Enter some copy for the report")]
        [Display(Name = @"Text")]
        public string Text { get; set; }

        [Required(ErrorMessage = @"Enter some tags")]
        [Display(Name = @"Tags")]        
        public string Tags { get; set; }

        // Optional details
        [Required(ErrorMessage = @"Enter a location")]
        [Display(Name = @"Location")]        
        public string Location { get; set; }
    }
}