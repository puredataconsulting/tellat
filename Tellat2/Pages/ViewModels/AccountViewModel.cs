﻿using System.ComponentModel.DataAnnotations;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the account controller with data binding
    /// </summary>
    public class AccountViewModel : BaseViewModel
    {
        [Required(ErrorMessage = "Invalid email address")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Invalid password")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public bool IsKeepingLogin { get; set; }
    }
}