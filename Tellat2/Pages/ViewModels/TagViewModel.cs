﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the tag page controller with data binding
    /// </summary>
    public class TagViewModel : SortablesViewModel
    {
        public Tag Tag { get; set; }
        public List<Report> Reports { get; set; }

        public Report PrimaryReport { get; set; }
    }
}