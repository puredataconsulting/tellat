﻿using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the registration page controller with data binding
    /// </summary>
    public class RegistrationViewModel : BaseViewModel
    {
        // Required details
        [Required(ErrorMessage = "Enter an email address")]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "Addresses must match")]
        [Display(Name = "Confirm Email Address")]
        public string EmailAddressRepeat { get; set; }

        [Required(ErrorMessage = "Please enter a username")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Enter a biography")]
        [Display(Name = "Biography")]
        public string ProfileText { get; set; }

        [Required(ErrorMessage = "Enter a first name")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter a last name")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public List<SelectListItem> Countries { get; set; }

        [Required(ErrorMessage = "Please enter your country of residence")]
        [Display(Name = "Country")]
        public int CountryId { get; set; }

        public List<SelectListItem> Genders { get; set; }

        public string GenderId { get; set; }

        public List<SelectListItem> BirthDays { get; set; }

        public string BirthDayId { get; set; }
        public List<SelectListItem> BirthMonths { get; set; }

        public string BirthMonthId { get; set; }
        public List<SelectListItem> BirthYears { get; set; }

        public string BirthYearId { get; set; }

        public bool IsGenderPublished { get; set; }

        public bool IsLocationPublished { get; set; }

        public bool IsDateOfBirthPublished { get; set; }

        public bool IsYearOfBirthPublished { get; set; }


        public RegistrationViewModel()
        {
            this.EmailAddress = string.Empty;
            this.Password = string.Empty;
            this.IsDateOfBirthPublished = false;
            this.IsGenderPublished = false;
            this.IsYearOfBirthPublished = true;
            this.IsLocationPublished = true;

            Genders = new List<SelectListItem>
            {
                new SelectListItem {Text = "Male", Value="0", Selected=false },
                new SelectListItem {Text = "Female", Value="1", Selected= false },
                new SelectListItem {Text = "Questioning", Value="2", Selected= false }
            };

            BirthDays = new List<SelectListItem>();
            for(int i = 1 ; i <= 31 ; i++)
            {
                BirthDays.Add(new SelectListItem { Text = i.ToString(), Value = i .ToString()});
            }

            BirthMonths = new List<SelectListItem>();
            BirthMonths.Add(new SelectListItem { Text = "January", Value = "1" });
            BirthMonths.Add(new SelectListItem { Text = "February", Value = "2" });
            BirthMonths.Add(new SelectListItem { Text = "March", Value = "3" });
            BirthMonths.Add(new SelectListItem { Text = "April", Value = "4" });
            BirthMonths.Add(new SelectListItem { Text = "May", Value = "5" });
            BirthMonths.Add(new SelectListItem { Text = "June", Value = "6" });
            BirthMonths.Add(new SelectListItem { Text = "July", Value = "7" });
            BirthMonths.Add(new SelectListItem { Text = "August", Value = "8" });
            BirthMonths.Add(new SelectListItem { Text = "September", Value = "9" });
            BirthMonths.Add(new SelectListItem { Text = "October", Value = "10" });
            BirthMonths.Add(new SelectListItem { Text = "November", Value = "11" });
            BirthMonths.Add(new SelectListItem { Text = "December", Value = "12" });

            BirthYears = new List<SelectListItem>();

            for(int i = 1930 ; i < 2004 ; i++)
            {
                BirthYears.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }
        }
    }
}