﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    // It seems like overkill having separate MVC for Followers and Following but we're going to want to 
    // be able to mail Followers, invite people to follow us, and so on, in future
    public class FollowingViewModel : SortablesViewModel
    {
        public List<ApplicationUser> Members { get; set; }
    }
}