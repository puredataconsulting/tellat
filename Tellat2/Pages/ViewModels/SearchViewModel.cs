﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the search page controller with data binding
    /// </summary>
    public class SearchViewModel : BaseViewModel
    {
        public List<Report> Reports { get; set; } 
    }
}