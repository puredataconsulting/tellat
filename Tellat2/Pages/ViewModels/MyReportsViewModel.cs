﻿
using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the reports page controller with data binding
    /// </summary>
    public class MyReportsViewModel : SortablesViewModel
    {
        public List<Report> Reports { get; set; }

        public ApplicationUser Member { get; set; }
    }
}