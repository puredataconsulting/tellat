﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the home controller with data binding
    /// </summary>
    public class HomeViewModel : BaseViewModel
    {
        public List<Category> Categories { get; set; }

        public int TopReportID { get; set; }
    }
}