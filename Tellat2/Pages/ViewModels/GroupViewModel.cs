﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the groups page controller with data binding
    /// </summary>
    public class GroupViewModel : SortablesViewModel
    {
        public ApplicationUser Owner { get; set; }
        public List<ApplicationUser> Members { get; set; }

        public List<Report> Reports { get; set; }

        public Group Group { get; set; }

        // Fields for dealing with group creation
        public string Description { get; set; }

        public bool IsAlreadyMember;

    }
}