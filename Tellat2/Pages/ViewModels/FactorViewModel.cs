using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tellat2.Pages.ViewModels
{
    public class FactorViewModel {
        public string Purpose { get; set; }
    }
}