﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tellat2.Pages.ViewModels
{

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
}
