﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Pages.ViewModels
{
    public class SortablesViewModel : BaseViewModel
    {
        public string Title { get; set; }

        public int ReportsSortCriterion {  get; set;}

        public int UsersSortCriterion { get; set; }

        public SortablesViewModel()
        {
            // We're using neg numbers to imply no fixed sort criterion
            ReportsSortCriterion = -1;
            UsersSortCriterion = -1;
        }
    }
}