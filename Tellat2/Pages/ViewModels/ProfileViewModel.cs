﻿using Framework2.Core.Models.Core;
using System.ComponentModel.DataAnnotations;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Pages.ViewModels
{
    /// <summary>
    /// Provides the profile page controller with data binding
    /// </summary>
    public class ProfileViewModel : BaseViewModel
    {
        [Required(ErrorMessage = @"Enter a first name")]
        [Display(Name = @"First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = @"Enter a last name")]
        [Display(Name = @"Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = @"Enter a user name which others will see")]
        [Display(Name = @"User Name")]
        public string UserName { get; set; }


        [Required(ErrorMessage = @"Enter a biography")]
        [Display(Name = @"Biography")]
        public string ProfileText { get; set; }

        public ApplicationUser ProfiledUser;
        // We need to suppress some items if the current user's not logged in and looking at himself
        public bool IsProfiledUserCurrent;

        public int NPublishedReports { get; set; }

        public int NRatedBy { get; set; }

        public UserOwnerComponentViewModel UserOwnerComponentViewModel { get; set; }
    }
}