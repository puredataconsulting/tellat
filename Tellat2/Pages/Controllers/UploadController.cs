﻿using System;
using System.Web.Mvc;
using Framework2.Azure.Config;
using Framework2.Azure.Exceptions;
using Framework2.Azure.Services.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using System.IO;
using Microsoft.WindowsAzure.MediaServices.Client;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Tellat2.Pages.Controllers
{
    public class UploadController : BaseController<UploadViewModel>
    {
        [Dependency]
        public IAzureBlobService AzureBlobService { get; set; }

        [Dependency]
        public IAzureMediaAssetService AzureMediaAssetService { get; set; }

        /// <summary>
        /// Upload/UploadVideo
        /// </summary>   
        public ActionResult UploadVideo()
        {
            string publishUrl = string.Empty;

            // Validate the arguments
            var httpPostedFileBase = Request.Files[0];
            if (httpPostedFileBase == null || httpPostedFileBase.InputStream == null)
            {
                return Json(new { success = false, message = "The provided file is empty or invalid" }, "text/html");
            }
            string originalFilename = System.IO.Path.GetFileName(httpPostedFileBase.FileName);
            string extension = System.IO.Path.GetExtension(originalFilename).Substring(1).ToLower();

            if(!AzureConfig.SupportedVideoFormats.Contains(extension))
            {
                return Json(new { success = false, message = extension + " files are not supported. Sorry!" }, "text/html");
            }

            // Upload to Azure
            string uniqueFilename = Guid.NewGuid().ToString() + "_" + originalFilename;
            CloudBlockBlob originalBlob = null;
            try
            {
                originalBlob = AzureBlobService.UploadToContainer(httpPostedFileBase.InputStream, AzureContainers.Videos, uniqueFilename);
            }
            catch
            {
                return Json(new { success = false, message = "Unable to upload the video file to cloud storage" }, "text/html");
            }


            // Success
            return Json(new { success = true, blobName = uniqueFilename}, "text/html");
        }


    }
}
