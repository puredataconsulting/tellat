﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    public class GuideController : BaseController<GuideViewModel>
    {
        //
        // GET: /Default1/

        public ActionResult Index()
        {
            return View(ViewModel);
        }

    }
}
