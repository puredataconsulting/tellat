﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;
using Framework2.Core.Services.Interfaces;
using System;
using Tellat2.Emails.ViewModels;
using Platform.Email.Services;
using System.Threading.Tasks;

namespace Tellat2.Pages.Controllers
{
    public class GroupController : BaseController<GroupViewModel>
    {
        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }


        [Dependency]
        public IGroupRepository GroupRepository { get; set; }

        [Dependency]
        public IGroupReportRepository GroupReportRepository { get; set; }


        [Dependency]
        public INotificationRepository NotificationRepository { get; set; }

        /// <summary>
        /// /Group/Id
        /// </summary>
        [HttpGet]
        public ActionResult Index(int id, int? usersSortCriterion, int? reportsSortCriterion)
        {
            try
            {
                ViewModel.UsersSortCriterion = usersSortCriterion.HasValue ? usersSortCriterion.Value : -1;
                ViewModel.Group = GroupRepository.FindById(id);
                ViewModel.Members = GroupMemberRepository.FindAllByGroupId(id, Utility.Utilities.GetUserSortCriterion(usersSortCriterion));

                bool isPublicOnly = !ViewModel.IsCurrent();
                ViewModel.ReportsSortCriterion = reportsSortCriterion.HasValue ? reportsSortCriterion.Value : -1;
                ViewModel.Reports = GroupReportRepository.FindReportsByGroup(id, Utility.Utilities.GetReportSortCriterion(reportsSortCriterion), isPublicOnly);

            }
            catch (RepositoryException ex)
            {
            }

            return View(ViewModel);
        }

        /// <summary>
        /// /Group/Create
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                ViewModel.CurrentUser = this.CurrentUser;
                ViewModel.Owner = ViewModel.CurrentUser;
            }
            catch (RepositoryException ex)
            {
            }

            return View("Create", ViewModel);
        }

        /// <summary>
        /// /Group/Join
        /// </summary>
        [HttpGet]
        public async Task<ActionResult> Join(int id)
        {
            try
            {
                ViewModel.Group = GroupRepository.FindById(id);
                ViewModel.Group.LastActivity = DateTime.Now;
                List<ApplicationUser> owners = GroupMemberRepository.FindOwnersByGroupId(id);

                bool isAdmin;
                ViewModel.IsAlreadyMember = GroupMemberRepository.IsMember(CurrentUserId, id, out isAdmin);
                if (!ViewModel.IsAlreadyMember)
                {
                    // We need to issue an email and create a notification in the DB
                    JoinGroupEmailViewModel emailModel = new JoinGroupEmailViewModel()
                    {
                        GroupName = ViewModel.Group.Title,
                        RequesterName = CurrentUser.UserName
                    };

                    string html = EmailService.FillTemplateFromModel("~/Emails/Views/JoinGroup.cshtml", emailModel);
                    await UserManager.SendEmailAsync(owners[0].Id, "A Tellat user wants to join your Newsgroup", html);

                    // For the time being let's just add this guy to the group
                    //ViewModel.IsAlreadyMember = !GroupMemberRepository.Join(ViewModel.CurrentUserId, ViewModel.Group.ID, false);
                    //if(!)

                    // Create a notification for the owner
                    NotificationRepository.AddNotification(owners[0].Id, NotificationTypes.JoinGroupRequest, CurrentUserId, ViewModel.Group.ID.ToString());

                    // Show the activity for the user and the group
                    this.CurrentUser.LastActivity = DateTime.Now;

                    //TODO does he get updated anyway?
                    //UserRepository.Update(ViewModel.CurrentUser);

                    GroupRepository.Update(ViewModel.Group);
                }

            }
            catch (RepositoryException ex)
            {
            }

            return View("Join", ViewModel);
        }

        /// <summary>
        /// /Report/Create
        /// </summary>
        [HttpPost]
        public ActionResult Create(GroupViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Create", model);
            }

            try
            {
                // Don't create until we get the button press!!
                model.Group = GroupRepository.Create();
                model.Group.Title = model.Title;
                model.Group.Description = model.Description;
                model.Group.LastActivity = DateTime.Now;
                GroupRepository.Add(model.Group);

                // Wire the new group to this user as its admin (the first admin typically being returned as the Owner in future)
                GroupMemberRepository.Join(ViewModel.CurrentUserId, model.Group.ID, true);
            }
            catch (RepositoryException ex)
            {
            }

            return RedirectToAction("Index", "Users", new { userId = ViewModel.CurrentUserId});
        }
    }
}
