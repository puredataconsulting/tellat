﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;

namespace Tellat2.Pages.Controllers
{
    public class TagController : BaseController<TagViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public ITagRepository TagRepository { get; set; }

        [Dependency]
        public ITaggingRepository TaggingRepository { get; set; }

        /// <summary>
        /// /Category/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index(int id, int? sortCriterion)
        {
            try
            {
               
                 ViewModel.ReportsSortCriterion = sortCriterion.HasValue? sortCriterion.Value : -1;
                ViewModel.Tag = TagRepository.FindById(id);
                ViewModel.Reports = TaggingRepository.FindReportsByTag(id, Utility.Utilities.GetReportSortCriterion(sortCriterion));
                ViewModel.Title = "Reports matching \"" + ViewModel.Tag.Text + "\"";


                // Tags can't be added without a report so we shouldn't ever get zero back
                // (unless we allow the deletion of reports and not orphaned tags?)

                // Seek our report with the most viewings
                Report bestReport = null;
                int bestCount = 0;
                foreach(Report report in ViewModel.Reports)
                {
                    if(report.NViewings > bestCount)
                    {
                        bestCount = report.NViewings;
                        bestReport = report;
                    }
                }
                ViewModel.PrimaryReport = bestReport;
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }
    }
}
