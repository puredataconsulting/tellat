﻿using System;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;
using System.Linq;

namespace Tellat2.Pages.Controllers
{
    public class ReportsController : BaseController<ReportsViewModel>
    {

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        public ActionResult Index(int? sortCriterion)
        {
            try
            {
                ViewModel.ReportsSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;


                // Is there a current user?
                bool isPublicOnly = true;
                ViewModel.Reports = ReportRepository.FindAllSorted(Utility.Utilities.GetReportSortCriterion(sortCriterion), isPublicOnly);
                ViewModel.Title = ViewModel.Reports.Count + " Reports";


                // Beware zero here
                // Seek our report with the most viewings
                Report bestReport = null;
                int bestCount = 0;
                foreach (Report report in ViewModel.Reports)
                {
                    if (report.NViewings > bestCount)
                    {
                        bestCount = report.NViewings;
                        bestReport = report;
                    }
                }
                ViewModel.PrimaryReport = bestReport;
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }

        /// <summary>
        /// /Reports/Search
        /// </summary>
        public ActionResult Search(string searchTerms, int? sortCriterion)
        {
            try
            {
                ViewModel.Title = "Reports matching " + searchTerms;
                ViewModel.Reports = ReportRepository.DoSearch(searchTerms, Utility.Utilities.GetReportSortCriterion(sortCriterion), true);

                // Beware zero here
                // Seek our report with the most viewings
                Report bestReport = null;
                int bestCount = 0;
                foreach (Report report in ViewModel.Reports)
                {
                    if (report.NViewings > bestCount)
                    {
                        bestCount = report.NViewings;
                        bestReport = report;
                    }
                }
                ViewModel.PrimaryReport = bestReport;
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }
    }
}
