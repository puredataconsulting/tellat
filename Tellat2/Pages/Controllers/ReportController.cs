﻿using System;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tellat2.Components.ViewModels;
using Framework2.Core.Services.Interfaces;
using System.ComponentModel;
using Microsoft.WindowsAzure.MediaServices.Client;
using Framework2.Azure.Services;
using Framework2.Azure.Config;
using Framework2.Azure.Services.Interfaces;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading.Tasks;

namespace Tellat2.Pages.Controllers
{
    public class ReportController : BaseController<ReportViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        [Dependency]
        public IReportCategoryRepository ReportCategoryRepository { get; set; }

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        [Dependency]
        public ITagRepository TagRepository { get; set; }

        [Dependency]
        public ITaggingRepository TaggingRepository { get; set; }

        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        [Dependency]
        public IAzureMediaAssetService AzureMediaAssetService { get; set; }

        [Dependency]
        public IAzureBlobService AzureBlobService { get; set; }

        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }

        [Dependency]
        public IGroupReportRepository GroupReportRepository { get; set; }


        /// <summary>
        /// /Report/Index
        /// </summary>
        public ActionResult Index(int id)
        {
            try
            {
                ViewModel.Report = ReportRepository.FindById(id);
                string userId = ViewModel.Report.ApplicationUserId;
                ViewModel.RelatedReports = ReportRepository.FindRelatedReports(id, 5);

                ViewModel.ReportOwnerComponentViewModel = new ReportOwnerComponentViewModel();
                ViewModel.ReportOwnerComponentViewModel.IsReportEditableByThisUser = ViewModel.IsCurrentUserAdmin() || ViewModel.IsCurrent(userId);
                ViewModel.ReportOwnerComponentViewModel.IsReportDeleteableByThisUser = ViewModel.IsCurrentUserAdmin() || ViewModel.IsCurrent(userId);
                ViewModel.ReportOwnerComponentViewModel.ReportId = id;

                if (ViewModel.Report != null)
                {
                    ViewModel.Report.NViewings++;
                    ReportRepository.Update(ViewModel.Report);
                }
            }
            catch (RepositoryException ex)
            {
            }

            return View(ViewModel);
        }

        /// <summary>
        /// /Report/Create
        /// </summary>
        [Authorize]
        [HttpGet]
        public ActionResult Create()
        {
            try
            {
                int nCategories = 8;
                ViewModel.Categories = CategoryRepository.GetFixedCategories(nCategories);
                ViewModel.Report = ReportRepository.Create();
                ViewModel.CategoryIds = new List<bool>(nCategories);
                for (int i = 0; i < nCategories; i++) ViewModel.CategoryIds.Add(false);

                // Which groups is our boy involved with?
                ViewModel.GroupLinks = new List<ReportViewModel.GroupLink>();
                List<Group> groups = GroupMemberRepository.FindGroupsByUserId(CurrentUserId);
                foreach(Group group in groups)
                {
                    ViewModel.GroupLinks.Add(new ReportViewModel.GroupLink() { GroupId = group.ID, GroupName = group.Title, IsLinked = false });
                }
            }
            catch (RepositoryException ex)
            {
            }

            return View("Create", ViewModel);
        }

        /// <summary>
        /// /Report/Create
        /// </summary>
        [HttpPost]
        public ActionResult Create(ReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                //var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));
                //var list = errors.ToList();
                return View("Index", model);
            }

            try
            {
                model.Report.ApplicationUserId = CurrentUserId;
                model.Report.LastActivity = DateTime.Now;


                int nCategories = 8;
                model.Categories = CategoryRepository.GetFixedCategories(nCategories);

                ReportRepository.Add(model.Report);

                // The associated media item
                MediaItem mediaItem = MediaItemRepository.Create();
                // Set the video up with a temporary link to an off-the-shelf image - this will be replaced when the batch 
                // encode/thumbnail process has completed
                mediaItem.ReportID = model.Report.ID;
                mediaItem.MediaType = MediaItem.MediaTypes.Content;
                mediaItem.ThumbnailUri = "movie.png";
                mediaItem.Uri = string.Empty;
                // The assets we create going forward will be named based on the name that was given to the original BLOB upload
                // so if we remember that name we know what to delete later
                mediaItem.AssociatedAssets = model.UploadedVideoBlobName;
                MediaItemRepository.Add(mediaItem);

                // Wiring the report to its categories
                for (int i = 0; i < nCategories; i++)
                {
                    if (model.CategoryIds[i])
                    {
                        ReportCategoryRepository.AddReportToCategory(model.Report.ID, model.Categories[i].ID);
                    }
                }
                
                // Wiring the report to its groups (yes, I know, the behaviour's the same as for categories ergo they're the same thing)
                // Note that if a link isn't checked it's id comes back as 0 despite the hidden field. Hmm.
                if (model.GroupLinks != null)
                {
                    foreach (var groupLink in model.GroupLinks)
                    {
                        if (groupLink.IsLinked)
                        {
                            GroupReportRepository.AddReportToGroup(model.Report.ID, groupLink.GroupId);
                        }
                    }
                }

                // Now we need to think about the tags
                // The user's suggested tags came in via the model - we need to bin everything that isn't alphanumeric
                if (!string.IsNullOrEmpty(model.SuggestedTags))
                {
                    string[] fields = model.SuggestedTags.Split(new char[] { ' ', '\t', ';', ':' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string field in fields)
                    {
                        AddTagging(field, model.Report.ID);
                    }
                }

                // http://stackoverflow.com/questions/16469094/starting-and-forgetting-an-async-task-in-mvc-action
                // So... now we need to fire off a long-running process to encode and thumbnail this report
#pragma warning disable 4014 // The pragma prevents a warning that we're launching an async process with no expectation of a result - fire and forget
                Task.Run(async () =>
                {
                    await DoEncodingAsync(model.UploadedVideoBlobName, model.Report.ID);
                });
                
                // DoEncoding(model.UploadedVideoBlobName, model.Report.ID); // Sync call while I debug it

            }
            catch (Exception ex)
            {
                throw new Exception("ReportController.Create error!", ex);
            }

            return View("Uploaded", ViewModel);
        }

        public ActionResult Favourite(int reportId)
        {
            try
            {
                var favourite = FavouriteRepository.Create();

                favourite.ReportId = reportId;
                favourite.ApplicationUserId = CurrentUserId;

                FavouriteRepository.Add(favourite);
            }
            catch (RepositoryException ex)
            {
                throw new Exception("ReportController.Favourite error!", ex);
            }
            return RedirectToAction("Index", new { id = reportId });
        }


        /// <summary>
        /// /Report/Edit
        /// </summary>
        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                Report report = ReportRepository.FindById(id);
                ViewModel.Report = report;
                ViewModel.Scope = report.Scoping;
                ViewModel.LifeStage = report.LifeCycleStage;

                // Categories
                // This hassle is a throw back from the checkbox design!
                // We need to get the intersection and non-intersection of the set of categories 
                // before the edit, and after. No prizes for finding an easier way. Oh alright then. 
                int nCategories = 8;
                ViewModel.Categories = CategoryRepository.GetFixedCategories(nCategories);
                List<int> possibleCatIds = new List<int>(nCategories);
                for(int i = 0 ; i < nCategories ; i++ )
                {
                    possibleCatIds.Add(ViewModel.Categories[i].ID);
                }

                List<int> existingCatIdstrs = ReportCategoryRepository.GetCategoryIdsByReport(id);

                ViewModel.CategoryIds = new List<bool>(nCategories);

                for (int i = 0; i < nCategories; i++)
                {
                    // If the list of cats this report is in contains a given id, mark it for checkboxing
                    ViewModel.CategoryIds.Add(existingCatIdstrs.Contains(possibleCatIds[i]));
                }

                // Groups (cf categories above. This treatment is smarter I think)

                // Which groups is this report linked to?
                List<Group> attachedGroups = GroupReportRepository.FindGroupsByReport(id);
                // Which groups does this user belong to?
                 ViewModel.GroupLinks = new List<ReportViewModel.GroupLink>();
                List<Group> allGroups = GroupMemberRepository.FindGroupsByUserId(CurrentUserId);
                foreach (Group group in allGroups)
                {
                    // Add a group link, checked if this report belongs to that group
                    bool isLinked = attachedGroups.FirstOrDefault(g => g.ID == group.ID) != null;
                    ViewModel.GroupLinks.Add(new ReportViewModel.GroupLink() { GroupId = group.ID, GroupName = group.Title, IsLinked = isLinked });
                }


                // Tags

                // The fiddly bit here is getting the existing tags off into separate strings
                List<Tag> tags = TaggingRepository.FindTagsByReport(id);
                if (tags.Count > 0)
                {
                    StringBuilder sb = new StringBuilder(tags[0].Text);
                    for (int i = 1; i < tags.Count; i++)
                    {
                        sb.Append(" ");
                        sb.Append(tags[i].Text);
                    }
                    ViewModel.SuggestedTags = sb.ToString();
                }
                else ViewModel.SuggestedTags = string.Empty;
            }
            catch (RepositoryException ex)
            {
            }

            return View("Edit", ViewModel);
        }

        /// <summary>
        /// /Report/Edit
        /// </summary>
        [HttpPost]
        public ActionResult Edit(int id, ReportViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index", model);
            }

            try
            {
                Report report = ReportRepository.FindById(id);

                report.LifeCycleStage = model.LifeStage;
                report.Scoping = model.Scope;
                report.Title = model.Report.Title;
                report.Text = model.Report.Text;
                report.LastActivity = DateTime.Now;

                ReportRepository.Update(report);

                ViewModel.CurrentUser = this.CurrentUser;
                ViewModel.CurrentUser.LastActivity = DateTime.Now;
                //TODO again how do we update this guy
                //UserRepository.Update(ViewModel.CurrentUser);

                // Tags

                List<Tagging> ot = TaggingRepository.FindTaggingsByReport(id);
                List<string> originalTags = new List<string>();
                foreach (Tagging tagging in ot) originalTags.Add(tagging.Tag.Text);

                List<string> newTags = new List<string>();
                if (!string.IsNullOrEmpty(model.SuggestedTags))
                {
                    string[] fields = model.SuggestedTags.Split(new char[] { ' ', '\t', ';', ':' }, StringSplitOptions.RemoveEmptyEntries);
                    newTags.AddRange(fields);
                }

                // Now we need to compare the lists
                // What's in the old list but not in the new?
                for (int i = 0; i < originalTags.Count; i++)
                {
                    string originalTag = originalTags[i];
                    if (!newTags.Contains(originalTag))
                    {
                        TaggingRepository.RemoveTagging(ot[i].ID, id);
                    }
                }

                // What's in the new list but not in the old?
                foreach (string newTag in newTags)
                {
                    if (!originalTags.Contains(newTag))
                    {
                        AddTagging(newTag, id);
                    }
                }

                // Categories

                int nCategories = 8;
                model.Categories = CategoryRepository.GetFixedCategories(nCategories);

                List<int> currentCategoryIds = ReportCategoryRepository.GetCategoryIdsByReport(id);
                List<int> newCategoryIds = new List<int>();

                for (int i = 0; i < nCategories; i++)
                {
                    if (model.CategoryIds[i])
                    {
                        newCategoryIds.Add(model.Categories[i].ID);
                    }
                }
                // Let's get the left and right hand side of the Venn diagram between new and old categories
                // We don't care about the intersection - those stay as they were
                IEnumerable<int> retiredCategoryIds = currentCategoryIds.Except(newCategoryIds);
                IEnumerable<int> addedCategoryIds = newCategoryIds.Except(currentCategoryIds);

                foreach (var catId in retiredCategoryIds)
                {
                    ReportCategoryRepository.RemoveReportFromCategory(id, catId);
                }
                foreach (var catId in addedCategoryIds)
                {
                    ReportCategoryRepository.AddReportToCategory(id, catId);
                }


                // Let's get set intersections from what we had before and what we've got now
                IEnumerable<int> currentGroupIds = from gp in GroupReportRepository.FindGroupsByReport(id) select gp.ID;
                IEnumerable<int> newGroupIds = model.GroupLinks == null ? new List<int>() : from link in model.GroupLinks where link.IsLinked select link.GroupId;
                IEnumerable<int> retiredGroupIds = currentGroupIds.Except(newGroupIds);
                IEnumerable<int> addedGroupIds = newGroupIds.Except(currentGroupIds);
                foreach (var groupId in retiredGroupIds)
                {
                    GroupReportRepository.RemoveReportFromGroup(id, groupId);
                }
                foreach (var groupId in addedGroupIds)
                {
                    GroupReportRepository.AddReportToGroup(id, groupId);
                }


            }
            catch (RepositoryException ex)
            {
            }

            return RedirectToAction("Index", "Report", new {id=id });
        }

        /// <summary>
        /// /Report/Edit
        /// </summary>
        public ActionResult Delete(int id)
        {
            try
            {
                // We need to delete a report's link with 
                // Categories via ReportCategories
                // Tags via Taggings
                // Users via comments
                // Users via Favourites
                // Then we can delete the associated media item
                // Then we can unhook the report itself

                // Categories
                ReportCategoryRepository.DeleteByReport(id);
                // Tags
                TaggingRepository.RemoveTaggingsByReport(id);
                // Comments
                CommentRepository.RemoveCommentsByReport(id);
                // Favourites
                FavouriteRepository.RemoveFavouritesByReport(id);

                // Media (including blob delete)
                MediaItemRepository.RemoveMediaItemsByReport(id);

                ReportRepository.RemoveReport(id);
            }
            catch (RepositoryException ex)
            {
                throw new Exception("ReportController.Delete error!", ex);
            }

            return RedirectToAction("Index", "Profile");
        }

        public ActionResult Uploaded()
        {
            try
            {
            }
            catch (RepositoryException ex)
            {
            }
            return View(ViewModel);
        }

        protected void AddTagging(string tagtext, int reportId)
        {
            // Does a tag of this name already exist?
            Tag tag = TagRepository.FindByText(tagtext);
            if (tag == null)
            {
                // It's new - create a new one
                tag = TagRepository.Create();
                tag.Text = tagtext;
                tag.LastIntervalBoundary = DateTime.Now;
                tag.NHitsInPreviousInterval = 0;
                tag.NHitsInCurrentInterval = 0;
                TagRepository.Add(tag);
            }
            else
            {
                // Is this report already linked to that tag?
                if (TaggingRepository.IsTaggingInPlace(tag.ID, reportId))
                {
                    // If so there's nothing to do. We can't have the user type his tag a hundred times to 
                    // make it hit the trending list
                    return;
                }
            }

            // Now we register the hit on the tag
            TagRepository.RegisterTagHit(tag.ID);

            // And now we create the Tagging to link the tag and the report

            Tagging tagging = TaggingRepository.Create();
            tagging.TagId = tag.ID;
            tagging.ReportID = reportId;
            TaggingRepository.Add(tagging);
        }

        async Task<string> DoEncodingAsync(string uniqueFilename, int reportId)
        {
            return DoEncoding(uniqueFilename, reportId);
        }

        string DoEncoding(string uniqueFilename, int reportId)
        {
            try
            {
                CloudBlockBlob originalBlob = null;
                if (!AzureBlobService.FindBlob(AzureContainers.Videos.ToString(), uniqueFilename, out originalBlob))
                {
                    throw new Exception("DoEcoding: Unable to locate the original blob \"" + uniqueFilename + "\"");
                }

                IAsset copiedVideoAsset = null;
                try
                {
                    // We have no use for the link to this asset so don't keep the returned value
                    AzureMediaAssetService.CopyMediaAssetFromBlob(AzureBlobService.CloudBlobClient, AzureContainers.Videos, uniqueFilename, out copiedVideoAsset);
                }
                catch(Exception ex)
                {
                    throw new Exception("DoEncoding: Unable to clone BLOB to Media Services", ex);
                }

                string thumbnailUrl, encodedUrl = string.Empty;

                try
                {
                    AzureMediaAssetService.EncodeVideoAssetDefaultAndThumbnail(copiedVideoAsset, uniqueFilename, out encodedUrl, out thumbnailUrl);
                }
                catch(Exception ex)
                {
                    // If this step fails the outputAsset_ and encodingOutputAsset_ survive but we know we 
                    // can delete the copied asset
                    if (copiedVideoAsset != null) copiedVideoAsset.Delete();
                    throw new Exception("DoEncoding: Failed to encode/create thumbnail", ex);
                }

                // Remove the original storage (we've copied it so it's redundant)
                try
                {
                    AzureBlobService.RemoveBlob(originalBlob);
                }
                catch(Exception ex)
                {
                    if (copiedVideoAsset != null) copiedVideoAsset.Delete();
                    throw new Exception("DoEncoding: Failed to delete the original blob", ex);
                }

                try
                {
                    if (copiedVideoAsset != null)
                    {
                        copiedVideoAsset.Delete();
                    }
                }
                catch(Exception ex)
                {
                    throw new Exception("DoEncoding: Failed to delete the copied media asset", ex);
                }

                try
                {
                    // Okay, we're all good to update the report
                    Report report = ReportRepository.FindById(reportId);
                    // Sepcifically we need to update it's zeroth media item
                    MediaItem mediaItem = report.MediaItems[0];
                    mediaItem.MediaType = MediaItem.MediaTypes.MediaServicesTransferred;
                    mediaItem.Uri = encodedUrl;
                    mediaItem.ThumbnailUri = thumbnailUrl;
                    MediaItemRepository.Update(mediaItem);
                }
                catch(Exception ex)
                {
                    throw new Exception("DoEncoding: Failed to update the report's media item", ex);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ReportController DoEncoding failure!", ex);
            }

            return string.Empty;

        }

    }
}
