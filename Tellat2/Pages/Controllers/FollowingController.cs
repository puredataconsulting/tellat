﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    public class FollowingController : BaseController<FollowingViewModel>
    {
        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }

        //[Dependency]
        //public IUserRepository UserRepository { get; set; }

        /// <summary>
        /// /Category/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index(string id, int? sortCriterion)
        {
            try
            {
                ViewModel.Title = "Following (" + FollowerRepository.GetNSourcesForTarget(id) + ")";
                ViewModel.UsersSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
                ViewModel.Members = FollowerRepository.GetTargetsForSource(id, Utility.Utilities.GetUserSortCriterion(sortCriterion));
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }
    }
}
