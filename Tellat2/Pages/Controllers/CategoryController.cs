﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    public class CategoryController : BaseController<CategoryViewModel>
    {
        [Dependency]
        public IReportCategoryRepository ReportCategoryRepository { get; set; }

        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        /// <summary>
        /// /Category/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index(int id, int? sortCriterion)
        {
            try
            {
                 ViewModel.ReportsSortCriterion = sortCriterion.HasValue? sortCriterion.Value : -1;
                ViewModel.Category = CategoryRepository.FindById(id);
                ViewModel.Reports = ReportCategoryRepository.FindReportsByCategory(id, Utility.Utilities.GetReportSortCriterion(sortCriterion), !ViewModel.IsCurrent());
                ViewModel.PrimaryReport = ReportCategoryRepository.FindPrimaryReportByCategoryId(id);
                ViewModel.Title = ViewModel.Category.Title;

            }
            catch (RepositoryException ex)
            {
                
            }

            return View(ViewModel);
        }
    }
}
