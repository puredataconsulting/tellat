﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using System.Linq;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Identity;
using Tellat2.Pages.ViewModels;
using System.Web;
using System;

using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Tellat2.Pages.Controllers
{
    public class HomeController : BaseController<HomeViewModel>
    {
        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /Home/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ViewModel.Categories = CategoryRepository.FindAll().ToList();
                ViewModel.TopReportID = -1;
                Report topReport = ReportRepository.FindMostViewed();
                if (topReport != null)
                {
                    ViewModel.TopReportID = topReport.ID;
                }

            }
            catch (RepositoryException ex)
            {

            }
            return View(ViewModel);
        }

        /// <summary>
        /// Home/PleaseRegister
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PleaseRegister()
        {
            return View();
        }

    }
}
