﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Azure.Services.Interfaces;
using Framework2.Azure.Config;
using System;
using Framework2.Azure.Exceptions;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Core;

namespace Tellat2.Pages.Controllers
{
    public class UsersController : BaseController<UsersViewModel>
    {
        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        /// <summary>
        /// /Users/Index/Id
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ViewModel.User = this.CurrentUser;
                //TODO
                //ViewModel.IsEditableByThisUser = (ViewModel.IsLoggedIn && ViewModel.CurrentUser.IsAdministrator);
            }
            catch (RepositoryException ex)
            {

            }

            return View("User", ViewModel);
        }

        /// <summary>
        /// /Users/All
        /// </summary>
        [HttpGet]
        public ActionResult All(int? sortCriterion)
        {
            try
            {
                ViewModel.UsersSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;

                var userManager = this.UserManager;
                if(userManager != null)
                {
                    ViewModel.Users = ApplicationUserRepository.FilterSort(userManager.Users, Utility.Utilities.GetUserSortCriterion(sortCriterion));
                    ViewModel.Title = "Reporters (" + ViewModel.Users.Count + ")";
                }
            }
            catch (RepositoryException ex)
            {

            }

            return View("Users", ViewModel);
        }
    }
}
