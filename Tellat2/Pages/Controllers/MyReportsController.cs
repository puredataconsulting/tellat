﻿using System;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;

namespace Tellat2.Pages.Controllers
{
    public class MyReportsController : BaseController<MyReportsViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /MyReports/Index
        /// </summary>
        public ActionResult Index(string id, int? sortCriterion)
        {
            try
            {
                ViewModel.ReportsSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : 1;
                ViewModel.Member = this.GetUserById(id);
                ViewModel.Reports = ReportRepository.FindAllByUserId(id, Utility.Utilities.GetReportSortCriterion(sortCriterion), !ViewModel.IsCurrent(id));
                ViewModel.Title = "Reports by " + ViewModel.Member.UserName;

            }
            catch (RepositoryException)
            {

            }

            return View(ViewModel);
        }
    }
}
