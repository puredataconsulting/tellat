﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Services.Interfaces;
using System.Collections.Generic;
using Framework2.Core.Models.Core;

namespace Tellat2.Pages.Controllers
{
    public class GroupsController : BaseController<GroupsViewModel>
    {
        [Dependency]
        public IGroupRepository GroupRepository { get; set; }

        /// <summary>
        /// /Groups/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                // A group may have multiple admins so the concept of Owner is a toughie -
                // we use the GroupService to find the first admin in the list of admins and
                // call that Owner
                ViewModel.GroupsDictionary = new Dictionary<Group, ApplicationUser>();
                List<Group> groups = GroupRepository.FindAll().ToList();
                foreach(Group gp in groups)
                {
                    var query = from member in gp.GroupMembers where member.IsAdministrator select member.ApplicationUser;
                    var owner = query.FirstOrDefault<ApplicationUser>();
                    ViewModel.GroupsDictionary.Add(gp, owner);
                }
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }
    }
}
