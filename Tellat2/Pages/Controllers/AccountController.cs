﻿using System.Globalization;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tellat2.Identity;
using Framework2.Core.Models.Core;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Platform.Email.Services;
using Platform.Email.ViewModels;
using Tellat2.Emails.ViewModels;


namespace Tellat2.Pages.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        [Dependency]
        public ICountryRepository CountryRepository { get; set; }

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationUser CurrentUser
        {
            get
            {
                if (User.Identity.IsAuthenticated)
                {
                    return UserManager.FindByNameAsync(User.Identity.Name).Result;
                }
                else return null;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        private SignInHelper _helper;

        private SignInHelper SignInHelper
        {
            get
            {
                if (_helper == null)
                {
                    _helper = new SignInHelper(UserManager, AuthenticationManager);
                }
                return _helper;
            }
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = await SignInHelper.PasswordSignInEmail(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresTwoFactorAuthentication:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInHelper.HasBeenVerified())
            {
                return View("Error");
            }
            var user = await UserManager.FindByIdAsync(await SignInHelper.GetVerifiedUserIdAsync());
            if (user != null)
            {
                // To exercise the flow without actually sending codes, uncomment the following line
                //ViewBag.Status = "For DEMO purposes the current " + provider + " code is: " + await UserManager.GenerateTwoFactorTokenAsync(user.Id, provider);
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInHelper.TwoFactorSignIn(model.Provider, model.Code, isPersistent: false, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/EditAccount

        public ActionResult EditAccount()
        {

            ApplicationUser user = this.CurrentUser;
            if(user == null)
            {
                throw new Exception("!!! Account/EditAccount: No current user");
            }

            EditAccountViewModel viewModel = new EditAccountViewModel();
            DateTime dt = user.Birthday;
            viewModel.BirthDayId = dt.Day.ToString();
            viewModel.BirthMonthId = dt.Month.ToString();
            viewModel.BirthYearId = dt.Year.ToString();
            viewModel.FirstName = user.FirstName;
            viewModel.LastName = user.LastName;
            viewModel.ProfileText = user.ProfileText;
            viewModel.UserName = user.UserName;
            viewModel.IsDateOfBirthPublished = (user.PrivacyOptions & UserPrivacySettings.DateOfBirth) != 0;
            viewModel.IsGenderPublished = (user.PrivacyOptions & UserPrivacySettings.Gender) != 0;
            viewModel.IsLocationPublished = (user.PrivacyOptions & UserPrivacySettings.RoughLocation) != 0;
            viewModel.IsYearOfBirthPublished = (user.PrivacyOptions & UserPrivacySettings.YearOfBirth) != 0;

            viewModel.Countries = CountryRepository.FindAllAsSelectList(0);


            return View(viewModel);
        }
        //
        // POST: /Account/EditAccount
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAccount(EditAccountViewModel model)
        {
            // Now here's a PITA
            model.Countries = CountryRepository.FindAllAsSelectList(0);

            if (ModelState.IsValid)
            {
                ApplicationUser user = this.CurrentUser;
                if (user == null)
                {
                    throw new Exception("!!! Account/EditAccount: No current user");
                }

                // Add the additional details
                user.CountryId = model.CountryId;
                user.ProfileText = model.ProfileText;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Birthday = new DateTime(int.Parse(model.BirthYearId), int.Parse(model.BirthMonthId), int.Parse(model.BirthDayId));
                user.GenderType = string.IsNullOrEmpty(model.GenderId) ? GenderTypes.Male : (GenderTypes)(int.Parse(model.GenderId));

                UserPrivacySettings privacyOptions = UserPrivacySettings.None;
                if (model.IsYearOfBirthPublished)
                {
                    privacyOptions |= UserPrivacySettings.YearOfBirth;
                }
                if (model.IsDateOfBirthPublished)
                {
                    privacyOptions |= UserPrivacySettings.DateOfBirth;
                }
                if (model.IsGenderPublished)
                {
                    privacyOptions |= UserPrivacySettings.Gender;
                }
                if (model.IsLocationPublished)
                {
                    privacyOptions |= UserPrivacySettings.RoughLocation;
                }
                user.PrivacyOptions = privacyOptions;
                var result = await UserManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Profile", new { id = await SignInHelper.GetVerifiedUserIdAsync() });
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }



        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            RegistrationViewModel viewModel = new RegistrationViewModel();

            viewModel.Countries = CountryRepository.FindAllAsSelectList(0);

            return View(viewModel);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegistrationViewModel model)
        {
                  // This doesn't get passed around so it gets nulled in transit. PITA.
                model.Countries = CountryRepository.FindAllAsSelectList(0);
          if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.UserName, Email = model.EmailAddress };


                // Add the additional details
                user.CountryId = model.CountryId;
                user.ProfileText = model.ProfileText;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Birthday = new DateTime(int.Parse(model.BirthYearId), int.Parse(model.BirthMonthId), int.Parse(model.BirthDayId));
                user.GenderType = (GenderTypes)(int.Parse(model.GenderId));

                UserPrivacySettings privacyOptions = UserPrivacySettings.None;
                if (model.IsYearOfBirthPublished)
                {
                    privacyOptions |= UserPrivacySettings.YearOfBirth;
                }
                if (model.IsDateOfBirthPublished)
                {
                    privacyOptions |= UserPrivacySettings.DateOfBirth;
                }
                if (model.IsGenderPublished)
                {
                    privacyOptions |= UserPrivacySettings.Gender;
                }
                if (model.IsLocationPublished)
                {
                    privacyOptions |= UserPrivacySettings.RoughLocation;
                }
                user.PrivacyOptions = privacyOptions;
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // We need to wire our user up with a default media item
                    MediaItem mediaItem = MediaItemRepository.Create();
                    mediaItem.MediaType = MediaItem.MediaTypes.Content;
                    mediaItem.ApplicationUserId = user.Id;
                    mediaItem.Uri = "generic-person.png";
                    mediaItem.ThumbnailUri = "profile-blue.png";
                    MediaItemRepository.Add(mediaItem);

                    var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    RegistrationEmailViewModel emailModel = new RegistrationEmailViewModel()
                    {
                        Link = callbackUrl
                    };

                    string html = EmailService.FillTemplateFromModel("~/Emails/Views/Registration2.cshtml", emailModel);
                    await UserManager.SendEmailAsync(user.Id, "Confirm your account", html);
                    ViewBag.Link = callbackUrl;
                    return View("DisplayEmail");
                }
                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            if (result.Succeeded)
            {
                return View("ConfirmEmail");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                ForgotPasswordEmailViewModel emailModel = new ForgotPasswordEmailViewModel()
                {
                    Link = callbackUrl
                };

                string html = EmailService.FillTemplateFromModel("~/Emails/Views/PasswordReset.cshtml", emailModel);
                await UserManager.SendEmailAsync(user.Id, "Reset your Tellat password", html);
                ViewBag.Link = callbackUrl;
                return View("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl)
        {
            var userId = await SignInHelper.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            // Generate the token and send it
            if (!ModelState.IsValid)
            {
                return View();
            }

            if (!await SignInHelper.SendTwoFactorCode(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInHelper.ExternalSignIn(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresTwoFactorAuthentication:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInHelper.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}