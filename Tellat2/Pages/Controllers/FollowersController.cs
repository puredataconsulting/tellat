﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    public class FollowersController : BaseController<FollowersViewModel>
    {
        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }


        /// <summary>
        /// /Category/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index(string id, int? sortCriterion)
        {
            try
            {
                ViewModel.Title = "Followers (" + FollowerRepository.GetNTargetsForSource(id) + ")";
                ViewModel.UsersSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
                ViewModel.Members = FollowerRepository.GetSourcesForTarget(id, Utility.Utilities.GetUserSortCriterion(sortCriterion));
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }
    }
}
