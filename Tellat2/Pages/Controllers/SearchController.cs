﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    public class SearchController : BaseController<SearchViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /Search/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ViewModel.Reports = ReportRepository.FindAll().ToList();
            }
            catch (RepositoryException ex)
            {
                
            }

            return View(ViewModel);
        }
    }
}
