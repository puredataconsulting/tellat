﻿using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using Framework2.Core.Models.Core;
using System;
using Tellat2.Emails.ViewModels;
using Platform.Email.Services;
using System.Threading.Tasks;

namespace Tellat2.Pages.Controllers
{
    public class ProfileController : BaseController<ProfileViewModel>
    {

        [Dependency]
        public IApplicationUserRepository ApplicationUserRepository { get; set; }

        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }

        [Dependency]
        public IGroupRepository GroupRepository { get; set; }


        [Dependency]
        public INotificationRepository NotificationRepository { get; set; }

        /// <summary>
        /// /Profile/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                ViewModel.ProfiledUser = this.GetUserById(id);
                ViewModel.IsProfiledUserCurrent = id == CurrentUserId;
            }
            else
            {
                ViewModel.IsProfiledUserCurrent = false;
                if (ViewModel.ProfiledUser == null)
                {
                    if (ViewModel.CurrentUserId != null)
                    {
                        ViewModel.CurrentUser = this.CurrentUser;
                        ViewModel.ProfiledUser = ViewModel.CurrentUser;
                        ViewModel.IsProfiledUserCurrent = true;
                    }
                }
                else
                {
                    if (ViewModel.CurrentUserId != null)
                    {
                        if (ViewModel.ProfiledUser.Id == ViewModel.CurrentUserId)
                        {
                            ViewModel.IsProfiledUserCurrent = true;

                        }
                    }
                }
            }

            ViewModel.UserOwnerComponentViewModel = new Components.ViewModels.UserOwnerComponentViewModel();
            if (ViewModel.ProfiledUser != null)
            {
                ViewModel.UserOwnerComponentViewModel.UserId = ViewModel.ProfiledUser.Id;
                ViewModel.UserOwnerComponentViewModel.IsUserDeleteableByThisUser = ViewModel.IsCurrentUserAdmin();
                ViewModel.UserOwnerComponentViewModel.IsUserEditableByThisUser = ViewModel.IsCurrent(ViewModel.ProfiledUser.Id);
            }
            else
            {
                ViewModel.UserOwnerComponentViewModel.IsUserDeleteableByThisUser = false;
                ViewModel.UserOwnerComponentViewModel.IsUserEditableByThisUser = false;
            }
            
            // We can get here from the My Profile link on the footer, so if we're not logged in we need to react
            if (ViewModel.ProfiledUser == null) return RedirectToAction("PleaseRegister", "Home");
            else return View("Index", ViewModel);
        }


        /// <summary>
        /// /Users/Edit
        /// </summary>
        public ActionResult Delete(string id)
        {
            try
            {
                 ApplicationUser user = this.GetUserById(id);

                // Unhook the user from all relationships
                 ApplicationUserRepository.DecoupleUser(user);

                 this.UserManager.DeleteAsync(user);

            }
            catch (RepositoryException ex)
            {
            }

            return RedirectToAction("Index", "Users");
        }


        public async Task<ActionResult> AcceptNotification(int id)
        {
            try
            {
                Notification notification = NotificationRepository.FindById(id);
                // We need to join this dude to the quoted group
                GroupMemberRepository.Join(notification.Sender, int.Parse(notification.Reference), false);

                // Send him an email
                JoinGroupEmailViewModel emailModel = new JoinGroupEmailViewModel()
                {
                    GroupName = GroupRepository.FindById(int.Parse(notification.Reference)).Title
                };

                string html = EmailService.FillTemplateFromModel("~/Emails/Views/JoinGroupAccept.cshtml", emailModel);
                await UserManager.SendEmailAsync(notification.Sender, "Re your application to join a Tellat Newsgroup", html);

                // Remove the notification
                NotificationRepository.RemoveNotification(id);

                return RedirectToAction("Index", "Profile");

            }
            catch (Exception ex)
            {
                throw new Exception("!!!NotificationsComponentController.Accept", ex);
            }
        }

        public async Task<ActionResult> RejectNotification(int id)
        {
            try
            {
                Notification notification = NotificationRepository.FindById(id);

                // Send him an email
                JoinGroupEmailViewModel emailModel = new JoinGroupEmailViewModel()
                {
                    GroupName = GroupRepository.FindById(int.Parse(notification.Reference)).Title
                };

                string html = EmailService.FillTemplateFromModel("~/Emails/Views/JoinGroupReject.cshtml", emailModel);
                await UserManager.SendEmailAsync(notification.Sender, "Re your application to join a Tellat Newsgroup", html);


                // Remove the notification
                NotificationRepository.RemoveNotification(id);

                return RedirectToAction("Index", "Profile");

            }
            catch (Exception ex)
            {
                throw new Exception("!!!NotificationsComponentController.Accept", ex);
            }
        }
    }
}
