﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Pages.Controllers
{
    [Authorize]
    public class CategoriesAdminController : BaseController<CategoryViewModel>
    {
        //[Dependency]
        //public ICategoryRepository CategoryRepository { get; set; }

        //[Dependency]
        //public IMediaItemRepository MediaItemRepository { get; set; }

        //[Dependency]
        //public ICountryRepository CountryRepository { get; set; }


        /// <summary>
        /// /Category/Index
        /// </summary>
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                //TODO This is a placeholder
            }
            catch (RepositoryException ex)
            {

            }

            return View(ViewModel);
        }

    }
}