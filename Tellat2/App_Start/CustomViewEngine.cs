﻿using System.Web.Mvc;

namespace Tellat2
{
    /// <summary>
    /// A custom view engine allows us to register non-standard view paths 
    /// </summary>
    public class CustomViewEngine : RazorViewEngine
    {
        public CustomViewEngine()
        {
            var viewLocations = new[] 
            {  
                "~/Components/Views/{1}/{0}.cshtml",
                "~/Editors/Views/{1}/{0}.cshtml",
                "~/Pages/Views/{1}/{0}.cshtml",
                "~/Pages/Views/Shared/{0}.cshtml",
                "~/Mobile/Views/{1}/{0}.cshtml"
            };

            PartialViewLocationFormats = viewLocations;
            ViewLocationFormats = viewLocations;
        }
    }
}