﻿using System.Web.Optimization;

namespace Tellat2
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));



            #region JQuery

            // Base JQuery
            bundles.Add(new ScriptBundle("~/Bundles/jquery-base").Include(
                "~/Scripts/jquery-{version}.js"));

            // Unobtrusive JQuery
            bundles.Add(new ScriptBundle("~/Bundles/jquery-val").Include(
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate*"));

            #endregion

            #region Components

            // Fancybox
            bundles.Add(new ScriptBundle("~/Bundles/fancybox").Include(
                "~/Scripts/jquery.fancybox.js"));

            bundles.Add(new StyleBundle("~/Content/fancybox").Include(
                "~/Content/fancybox.css"));

            // Fine Uploader
            bundles.Add(new ScriptBundle("~/Bundles/fineuploader").Include(
                "~/Scripts/jquery.fineuploader-{version}.js"));

            // JWPlayer
            bundles.Add(new ScriptBundle("~/Bundles/jwplayer").Include(
                "~/Scripts/jwplayer.js",
                "~/Scripts/jwplayer.html5.js"));

            // Chart.js
            bundles.Add(new ScriptBundle("~/Bundles/chartjs").Include(
                "~/Scripts/chart.min.js"));

            // Rater
            bundles.Add(new ScriptBundle("~/Bundles/rater").Include(
                "~/Scripts/jquery.rater.js"));

            #endregion

            #region Desktop

            // Core
            bundles.Add(new StyleBundle("~/Content/desktop-core").Include(
                "~/Content/master.css",
                "~/Content/all.css",
                "~/Content/components.css",
                "~/Content/bootstrapOffcuts.css")); // Bootstrap is the Twitter default styling

            bundles.Add(new StyleBundle("~/Content/account").Include(
                "~/Content/pageAccount.css"));

            bundles.Add(new StyleBundle("~/Content/group").Include(
                "~/Content/pageGroup.css"));

            bundles.Add(new StyleBundle("~/Content/groups").Include(
                "~/Content/pageGroups.css"));

            bundles.Add(new StyleBundle("~/Content/home").Include(
                "~/Content/pageHome.css"));

            bundles.Add(new StyleBundle("~/Content/registration").Include(
                "~/Content/pageRegistration.css"));

            bundles.Add(new StyleBundle("~/Content/upload").Include(
                "~/Content/pageUpload.css"));

            bundles.Add(new StyleBundle("~/Content/profile").Include(
                "~/Content/pageProfile.css"));

            bundles.Add(new StyleBundle("~/Content/report").Include(
                "~/Content/fineloader.css",
                "~/Content/pageReport.css"));

            bundles.Add(new StyleBundle("~/Content/users").Include(
                "~/Content/pageUsers.css"));

            bundles.Add(new StyleBundle("~/Content/guide").Include(
                "~/Content/pageGuide.css"));

            bundles.Add(new StyleBundle("~/Content/rater").Include(
                "~/Content/raterStyle.css"));

            bundles.Add(new StyleBundle("~/Content/componentCategory").Include(
                "~/Content/componentCategory.css"));

            bundles.Add(new StyleBundle("~/Content/componentComment").Include(
                "~/Content/componentComment.css"));

            bundles.Add(new StyleBundle("~/Content/componentDashboard").Include(
                "~/Content/componentDashboard.css"));

            bundles.Add(new StyleBundle("~/Content/componentFavourites").Include(
                "~/Content/componentFavourites.css"));

            bundles.Add(new StyleBundle("~/Content/componentGroups").Include(
                "~/Content/componentGroups.css"));

            bundles.Add(new StyleBundle("~/Content/componentMembers").Include(
                "~/Content/componentMembers.css"));

            bundles.Add(new StyleBundle("~/Content/componentNavigation").Include(
                "~/Content/componentNavigation.css"));

            bundles.Add(new StyleBundle("~/Content/componentNotifications").Include(
                "~/Content/componentNotifications.css"));

            bundles.Add(new StyleBundle("~/Content/componentReport").Include(
                "~/Content/componentReport.css"));

            bundles.Add(new StyleBundle("~/Content/componentReportContent").Include(
                "~/Content/componentReportContent.css"));

            bundles.Add(new StyleBundle("~/Content/componentReportOwner").Include(
                "~/Content/componentReportOwner.css"));

            bundles.Add(new StyleBundle("~/Content/componentReports").Include(
                "~/Content/componentReports.css"));

            bundles.Add(new StyleBundle("~/Content/componentSidebar").Include(
                "~/Content/componentSidebar.css"));

            bundles.Add(new StyleBundle("~/Content/componentSmallMember").Include(
                "~/Content/componentSmallMember.css"));

            bundles.Add(new StyleBundle("~/Content/componentSmallReport").Include(
                "~/Content/componentSmallReport.css"));

            bundles.Add(new StyleBundle("~/Content/componentSocial").Include(
                "~/Content/componentSocial.css"));

            bundles.Add(new StyleBundle("~/Content/componentTrending").Include(
                "~/Content/componentTrending.css"));

            bundles.Add(new StyleBundle("~/Content/componentUser").Include(
                "~/Content/componentUser.css"));

            bundles.Add(new StyleBundle("~/Content/componentUserOwner").Include(
                "~/Content/componentUserOwner.css"));

            bundles.Add(new StyleBundle("~/Content/componentUserReports").Include(
                "~/Content/componentUserReports.css"));

            bundles.Add(new StyleBundle("~/Content/componentVideo").Include(
                "~/Content/componentVideo.css"));

            #endregion
        }
    }
}
