using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Tellat2.Pages.Controllers;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Repositories.Core;
using Framework2.Core.Services.Interfaces;
using Framework2.Core.Services;
using Framework2.Azure.Services.Interfaces;
using Framework2.Azure.Services;
using Framework2.Core.Context;
using Platform.Email.Services.Interfaces;
using Platform.Email.Services;
using Framework2.Core.Models.Core;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Tellat2.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // See http://stackoverflow.com/questions/22846327/the-type-iuserstore1-does-not-have-an-accessible-constructor
            container.RegisterType<AccountController>(new InjectionConstructor());
            // These resolve the   "is an interface and cannot be constructed" throws
            container.RegisterType<RolesAdminController>(new InjectionConstructor());
            container.RegisterType<UsersAdminController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            // Register the context manager
            container.RegisterType<IContextManager, ContextManager>(new ContextLifetimeManager());

            // Register the repositories
            container.RegisterType<ILogRepository, LogRepository>();
            container.RegisterType<ICommentRepository, CommentRepository>();
            container.RegisterType<IGroupRepository, GroupRepository>();
            container.RegisterType<IFollowerRepository, FollowerRepository>();
            container.RegisterType<IMediaItemRepository, MediaItemRepository>();
            container.RegisterType<IReportRepository, ReportRepository>();
            container.RegisterType<IAlbumRepository, AlbumRepository>();
            container.RegisterType<IGroupMemberRepository, GroupMemberRepository>();
            container.RegisterType<ICategoryRepository, CategoryRepository>();
            container.RegisterType<ICountryRepository, CountryRepository>();
            container.RegisterType<IFavouriteRepository, FavouriteRepository>();
            container.RegisterType<ITaggingRepository, TaggingRepository>();
            container.RegisterType<ITagRepository, TagRepository>();
            container.RegisterType<IReportCategoryRepository, ReportCategoryRepository>();
            container.RegisterType<IGroupReportRepository, GroupReportRepository>();
            container.RegisterType<INotificationRepository, NotificationRepository>();

            // The app user repository is a different kind of animal - it doesn't derive from BaseRepository
            container.RegisterType<IApplicationUserRepository, ApplicationUserRepository>();

            // Additional services
            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IAzureBlobService, AzureBlobService>();
            container.RegisterType<IAzureMediaAssetService, AzureMediaAssetService>();

            // Register the model related services
            //container.RegisterType<ILogService, LogService>();
            container.RegisterType<ICommentService, CommentService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IGroupService, GroupService>();
            container.RegisterType<IFollowerService, FollowerService>();
            container.RegisterType<IMediaItemService, MediaItemService>();
            container.RegisterType<IReportService, ReportService>();
            container.RegisterType<IAlbumService, AlbumService>();
            container.RegisterType<IGroupMemberService, GroupMemberService>();
            //container.RegisterType<ICategoryService, CategoryService>();
            //container.RegisterType<ICountryService, CountryService>();
            container.RegisterType<IFavouriteService, FavouriteService>();
            container.RegisterType<ITaggingService, TaggingService>();
            container.RegisterType<ITagService, TagService>();
        }
    }
}
