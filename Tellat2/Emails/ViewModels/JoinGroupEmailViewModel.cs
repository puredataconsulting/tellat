﻿using Platform.Email.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tellat2.Emails.ViewModels
{
    public class JoinGroupEmailViewModel : BaseEmailViewModel
    {
        public string GroupName { get; set; }
        public string RequesterName { get; set; }
    }
}