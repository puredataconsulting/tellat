﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class FavouritesComponentViewModel : SortablesComponentViewModel
    {
        public List<Report> Favourites { get; set;  }

        public List<SelectListItem> SortCriteria
        {
            get
            {
                var query = from item in Utility.Utilities.ReportSortsTitles orderby item.Key select new SelectListItem { Text = item.Value, Value = item.Key.ToString(), Selected = item.Key == _reportsSortCriterion };
                return query.ToList();
            }
        }

    }
}