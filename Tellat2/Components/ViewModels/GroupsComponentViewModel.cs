﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the groups component controller with data binding
    /// </summary>
    public class GroupsComponentViewModel : BaseViewModel
    {
        public Dictionary<Group, ApplicationUser> GroupsDictionary { get; set; }
    }
}