﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class DashboardComponentViewModel : BaseViewModel
    {

        public ApplicationUser Member { get; set; }

        /// <summary>
        /// How many items has this user rated?
        /// </summary>
        public int NHasRated { get; set; }

        /// <summary>
        /// How many items are un/published
        /// </summary>
        public int NPublished { get; set; }
        public int NUnpublished { get; set; }

        /// <summary>
        /// How many of my reports have been rated by others?
        /// </summary>
        public int NRatedBy { get; set; }

        /// <summary>
        /// How many viewings have my reports had?
        /// </summary>
        public int NViewings { get; set; }

        /// <summary>
        /// How many comments have my reports had?
        /// </summary>
        public int NCommentedOn { get; set; }

        /// <summary>
        /// How many am I following?
        /// </summary>
        public int NFollowSources { get; set; }

        /// <summary>
        /// How many are following me?
        /// </summary>
        public int NFollowTargets { get; set; }

        public int NFavourites { get; set; }
    }
}