﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class UserOwnerComponentViewModel : BaseViewModel
    {

        public string UserId { get; set; }

        public bool IsUserEditableByThisUser { get; set; }
        public bool IsUserDeleteableByThisUser { get; set; }

        public bool IsModifierPossible
        {
            get
            {
                return IsUserEditableByThisUser || IsUserDeleteableByThisUser;
            }
        }
    }
}