﻿using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the youtube component controller with data binding
    /// </summary>
    public class YouTubeComponentViewModel : BaseViewModel
    {
        public string Url { get; set; }
    }
}