﻿using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the image viewer component controller with data binding
    /// </summary>
    public class ImageComponentViewModel : BaseViewModel
    {
        public MediaItem Image { get; set; }
    }
}