﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the category component controller with data binding
    /// </summary>
    public class CommentComponentViewModel : BaseViewModel
    {
        public int ReportId { get; set; }
        public string Text { get; set; }

        public List<Comment> Comments { get; set; }
    }
}