﻿using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the report component controller with data binding
    /// </summary>
    public class ReportContentComponentViewModel : BaseViewModel
    {
        public Report Report { get; set; }

        public int NComments { get; set;  }

        
    }
}