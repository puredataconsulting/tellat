﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;
using System.Web.Mvc;
using System.Linq;


namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the my reports component controller with data binding
    /// </summary>
    public class ReportsComponentViewModel : SortablesComponentViewModel
    {
        public List<Report> Reports { get; set; }


        public List<SelectListItem> SortCriteria
        {
            get
            {
                var query = from item in Utility.Utilities.ReportSortsTitles orderby item.Key select new SelectListItem { Text = item.Value, Value = item.Key.ToString(), Selected = item.Key == _reportsSortCriterion };
                return query.ToList();
            }
        }
    }
}