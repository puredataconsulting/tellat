﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class ReportOwnerComponentViewModel : BaseViewModel
    {
        public bool IsFeatured { get; set; }

        public int ReportId { get; set; }

        public bool IsReportEditableByThisUser { get; set; }
        public bool IsReportDeleteableByThisUser { get; set; }

        public bool IsModifierPossible
        {
            get
            {
                return IsReportEditableByThisUser || IsReportDeleteableByThisUser;
            }
        }
    }
}