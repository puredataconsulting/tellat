﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class SortablesComponentViewModel  : BaseViewModel
    {
        protected int _reportsSortCriterion;
        public int ReportsSortCriterion
        {
            get
            {
                return _reportsSortCriterion;
            }
            set
            {
                _reportsSortCriterion = value;
            }
        }
         
        protected int _favouritesSortCriterion;
        public int FavouritesSortCriterion
        {
            get
            {
                return _favouritesSortCriterion;
            }
            set
            {
                _favouritesSortCriterion = value;
            }
        }

        protected int _usersSortCriterion;
        public int UsersSortCriterion
        {
            get
            {
                return _usersSortCriterion;
            }
            set
            {
                _usersSortCriterion = value;
            }
        }

        public SortablesComponentViewModel()
        {
            // We're using neg numbers to imply no fixed sort criterion
            ReportsSortCriterion = -1;
            UsersSortCriterion = -1;
        }

        public string Title { get; set; }
    }
}