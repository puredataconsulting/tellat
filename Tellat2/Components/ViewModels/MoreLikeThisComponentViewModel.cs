﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the more-like-this component controller with data binding
    /// </summary>
    public class MoreLikeThisComponentViewModel : SortablesComponentViewModel
    {
        public List<Report> Reports { get; set; } 
    }
}