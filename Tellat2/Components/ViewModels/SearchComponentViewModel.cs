﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the navigation component controller with data binding
    /// </summary>
    public class SearchComponentViewModel : BaseViewModel
    {
        public string searchTerms { get; set; }
    }
}