﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the category component controller with data binding
    /// </summary>
    public class CategoryComponentViewModel : BaseViewModel
    {
        public Category Category { get; set; }

        public Report Featured { get; set; }

        public List<Report> Reports { get; set; } 
    }
}