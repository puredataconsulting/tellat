﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class SidebarComponentViewModel : BaseViewModel
    {
        public List<Report> Reports { get; set; }
    }
}