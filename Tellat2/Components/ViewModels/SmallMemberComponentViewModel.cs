﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    public class SmallMemberComponentViewModel : BaseViewModel
    {
        public ApplicationUser Member { get; set; }


        public int NPublishedReports { get; set; }

        public int NRatedBy { get; set; }

        public int NComments { get; set; }

        public string FollowMessage { get; set; }

    }
}