﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;
using System.Web.Mvc;
using System.Linq;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the members component controller with data binding
    /// </summary>
    public class MembersComponentViewModel : SortablesComponentViewModel
    {
        public List<ApplicationUser> Members { get; set; }

        public List<SelectListItem> SortCriteria
        {
            get
            {
                var query = from item in Utility.Utilities.UserSortsTitles orderby item.Key select new SelectListItem { Text = item.Value, Value = item.Key.ToString(), Selected = item.Key == _usersSortCriterion };
                return query.ToList();
            }
        }
    }
}