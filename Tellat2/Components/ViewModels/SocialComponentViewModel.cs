﻿using Framework2.Core.Models.Core;
using System.Collections;
using System.Collections.Generic;
using System.Web.Mvc;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the social component controller with data binding
    /// </summary>
    public class SocialComponentViewModel : BaseViewModel
    {

        public int ID { get; set; }

        public int NComments { get; set; }

        public int NViewings { get; set; }

        // You can only rate something once, and you can only rate if you're logged in
        public bool IsRateableByThisUser { get; set; }

        public bool IsRatedByThisUser { get; set; }

        public bool IsFavouritedByThisUser { get; set; }

        public bool CanBeFavouritedByThisUser { get; set; }

        public int ThisUserRating { get; set; }

        public float AverageRating { get; set; }

        public int NRatings { get; set; }

        public int NFavourites { get; set; }

        public int NewRating { get; set; }

        public Report Report { get; set; }

        public ApplicationUser Author { get; set; }
    }
}