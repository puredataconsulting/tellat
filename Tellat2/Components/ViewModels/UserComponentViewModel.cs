﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;
using Tellat2.Pages.ViewModels;
using System;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the navigation component controller with data binding
    /// </summary>
    public class UserComponentViewModel : SortablesComponentViewModel
    {
        public List<Report> Reports { get; set; }


        public List<Report> Favourites { get; set; }

        public List<Notification> Notifications { get; set; }

        public bool IsProfiledUserCurrent { get; set; }

        public ApplicationUser ProfiledUser { get; set; }

        public GroupsViewModel GroupsViewModel { get; set; }

        public int NPublishedReports { get; set; }

        public int NRatedBy { get; set; }

        public int NFavourites { get; set; }

        public int NFollowers { get; set; }

        public int NFollowing { get; set; }

        public int NTotalViewings { get; set; }

        public int Age { get; set; }

        public DateTime DateOfBirth { get; set; }

        public GenderTypes Gender { get; set; }
      
    }
}