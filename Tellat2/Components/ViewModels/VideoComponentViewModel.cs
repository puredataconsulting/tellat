﻿using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Tellat2.Base;

namespace Tellat2.Components.ViewModels
{
    /// <summary>
    /// Provides the video viewer component controller with data binding
    /// </summary>
    public class VideoComponentViewModel : BaseViewModel
    {
        public Report Report { get; set; }
        public List<Report> Reports { get; set; }

    }
}