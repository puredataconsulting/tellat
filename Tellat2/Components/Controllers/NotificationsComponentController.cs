﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class NotificationsComponentController : BaseController<NotificationsComponentViewModel>
    {
        [Dependency]
        public IGroupRepository GroupRepository { get; set; }

        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }



        /// <summary>
        /// /NotificationsComponent/Index
        /// </summary>
        public async Task<ActionResult> Index(List<Notification> notifications)
        {
            try
            {
                ViewModel.Notifications = notifications;
                foreach(var note in ViewModel.Notifications)
                {
                    // Grab a name for each notification's sender
                    var user = UserManager.FindByIdAsync(note.Sender).Result;
                    note.SenderName = user.UserName;
                    // Grab a name for the group too
                    note.GroupName = GroupRepository.FindById(int.Parse(note.Reference)).Title;
                }
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }

    }
}