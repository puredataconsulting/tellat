﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;
using System;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the group member functionality in to a re-useable component
    /// </summary>
    public class MembersComponentController : BaseController<MembersComponentViewModel>
    {
        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }


        /// <summary>
        /// /MembersComponent/Index
        /// </summary>
        public ActionResult Index(List<ApplicationUser> users, int? sortCriterion, string title)
        {
            try
            {
                ViewModel.Title = title;
                ViewModel.UsersSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
                ViewModel.Members = users;
            }
            catch (RepositoryException ex)
            {
                throw new Exception("!!!", ex);
            }

            return PartialView("_Index", ViewModel);
        }

        [Authorize]
        public ActionResult Follow(string id)
        {
            // Making this into a toggle - if you were following, after this you're not, and vice versa
            string sourceId = null;
            try
            {

                sourceId = ViewModel.CurrentUserId;
                if (FollowerRepository.IsFollowing(sourceId, id))
                {
                    // Unfollow
                    FollowerRepository.RemoveFollowing(sourceId, id);
                }
                else
                {
                    // Follow                
                    FollowerRepository.CreateFollowing(sourceId, id);
                }
            }
            catch (RepositoryException ex)
            {
                throw new Exception("!!!", ex);
            }
            return RedirectToAction("Index", "Profile", new { id = sourceId });
    }
    }
}