﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class DashboardComponentController : BaseController<DashboardComponentViewModel>
    {

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }


        /// <summary>
        /// /DashboardComponent/Index
        /// </summary>
        public ActionResult Index(string id)
        {
            if (string.IsNullOrEmpty(id))
            {

            }
            else
            {
                try
                {
                    // Use Identity to hand us our user
                    ApplicationUser applicationUser = this.CurrentUser;

                    ViewModel.Member = applicationUser;
                    ViewModel.NFavourites = FavouriteRepository.GetNFavouritesByUser(id);

                    ViewModel.NPublished = ReportRepository.GetNPublished(id);
                    ViewModel.NUnpublished = ReportRepository.GetNUnpublished(id);
                    ViewModel.NViewings = ReportRepository.CountViewingsForUser(id);

                    ViewModel.NRatedBy = ReportRepository.CountRatingsForUser(id);

                    ViewModel.NFollowSources = FollowerRepository.GetNTargetsForSource(id);
                    ViewModel.NFollowTargets = FollowerRepository.GetNSourcesForTarget(id);

                    // Hmm - this is a helluva database hit
                    List<Report> myReports = ReportRepository.FindAllByUserId(id, ReportSortCriterion.None, !ViewModel.IsCurrent(id));
                    ViewModel.NCommentedOn = 0;
                    foreach (Report report in myReports)
                    {
                        ViewModel.NCommentedOn += CommentRepository.GetNComments(report.ID);
                    }
                }
                catch (RepositoryException)
                {

                }
            }
            return PartialView("_Index", ViewModel);
        }
    }
}