﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the video viewer functionality in to a re-useable component
    /// </summary>
    public class VideoComponentController : BaseController<VideoComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /VideoComponent/Index
        /// </summary>
        public ActionResult Index(int reportId)
        {
            try
            {
                ViewModel.Report = ReportRepository.FindById(reportId);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
