﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class SmallMemberComponentController : BaseController<SmallMemberComponentViewModel>
    {

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }

        public ActionResult Index(string id)
        {
            try
            {
                if(ViewModel.IsLoggedIn)
                {
                    string currentUserId = ViewModel.CurrentUserId;
                    if (FollowerRepository.IsFollowing(currentUserId, id))
                    {
                        ViewModel.FollowMessage = "unfollow";
                    }
                    else ViewModel.FollowMessage = "follow";
                }
                ViewModel.Member = this.GetUserById(id);
                ViewModel.NPublishedReports = ReportRepository.GetNPublished(id);
                ViewModel.NRatedBy = ReportRepository.CountRatingsForUser(id);

            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}