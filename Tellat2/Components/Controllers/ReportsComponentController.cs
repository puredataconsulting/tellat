﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the my reports functionality in to a re-useable component
    /// </summary>
    public class ReportsComponentController : BaseController<ReportsComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /ReportsComponent/Index
        /// </summary>
        public ActionResult Index(List<Report> reports, int? sortCriterion, string title)
        {
            ViewModel.Title = title;
            ViewModel.ReportsSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
            ViewModel.Reports = reports;
            return PartialView("_Index", ViewModel);
        }
    }
}
