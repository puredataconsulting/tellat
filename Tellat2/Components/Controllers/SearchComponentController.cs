﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the search functionality in to a reuseable component
    /// </summary>
    public class SearchComponentController : BaseController<SearchComponentViewModel>
    {
        /// <summary>
        /// /SearchComponent/Index
        /// </summary>
        public ActionResult Index()
        {
            
            return PartialView("_Index", ViewModel);
        }

    }
}
