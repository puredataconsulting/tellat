﻿using Framework2.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class ReportOwnerComponentController : BaseController<ReportOwnerComponentViewModel>
    {

        /// <summary>
        /// /ReportOwnerComponent/Index
        /// </summary>
        public ActionResult Index(ReportOwnerComponentViewModel model)
        {
            try
            {

            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", model);
        }
    }
}