﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Tellat2.Pages.ViewModels;
using System.Collections.Generic;
using Framework2.Core.Models.Core;
using Framework2.Azure.Services.Interfaces;
using Framework2.Azure.Config;
using Framework2.Azure.Exceptions;
using System;
using Framework2.Azure.Services;
using Framework2.Core.Extensions;

using Tellat2.Extensions;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the commenting functionality in to a re-useable component
    /// </summary>
    public class UserComponentController : BaseController<UserComponentViewModel>
    {
        
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }

        [Dependency]
        public IGroupRepository GroupRepository { get; set; }

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }

        [Dependency]
        public IAzureBlobService AzureBlobService { get; set; }

        [Dependency]
        public INotificationRepository NotificationRepository { get; set; }

        /// <summary>
        /// /UserComponent/Index
        /// </summary>
        public ActionResult Index(string id, int? sortCriterion)
        {
            try
            {
                 ViewModel.ReportsSortCriterion = sortCriterion.HasValue? sortCriterion.Value : -1;
                ApplicationUser currentUser = this.CurrentUser;
                ViewModel.ProfiledUser = this.GetUserById(id);
                ViewModel.NPublishedReports = ReportRepository.GetNPublished(id);
                ViewModel.NRatedBy = ReportRepository.CountRatingsForUser(id);
                ViewModel.NFavourites = FavouriteRepository.GetNFavouritesByUser(id);
                ViewModel.NFollowers = FollowerRepository.GetNSourcesForTarget(id);
                ViewModel.NFollowing = FollowerRepository.GetNTargetsForSource(id);
                ViewModel.NTotalViewings = ReportRepository.CountViewingsForUser(id);
                ViewModel.Age = (int)((DateTime.Now - ViewModel.ProfiledUser.Birthday).TotalDays / 365);
                ViewModel.DateOfBirth = ViewModel.ProfiledUser.Birthday;
                ViewModel.Gender = ViewModel.ProfiledUser.GenderType;
                
                ViewModel.IsProfiledUserCurrent = false;
                if (ViewModel.ProfiledUser == null)
                {
                    if (ViewModel.IsLoggedIn)
                    {
                        ViewModel.ProfiledUser = currentUser;
                        ViewModel.IsProfiledUserCurrent = true;
                    }
                }
                else
                {
                    if (ViewModel.IsLoggedIn)
                    {
                        if (ViewModel.ProfiledUser.Id == ViewModel.CurrentUserId)
                        {
                            ViewModel.IsProfiledUserCurrent = true;

                        }
                    }
                }
                // Populate a sub model to carry Group and Owner data
                ViewModel.GroupsViewModel = new GroupsViewModel();
                ViewModel.GroupsViewModel.GroupsDictionary = new Dictionary<Group, ApplicationUser>();
                List<Group> groups = GroupMemberRepository.FindGroupsByUserId(ViewModel.ProfiledUser.Id);
                foreach (Group gp in groups)
                {
                    List<ApplicationUser> admins = GroupMemberRepository.FindOwnersByGroupId(gp.ID);
                    ViewModel.GroupsViewModel.GroupsDictionary.Add(gp, admins[0]);
                }

                ViewModel.Reports = ReportRepository.FindAllByUserId(id, Utility.Utilities.GetReportSortCriterion(sortCriterion), !ViewModel.IsCurrent(id));
                ViewModel.Favourites = FavouriteRepository.GetFavouriteReportsByUser(id);
                ViewModel.Notifications = NotificationRepository.SeekNotificationsForUser(id, NotificationTypes.JoinGroupRequest);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
        /// <summary>
        /// /CommentComponent/Post
        /// </summary>
        public ActionResult Post(UserComponentViewModel model)
        {
            try
            {
            }
            catch (RepositoryException ex)
            {

            }

            return PartialView("_Index", model);
        }


        /// <summary>
        /// /Users/Upload
        /// </summary>  
        //[HttpPost]
        public ActionResult UploadImage()
        {
            // Validate the arguments
            var httpPostedFileBase = Request.Files[0];
            if (httpPostedFileBase == null || httpPostedFileBase.InputStream == null)
            {
                return Json(new { success = false, message = "The provided file is empty or invalid" }, "text/html");
            }

            string filename = httpPostedFileBase.FileName;
            string extension = System.IO.Path.GetExtension(filename).Substring(1);

            if (!AzureConfig.SupportedImageFormats.Contains(extension))
            {
                return Json(new { success = false, message = "Png or jpg only please!" }, "text/html");
            }

            // We need two versions of this image - a "big" one for the page, and a thumbnail
            System.Drawing.Image masterImage = System.Drawing.Image.FromStream(httpPostedFileBase.InputStream);

            // Let's get this into a square by windowing
            // Nope, let's not, let the CSS deal with it.
            // Instead let's fix the longest edge at 320
            int maxDimension = 320, smallDimension = 79;
            int newWidth, newHeight, width = masterImage.Width;
            int height = masterImage.Height;
            if (height > width)
            {
                newHeight = maxDimension;
                newWidth = maxDimension * width / height;
            }
            else
            {
                newWidth = maxDimension;
                newHeight = maxDimension * height / width;
            }
            System.Drawing.Image bigImage = masterImage.GetThumbnailImage(newWidth, newHeight, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

            // Let's get this into a square
            System.Drawing.Image squareImage = new System.Drawing.Bitmap(maxDimension, maxDimension);
            using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(squareImage))
            {
                gr.FillRectangle(System.Drawing.Brushes.Black, 0, 0, maxDimension, maxDimension);
                gr.DrawImage(bigImage, new System.Drawing.Point((maxDimension - newWidth) / 2, (maxDimension - newHeight) / 2));
            }

            // Let's get this guy into a stream
            var bigStream = new System.IO.MemoryStream();
            squareImage.Save(bigStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            bigStream.Position = 0;

            // Upload to Azure
            string bigFilename = string.Empty;
            try
            {
                // We're constraining these guys to be jpg during the thumbnailing
                bigFilename = Guid.NewGuid().ToString() + ".jpg";
                AzureBlobService.UploadToContainer(bigStream, AzureContainers.Images, bigFilename);
            }
            catch (AzureException ex)
            {
                return Json(new { success = false, message = "Unable to upload the big file to cloud storage" }, "text/html");
            }

            float scale = (float)smallDimension / (float)maxDimension;
            System.Drawing.Image smallImage = squareImage.GetThumbnailImage(smallDimension, smallDimension, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero);

            // Let's get this guy into a stream
            var smallStream = new System.IO.MemoryStream();
            smallImage.Save(smallStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            smallStream.Position = 0;

            // Upload to Azure
            string smallFilename = string.Empty;
            try
            {
                smallFilename = Guid.NewGuid().ToString() + ".jpg";
                AzureBlobService.UploadToContainer(bigStream, AzureContainers.Images, smallFilename);
            }
            catch (AzureException ex)
            {
                return Json(new { success = false, message = "Unable to upload the small file to cloud storage" }, "text/html");
            }

            // Let's create a media item to represent the user's image
            // The CurrentUser property's hooking into Identity
            MediaItem mediaItem =  MediaItemRepository.FindById(CurrentUser.MediaItems[0].ID);

            // Was there an existing Azure image for this user?
            string container, existingItem;
            if (mediaItem.ParseMainUri(out container, out existingItem))
            {
                // Ths delete won't remove stuff from the "content" container and fails gracefully
                AzureBlobService.RemoveFromContainer(container, existingItem);
            }
            if (mediaItem.ParseThumbnailUri(out container, out existingItem))
            {
                AzureBlobService.RemoveFromContainer(container, existingItem);
            }

            mediaItem.MediaType = MediaItem.MediaTypes.Image;
            mediaItem.Uri = bigFilename;
            mediaItem.ThumbnailUri = smallFilename;
            //// Success
            MediaItemRepository.Update(mediaItem);

            // Bitter experience tells us this is somewhat critical
            bigImage.Dispose();
            squareImage.Dispose();
            smallImage.Dispose();
            masterImage.Dispose();

            return Json(new { success = true, filename = filename, uri = mediaItem.MediaItemPath(), thumbnail = mediaItem.ThumbnailPath() }, "text/html");


            //return Json(new { success = false}, "text/html");
        }

        /// <summary>
        /// Required, but not used
        /// </summary>
        /// <returns>true</returns>
        public bool ThumbnailCallback()
        {
            return true;
        }

    }
}