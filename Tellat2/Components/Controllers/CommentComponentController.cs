﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the commenting functionality in to a re-useable component
    /// </summary>
    public class CommentComponentController : BaseController<CommentComponentViewModel>
    {
        [Dependency]
        public ICommentService CommentService { get; set; }

        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        /// <summary>
        /// /CommentComponent/Index
        /// </summary>
        public ActionResult Index(int reportId)
        {
            try
            {
                ViewModel.Comments = CommentRepository.FindAllByReportId(reportId).ToList();
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }

        /// <summary>
        /// /CommentComponent/Post
        /// </summary>
        public ActionResult Post(CommentComponentViewModel model)
        {
            try
            {
                CommentService.Create(CurrentUserId, model.ReportId, model.Text);

                model.Comments = CommentRepository.FindAllByReportId(model.ReportId).ToList();
                model.Text = string.Empty;
            }
            catch (RepositoryException ex)
            {
                
            }

            return PartialView("_Index", model);
        }
    }
}