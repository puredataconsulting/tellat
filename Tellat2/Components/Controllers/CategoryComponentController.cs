﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the category functionality in to a re-useable component
    /// </summary>
    public class CategoryComponentController : BaseController<CategoryComponentViewModel>
    {
        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public IReportCategoryRepository ReportCategoryRepository { get; set; }

        /// <summary>
        /// /CategoryComponent/Index
        /// </summary>
        public ActionResult Index(int id, int? sortCriterion)
        {
            try
            {
                ViewModel.Category = CategoryRepository.FindById(id);

                // For an empty category with nothing featured this will remain null
                ViewModel.Featured = null;
                List<Report> featuredReports = ReportCategoryRepository.FindFeaturedReportsByCategory(id, Utility.Utilities.GetReportSortCriterion(sortCriterion), true);
                List<Report> notFeaturedReports = ReportCategoryRepository.FindNotFeaturedReportsByCategory(id, Utility.Utilities.GetReportSortCriterion(sortCriterion), true);

                ViewModel.Reports = new List<Report>();
                // Take the first featured report as The featured report
                int nFeatured = featuredReports.Count, nFeaturedRemaining = nFeatured;
                int nNotFeatured = notFeaturedReports.Count, nNotFeaturedRemaining = nNotFeatured;
                if (nFeatured > 0)
                {
                    ViewModel.Featured = featuredReports[0];
                    // Add what remains of the featured list in
                    for (int i = 0; i < nFeatured - 1 && i < 3; i++)
                    {
                        ViewModel.Reports.Add(featuredReports[i + 1]);
                    }

                    // Now we want to add the non-featured stuff
                    for (int i = ViewModel.Reports.Count, j = 0; i < 3 && j < nNotFeatured; i++, j++)
                    {
                        ViewModel.Reports.Add(notFeaturedReports[j]);
                    }
                }
                else
                {
                    // No featured, so we draw from the list of non-featured
                    if (nNotFeatured > 0)
                    {
                        ViewModel.Featured = notFeaturedReports[0];
                        // Now we want to add the non-featured stuff
                        for (int j = 0; j < 3 && j < nNotFeatured - 1; j++)
                        {
                            ViewModel.Reports.Add(notFeaturedReports[j + 1]);
                        }
                    }
                    else
                    {
                        // Nothing to show!!
                        ViewModel.Featured = null;
                    }
                }

                foreach (Report report in ViewModel.Reports)
                {
                    if (report.IsFeatured)
                    {
                        ViewModel.Featured = report;
                        break;
                    }
                }
                if (ViewModel.Featured == null)
                {
                    if (ViewModel.Reports.Count > 0)
                    {
                        ViewModel.Featured = ViewModel.Reports[0];
                    }
                }
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
