﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using System.Web;
using System.Threading.Tasks;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the social functionality in to a re-useable component
    /// </summary>
    public class SocialComponentController : BaseController<SocialComponentViewModel>
    {
        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        /// <summary>
        /// /SocialComponent/Index
        /// </summary>
        public async Task<ActionResult> Index(int id)
        {
            try
            {
                ViewModel.NComments = CommentRepository.GetNComments(id);
                string currentUserId;

                ViewModel.ID = id;

                if (User.Identity.IsAuthenticated)
                {
                    currentUserId = ViewModel.CurrentUserId;
                    // Is this report rated by this user?
                    int rating;
                    if(FavouriteRepository.FindRating(currentUserId, id, out rating))
                    {
                        ViewModel.IsRateableByThisUser = false;
                        ViewModel.IsRatedByThisUser = true;
                        ViewModel.ThisUserRating = rating;
                    }
                    else
                    {
                        ViewModel.IsRateableByThisUser = true;
                        ViewModel.IsRatedByThisUser = false;
                    }
                    ViewModel.IsFavouritedByThisUser = FavouriteRepository.IsFavourite(currentUserId, id);
                    ViewModel.CanBeFavouritedByThisUser = true;
                }
                else
                {
                    ViewModel.IsRateableByThisUser = false;
                    ViewModel.IsRatedByThisUser = false;
                    ViewModel.IsFavouritedByThisUser = false;
                    ViewModel.CanBeFavouritedByThisUser = false;
                }
                GetViewStats(id);
            }
            catch (RepositoryException ex)
            {

            }
            return PartialView("_Index", ViewModel);
        }

        /// <summary>
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult Rate()
        {
            int reportID = int.Parse(Request["id"].ToString());
            int rate = int.Parse(Request["rating"].ToString());

            if (FavouriteRepository.AddRating(ViewModel.CurrentUserId, reportID, rate))
            {
                ReportRepository.RegisterNewRating(reportID, rate);
            }
            ViewModel.IsRateableByThisUser = false;
            ViewModel.IsRatedByThisUser = true;
            ViewModel.ThisUserRating = rate;


            GetViewStats(reportID);

            return PartialView("_Index", ViewModel);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Favourite(string reportId, string favourite)
        {
            int id = int.Parse(reportId);
            bool isFavourite = bool.Parse(favourite);

            FavouriteRepository.SetFavourite(ViewModel.CurrentUserId, id, isFavourite);

            GetViewStats(id);

            return PartialView("_Index", ViewModel);
        }

        protected void GetViewStats(int reportID)
        {
            Report report = ReportRepository.FindById(reportID); 

            ViewModel.Report = report;
            ViewModel.Author = UserManager.FindByIdAsync(report.ApplicationUserId).Result;

            ViewModel.NViewings = report.NViewings;
            ViewModel.NRatings = report.NRatings;
            ViewModel.NFavourites = FavouriteRepository.GetNFavouritesByReport(report.ID);
            if (ViewModel.NRatings > 0)
            {
                ViewModel.AverageRating = (float)report.TotalRating / (float)report.NRatings;
            }
            else ViewModel.AverageRating = 0;
        }
    }
}
