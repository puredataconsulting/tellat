﻿using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the chat functionality in to a re-useable component
    /// </summary>
    public class YouTubeComponentController : BaseController<YouTubeComponentViewModel>
    {
        /// <summary>
        /// /YouTubeComponent/Index
        /// </summary>
        public ActionResult Index()
        {
            return PartialView("_Index", ViewModel);
        }
    }
}
