﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the more-like-this functionality in to a re-useable component
    /// </summary>
    public class MoreLikeThisComponentController : BaseController<MoreLikeThisComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /MoreLikeThisComponent/Index
        /// </summary>
        public ActionResult Index(int id, int? itemCount, int? sortCriterion)
        {
            try
            {
                 ViewModel.ReportsSortCriterion = sortCriterion.HasValue? sortCriterion.Value : -1;
                
                if (itemCount.HasValue)
                {
                    ViewModel.Reports = ReportRepository.FindRelatedReports(id, itemCount.Value);
                }
                else
                {
                    ViewModel.Reports = ReportRepository.FindAll().ToList();
                }
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
