﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class SmallReportComponentController : BaseController<SmallReportComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        public ActionResult Index(int id)
        {
            try
            {
                ViewModel.Report = ReportRepository.FindById(id);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}