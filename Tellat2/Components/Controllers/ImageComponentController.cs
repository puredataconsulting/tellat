﻿using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the image viewer functionality in to a re-useable component
    /// </summary>
    public class ImageComponentController : BaseController<ImageComponentViewModel>
    {
        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        /// <summary>
        /// /ImageComponent/Index
        /// </summary>
        public ActionResult Index(int imageId)
        {
            try
            {
                ViewModel.Image = MediaItemRepository.FindById(imageId);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
