﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Tellat2.Pages.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class GroupsComponentController : BaseController<GroupsComponentViewModel>
    {
        
        public ActionResult Index(GroupsViewModel model)
        {
            ViewModel.GroupsDictionary = model.GroupsDictionary;
            return PartialView("_Index", ViewModel);
        }
    }
}