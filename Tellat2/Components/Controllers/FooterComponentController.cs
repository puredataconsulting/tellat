﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class FooterComponentController : BaseController<FooterComponentViewModel>
    {
        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        public ActionResult Index()
        {
            try
            {
                ViewModel.Categories = CategoryRepository.GetFixedCategories(6);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}