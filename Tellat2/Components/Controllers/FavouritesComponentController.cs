﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class FavouritesComponentController : BaseController<FavouritesComponentViewModel>
    {

        /// <summary>
        /// /CategoryComponent/Index
        /// </summary>
        public ActionResult Index(List<Report> favourites, int? sortCriterion)
        {
            ViewModel.FavouritesSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
            ViewModel.Favourites = favourites;

            return PartialView("_Index", ViewModel);
        }
    }
}