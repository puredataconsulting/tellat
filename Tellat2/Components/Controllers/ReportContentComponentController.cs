﻿using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the category functionality in to a re-useable component
    /// </summary>
    public class ReportContentComponentController : BaseController<ReportContentComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /ReportComponent/Index
        /// </summary>
        public ActionResult Index(int reportId)
        {
            try
            {
                ViewModel.Report = ReportRepository.FindById(reportId);
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
