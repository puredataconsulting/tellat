﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the navigation functionality in to a re-useable component
    /// </summary>
    public class NavigationComponentController : BaseController<NavigationComponentViewModel>
    {
        [Dependency]
        public ICategoryRepository CategoryRepository { get; set; }

        /// <summary>
        /// /NavigationComponent/Index
        /// </summary>
        public ActionResult Index()
        {
            try
            {
                ViewModel.Categories = CategoryRepository.FindAll().ToList();
            }
            catch (RepositoryException)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}
