﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class TrendingComponentController : BaseController<TrendingComponentViewModel>
    {
        [Dependency]
        public ITagRepository TagRepository { get; set; }

        /// <summary>
        /// /TrendingComponent/Index
        /// </summary>
        public ActionResult Index()
        {
            try
            {
                ViewModel.Tags = TagRepository.ListByTrending(4);
            }
            catch (RepositoryException ex)
            {

            }
            return PartialView("_Index", ViewModel);
        }
    }
}