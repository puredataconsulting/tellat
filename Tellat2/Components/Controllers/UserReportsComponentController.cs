﻿using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using Tellat2.Base;
using Tellat2.Components.ViewModels;
using Framework2.Core.Models.Core;
using System.Collections.Generic;

namespace Tellat2.Components.Controllers
{
    /// <summary>
    /// Encapsulates the user reports functionality in to a reuseable component
    /// </summary>
    public class UserReportsComponentController : BaseController<UserReportsComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        /// <summary>
        /// /ReportsComponent/Index
        /// </summary>
        public ActionResult Index(List<Report> reports, int? sortCriterion)
        {
            ViewModel.ReportsSortCriterion = sortCriterion.HasValue ? sortCriterion.Value : -1;
            ViewModel.Reports = reports;
            return PartialView("_Index", ViewModel);
        }
    }
}
