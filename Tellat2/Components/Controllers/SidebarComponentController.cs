﻿using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tellat2.Base;
using Tellat2.Components.ViewModels;

namespace Tellat2.Components.Controllers
{
    public class SidebarComponentController : BaseController<SidebarComponentViewModel>
    {
        [Dependency]
        public IReportRepository ReportRepository { get; set; }

        public ActionResult Index()
        {
            try
            {
                ViewModel.Reports = ReportRepository.FindByIsFeatured(3);

            }
            catch (RepositoryException ex)
            {

            }

            return PartialView("_Index", ViewModel);
        }
    }
}