﻿using System.Web.Mvc;
using Framework2.Core.Models.Core;
using Microsoft.AspNet.Identity;
using Tellat2.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.Owin;

using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;


namespace Tellat2.Base
{
    /// <summary>
    /// The abstract base class for all controllers, and component controllers
    /// </summary>
    public abstract class BaseController<T> : Controller where T : BaseViewModel, new()
    {
        // Base view model for all controllers
        protected T ViewModel { get; set; }

        /// <summary>
        /// Constructor 
        /// </summary>
        protected BaseController() 
        {
            ViewModel = new T();
        }

        /// <summary>
        /// Is the curent user logged in
        /// </summary>
        public bool IsLoggedIn
        {
            get
            {
                bool isLoggedIn = false;
                //TODO Not sure how much of this is non null when no-one's logged in
                if(System.Web.HttpContext.Current.User != null)
                {
                    if(System.Web.HttpContext.Current.User.Identity != null)
                    {
                        isLoggedIn = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
                    }
                }
                return isLoggedIn;
            }
        }


        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        /// <summary>
        /// The current logged in user
        /// </summary>
        public ApplicationUser CurrentUser
        {
            get
            {
                if (User.Identity.IsAuthenticated)
                {
                    return UserManager.FindByNameAsync(User.Identity.Name).Result;
                }
                else return null;
            }
        }

        /// <summary>
        /// The current logged in user
        /// </summary>
        public string CurrentUserId
        {
            get
            {
                if (System.Web.HttpContext.Current.User.Identity != null)
                {
                    return System.Web.HttpContext.Current.User.Identity.GetUserId();
                }
                else return null;
            }
        }

        /// <summary>
        /// May return null
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApplicationUser GetUserById(string id)
        {
            return UserManager.FindByIdAsync(id).Result;
        }
    }
}
