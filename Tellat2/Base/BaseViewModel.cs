﻿using System.Web;
using Framework2.Core.Models.Core;
using Microsoft.AspNet.Identity;

namespace Tellat2.Base
{
    /// <summary>
    /// The base class for all view models
    /// </summary>
    public class BaseViewModel
    {
        public BaseViewModel()
        {
            // This is to flush out any failure to assign the user
            _currentUser = null;
        }

        /// <summary>
        /// Is the curent user logged in
        /// </summary>
        public bool IsLoggedIn
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }

        /// <summary>
        /// The current logged in user
        /// </summary>
        public string CurrentUserId
        {
            get
            {
                string id = null;
                if (HttpContext.Current.User.Identity != null)
                {
                    var userId = HttpContext.Current.User.Identity.GetUserId();
                    if(userId != null)
                    {
                        id = userId.ToString();
                    }
                }
                return id;
            }
        }

        private ApplicationUser _currentUser;

        /// <summary>
        /// This needs to be assigned in the controller where it's required - BaseController has a GetCurrentUser method
        /// that works with Identity
        /// </summary>
        public ApplicationUser CurrentUser
        {
            get
            {
                return _currentUser;
            }
            set
            {
                _currentUser = value;
            }

        }

        

        /// <summary>
        /// Is the quoted user logged in and current, and hence the owner of assets we'll see in the view?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsCurrent(string userId)
        {
            bool isOwner = false;

            if(IsLoggedIn)
            {
                isOwner = (CurrentUserId == userId);
            }

            return isOwner;
        }

        /// <summary>
        /// Is there a current user and if so is s/he logged in?
        /// </summary>
        /// <returns></returns>
        public bool IsCurrent()
        {
            bool isOwner = false;

            if (CurrentUserId != null)
            {
                if (IsLoggedIn)
                {
                    isOwner = true;
                }
            }
            return isOwner;
        }

        public bool IsCurrentUserAdmin()
        {
            bool isAdmin = false;
            if (CurrentUserId != null)
            {
                isAdmin = HttpContext.Current.User.IsInRole("Admin");
            }
            return isAdmin;
        }

    }
}
