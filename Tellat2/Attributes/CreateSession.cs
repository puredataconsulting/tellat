﻿using System.Web;
using System.Web.Mvc;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;

namespace Tellat2.Attributes
{
    /// <summary>
    /// Create and populate session
    /// </summary>
    public class CreateSession : FilterAttribute, IActionFilter
    {
        [Dependency]
        public IApplicationUserRepository UserRepository { get; set; }

        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Have we already validated this session?
            if (HttpContext.Current.Items.Contains("IsLoggedIn"))
            {
                return;
            }

            // Fetch the session token (if it exists?)
            var sessionToken = (string)HttpContext.Current.Items["SessionToken"];

            // Do we have a valid session token
            if (!string.IsNullOrEmpty(sessionToken))
            {
                // Fetch the user
                var user = UserRepository.FindBySessionTokenAsync(sessionToken).Result;

                // Populate the session with common values from the database
                if (user != null)
                {
                    HttpContext.Current.Items.Add("IsLoggedIn", true);
                    HttpContext.Current.Items.Add("CurrentUser", user);
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }
    }
}
