﻿using Microsoft.Web.Administration;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Web.Site
{
    public class WebRole : RoleEntryPoint
    {
        public override bool OnStart()
        {
#if !DEBUG
            // ** THIS IS REALLY IMPORTANT **
            using (ServerManager serverManager = new ServerManager())
            {
                // If yu're crashing here in the emulator it's because you're missing IIS 7 - switch it on in Control Panel
                    string baseName = RoleEnvironment.CurrentRoleInstance.Id + "_Tellat2";
                    try
                    {
                        foreach (Microsoft.Web.Administration.Site wsite in serverManager.Sites)
                        {
                            if (wsite.Name.StartsWith(baseName))
                            {
                                var app = wsite.Applications.First();
                                string appPoolName = app.ApplicationPoolName;
                                var appPool = serverManager.ApplicationPools[appPoolName];
                                // THIS IS WHAT THIS HAS ALL BEEN FOR:
                                appPool.ProcessModel.LoadUserProfile = true;
                                try
                                {
                                    serverManager.CommitChanges();
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("Failed to CommitChanges", ex);
                                }
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failure in OnStart", ex);
                    }
                
            }
#endif
            return base.OnStart();
        }

        public override void Run()
        {
            try
            {
                base.Run();
            }
            catch(Exception ex)
            {

            }
        }
    }
}