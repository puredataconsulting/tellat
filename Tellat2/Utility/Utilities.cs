﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tellat2.Utility
{
    public class Utilities
    {
        /// <summary>
        /// Drop downs on the web site give us the value of an index in the list box - we need to translate that
        /// apparently arbitrary integer into the human readable internal representation 
        /// </summary>
        /// <param name="sortInstruction"></param>
        /// <returns></returns>
        static public ReportSortCriterion GetReportSortCriterion(int? sortInstruction)
        {
            if (!sortInstruction.HasValue || sortInstruction <= 0) return ReportSortCriterion.None;
            else return ReportSortCodes[sortInstruction.Value];
        }
        public static readonly Dictionary<int, ReportSortCriterion> ReportSortCodes = new Dictionary<int, ReportSortCriterion>
        {
            { 1, ReportSortCriterion.Author | ReportSortCriterion.Ascending},
            //{2, ReportSortCriterion.Category | ReportSortCriterion.Ascending},
            {3, ReportSortCriterion.Title | ReportSortCriterion.Ascending},
            {4, ReportSortCriterion.Title | ReportSortCriterion.Descending},
            {5,ReportSortCriterion.DateAdded | ReportSortCriterion.Descending},
            {6, ReportSortCriterion.DateAdded | ReportSortCriterion.Ascending},
            {7,ReportSortCriterion.NViewings | ReportSortCriterion.Ascending},
            {8,ReportSortCriterion.NViewings | ReportSortCriterion.Descending},
            {9, ReportSortCriterion.Rating | ReportSortCriterion.Ascending},
            {10, ReportSortCriterion.Rating | ReportSortCriterion.Descending},
            {11, ReportSortCriterion.Public },
            {12, ReportSortCriterion.Private },
            {13, ReportSortCriterion.Draft },
            {14, ReportSortCriterion.Final }
        };

        public static readonly Dictionary<int, string> ReportSortsTitles = new Dictionary<int, string>
        {
            { 1, "Reporter"},
            //{2, "Category"},
            {3, "Title, alphabetically"},
            {4, "Title, reverse alphabetically"},
            {5, "Date added, most recent first"},
            {6, "Date added, most recent last"},
            {7, "Number of views, low to high"},
            {8, "Number of views, high to low"},
            {9, "Rating, low to high"},
            {10, "Rating, low to high"},
            {11, "Public only"},
            {12, "Private only"},
            {13, "Draft only"},
            {14, "Final only"}
        };


        static public UserSortCriterion GetUserSortCriterion(int? sortInstruction)
        {
            if (!sortInstruction.HasValue || sortInstruction <= 0) return UserSortCriterion.None;
            else return UserSortCodes[sortInstruction.Value];
        }
        public static readonly Dictionary<int, UserSortCriterion> UserSortCodes = new Dictionary<int, UserSortCriterion>
        {
            { 1, UserSortCriterion.LastName | UserSortCriterion.Ascending},
            {2, UserSortCriterion.FirstName | UserSortCriterion.Ascending},
            {3, UserSortCriterion.UserName | UserSortCriterion.Ascending},
            {4, UserSortCriterion.LastName | UserSortCriterion.Descending},
            {5,UserSortCriterion.FirstName | UserSortCriterion.Descending},
            {6, UserSortCriterion.UserName | UserSortCriterion.Descending},
            {7,UserSortCriterion.LastActivity | UserSortCriterion.Descending},
            {8,UserSortCriterion.LastActivity | UserSortCriterion.Ascending},
            //{9, UserSortCriterion.Acclaim | UserSortCriterion.Descending},
            //{10, UserSortCriterion.Acclaim | UserSortCriterion.Ascending},
            {11, UserSortCriterion.DateAdded | UserSortCriterion.Descending},
            {12, UserSortCriterion.DateAdded | UserSortCriterion.Ascending}
        };

        public static readonly Dictionary<int, string> UserSortsTitles = new Dictionary<int, string>
        {
            { 1, "Last name, alphabetically"},
            { 2, "First name, alphabetically"},
            { 3, "User name, alphabetically"},
            { 4, "Last name, reverse alphabetically"},
            { 5, "First name, reverse alphabetically"},
            { 6, "User name, reverse alphabetically"},
            {7, "Recent activity, most recent first"},
            {8, "Recent activity, most recent last"},
            //{9, "Acclaim, high to low"},
            //{10, "Acclaim, low to high"},
            {11, "Date added, most recent first"},
            {12, "Date added, most recent last"},
        };
    }


}
