﻿namespace Tellat2.Extensions
{
    /// <summary>
    /// Extension methods for strings
    /// </summary>
    public static class StringHelper
    {
        /// <summary>
        /// Drop a string to a maximum length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxChars"></param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxChars)
        {
            // Need to trap this out for a freaky  corner case
            if (value == null) return string.Empty;

            return value.Length <= maxChars ? value : value.Substring(0, maxChars) + " ..";
        }

        
    }
}