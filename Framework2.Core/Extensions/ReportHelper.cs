﻿using Framework2.Azure.Config;
using Framework2.Core.Models.Core;
using System.Collections.Generic;

namespace Framework2.Core.Extensions
{
    /// <summary>
    /// Extension methods to add full url resolving to report models
    /// </summary>
    public static class ReportHelper
    {
        /// <summary>
        /// Returns a fully qualified path to the file in Azure BLOB storage
        /// </summary>
        public static List<string> UriPaths(this Report report)
        {
            List<string> results = new List<string>();

            if (report != null)
            {
                foreach (MediaItem mediaItem in report.MediaItems)
                {
                    results.Add(mediaItem.MediaItemPath());
                }
            }
            return results;
        }

        /// <summary>
        /// Returns a fully qualified path to the files in Azure BLOB storage
        /// </summary>
        public static List<string> ThumbnailPaths(this Report report)
        {
            List<string> results = new List<string>();

            if (report != null)
            {
                foreach (MediaItem mediaItem in report.MediaItems)
                {
                    results.Add(mediaItem.ThumbnailPath());
                }
            }
            return results;
        }

        /// <summary>
        /// Returns a fully qualified path to the zeroth media item, thumbnail file in Azure BLOB storage
        /// </summary>
        public static string ThumbnailPath(this Report report)
        {
            if (report == null) return string.Empty;
            else if (report.MediaItems.Count == 0) return string.Empty;
            else
            {
                return report.MediaItems[0].ThumbnailPath();
            }
        }

        /// <summary>
        /// Returns a fully qualified path to the zeroth media item file in Azure BLOB storage
        /// </summary>
        public static string UriPath(this Report report)
        {
            if (report == null) return string.Empty;
            else if (report.MediaItems.Count == 0) return string.Empty;
            else
            {
                return report.MediaItems[0].MediaItemPath();
            }
        }

        public static string SmallIconPath(this Report report)
        {
            if (report == null) return string.Empty;
            else if (report.MediaItems.Count == 0) return string.Empty;
            else
            {
                string root = "/Content/Images/Pages/Shared/";
                string link;
                MediaItem.MediaTypes mediaType = report.MediaItems[0].MediaType;
                switch(mediaType)
                {
                    case MediaItem.MediaTypes.Audio:
                        link =  "img_media_audio_red.png";
                        break;

                    case MediaItem.MediaTypes.Image:
                        link =  "img_media_photo_red.png";
                        break;

                    case MediaItem.MediaTypes.Text:
                        link =  "img_media_text_red.png";
                        break;

                    default:
                    case MediaItem.MediaTypes.Video:
                        link =  "img_media_video_red.png";
                        break;
                }

                return root + link;
            }
        }
        public static string LargeIconPath(this Report report)
        {

            if (report == null) return string.Empty;
            else if (report.MediaItems.Count == 0) return string.Empty;
            else
            {
                string root = "/Content/Images/Pages/Shared/";
                string link;
                MediaItem.MediaTypes mediaType = report.MediaItems[0].MediaType;
                switch (mediaType)
                {
                    case MediaItem.MediaTypes.Audio:
                        link =  "img_media_audio_white.png";;
                        break;

                    case MediaItem.MediaTypes.Image:
                        link =  "img_media_photo_white.png";;
                        break;

                    case MediaItem.MediaTypes.Text:
                        link =  "img_media_text_white.png";;
                        break;

                    default:
                    case MediaItem.MediaTypes.Video:
                        link =  "img_media_video_white.png";;
                        break;
                }
                return root + link;
            }
        }
    }
}