﻿using Framework2.Azure.Config;
using Framework2.Core.Models.Core;

namespace Framework2.Core.Extensions
{
    /// <summary>
    /// Extension methods to add full url resolving to user models
    /// </summary>
    public static class ApplicationUserHelper
    {
        /// <summary>
        /// Returns a fully qualified path to the file in Azure BLOB storage
        /// </summary>
        public static string ThumbnailPath(this ApplicationUser user)
        {
            string result = string.Empty;
            if (user == null) return result;

            if(user.MediaItems != null)
            {
                if(user.MediaItems.Count > 0)
                {
                    result = user.MediaItems[0].ThumbnailPath();
                }
            }
            if(string.IsNullOrEmpty(result))
            {
                result = AzureConfig.ContentContainerUri + "/" + "profile-blue.png";
            }
            return result;
        }
    }
}