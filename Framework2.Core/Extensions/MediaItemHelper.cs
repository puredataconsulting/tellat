﻿using Framework2.Azure.Config;
using Framework2.Core.Models.Core;

namespace Framework2.Core.Extensions
{
    /// <summary>
    /// Extension methods to add full url resolving to report models
    /// </summary>
    public static class MediaItemHelper
    {
        /// <summary>
        /// Returns a fully qualified path to the file in Azure BLOB storage
        /// </summary>
        public static string MediaItemPath(this MediaItem mediaItem)
        {
            return ReconstructMediaPath(mediaItem);
        }

        /// <summary>
        /// Returns a fully qualified path to the thumbnail in Azure BLOB storage
        /// </summary>
        public static string ThumbnailPath(this MediaItem mediaItem)
        {
            if (mediaItem == null) return string.Empty;

            // Simple directly uploaded media has a thumbnail  in the "images" container...
            if (mediaItem.MediaType == MediaItem.MediaTypes.Image)
            {
                return AzureConfig.ImagesContainerUri + "/" + mediaItem.ThumbnailUri;
            }
            else if (mediaItem.MediaType == MediaItem.MediaTypes.Video
            || mediaItem.MediaType == MediaItem.MediaTypes.Audio
            || mediaItem.MediaType == MediaItem.MediaTypes.Text
            || mediaItem.MediaType == MediaItem.MediaTypes.Content)
            {
                return AzureConfig.ContentContainerUri + "/" + mediaItem.ThumbnailUri;
            }

        // ..whereas the results of the thumbnailing process should be referenced by direct (and unpredictable) uri
            else return mediaItem.ThumbnailUri;

        }

        private static string ReconstructMediaPath(this MediaItem mediaItem)
        {
            if (mediaItem == null) return string.Empty;

            if (mediaItem.MediaType == MediaItem.MediaTypes.Video)
                return AzureConfig.VideosContainerUri + "/" + mediaItem.Uri;

            else if (mediaItem.MediaType == MediaItem.MediaTypes.Image)
                return AzureConfig.ImagesContainerUri + "/" + mediaItem.Uri;

            else if (mediaItem.MediaType == MediaItem.MediaTypes.Audio)
                return AzureConfig.AudiosContainerUri + "/" + mediaItem.Uri;

            else if (mediaItem.MediaType == MediaItem.MediaTypes.Text)
                return AzureConfig.DocumentsContainerUri + "/" + mediaItem.Uri;

            else if (mediaItem.MediaType == MediaItem.MediaTypes.Content)
                return AzureConfig.ContentContainerUri + "/" + mediaItem.Uri;

            // TODO This will need to get smarter as we get into multiple encodings
            else return mediaItem.Uri;
        }

        /// <summary>
        /// Now we're mixing normal storage blob video (etc) with stuff that's gone into WAMS we need
        /// to abstract away our original notion of fixed containers. WAMS gives us no control over the name of the container 
        /// stuff gets put into, for now (March 2014) anyway.
        /// </summary>
        /// <param name="mediaItem"></param>
        /// <param name="container"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool ParseMainUri(this MediaItem mediaItem, out string container, out string item)
        {
            container = string.Empty;
            item = string.Empty;

            if (mediaItem == null) return false;

            if (mediaItem.MediaType == MediaItem.MediaTypes.Video)
            {
                container = AzureConfig.VideosContainerName;
                item = mediaItem.Uri;
            }
            else if (mediaItem.MediaType == MediaItem.MediaTypes.Image)
            {
                container = AzureConfig.ImagesContainerName;
                item = mediaItem.Uri;
            }
            else if (mediaItem.MediaType == MediaItem.MediaTypes.Audio)
            {
                container = AzureConfig.AudiosContainerName;
                item = mediaItem.Uri;
            }
            else if (mediaItem.MediaType == MediaItem.MediaTypes.Text)
            {
                container = AzureConfig.DocumentsContainerName;
                item = mediaItem.Uri;
            }
            else if (mediaItem.MediaType == MediaItem.MediaTypes.Content)
            {
                container = AzureConfig.ContentContainerName;
                item = mediaItem.Uri;
            }
            else 
            {
                // We have no choice but to parse the url down into segments
                System.Uri uri = new System.Uri(mediaItem.Uri);

                string[] segmentFields = uri.Segments;
                int nSegments = segmentFields.Length;
                container = segmentFields[nSegments - 2];
                item = segmentFields[nSegments - 1];
            }
            return true;
        }

        public static bool ParseThumbnailUri(this MediaItem mediaItem, out string container, out string item)
        {
            container = string.Empty;
            item = string.Empty;

            if (mediaItem == null) return false;

            if (mediaItem.MediaType == MediaItem.MediaTypes.Video ||
                mediaItem.MediaType == MediaItem.MediaTypes.Image ||
                mediaItem.MediaType == MediaItem.MediaTypes.Audio ||
                mediaItem.MediaType == MediaItem.MediaTypes.Content ||
                mediaItem.MediaType == MediaItem.MediaTypes.Text)
            {
                container = AzureConfig.ContentContainerName;
                item = mediaItem.ThumbnailUri;
            }
            else
            {
                // We have no choice but to parse the url down into segments
                System.Uri uri = new System.Uri(mediaItem.ThumbnailUri);

                string[] segmentFields = uri.Segments;
                int nSegments = segmentFields.Length;
                container = segmentFields[nSegments - 2];
                item = segmentFields[nSegments - 1];
            }
            return true;
        }


    }
}