﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Extensions
{
    public static class StringHelper
    {

        /// <summary>
        /// Returns true if a string contains any instance of one of the strings in the array 
        /// http://stackoverflow.com/questions/194930/how-do-i-use-linq-containsstring-instead-of-containsstring
        /// </summary>
        /// <param name="str"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool ContainsAny(this string str, params string[] values)
        {
            if (!string.IsNullOrEmpty(str) || values.Length > 0)
            {
                foreach (string value in values)
                {
                    if (str.Contains(value))
                        return true;
                }
            }

            return false;
        }
    }
}
