﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the Tagging
    /// </summary>
    public class TaggingRepository : BaseRepository<Tagging>, ITaggingRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TaggingRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public List<Report> FindReportsByTag(int tagId, ReportSortCriterion sortCriterion)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.TagId == tagId select tagging.Report;
                return ReportRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::FindReportsByTag", ex.InnerException);
            }
        }

        public List<Tag> FindTagsByReport(int reportId)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.ReportID == reportId select tagging.Tag;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::FindTagsByReport", ex.InnerException);
            }
        }

        public bool IsTaggingInPlace(int tagId, int reportID)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.TagId == tagId && tagging.ReportID == reportID select tagging;
                return query.Count() > 0;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::IsTaggingInPlace", ex.InnerException);
            }
        }

        public bool RemoveTagging(int tagId, int reportID)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.TagId == tagId && tagging.ReportID == reportID select tagging;
                if (query.Count() > 0)
                {
                    Context.Taggings.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::RemoveTagging", ex.InnerException);
            }
        }

        public List<Tagging> FindTaggingsByReport(int reportId)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.ReportID == reportId select tagging;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::FindTaggingsByReport", ex.InnerException);
            }
        }

        public bool RemoveTaggingsByReport(int reportID)
        {
            try
            {
                var query = from tagging in Context.Taggings where tagging.ReportID == reportID select tagging;
                if (query.Count() > 0)
                {
                    Context.Taggings.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TaggingRepository::RemoveTaggingsByReport", ex.InnerException);
            }

        }


    }
}
