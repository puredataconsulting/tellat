﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


using System.Data.Entity.SqlServer;

namespace Framework2.Core.Repositories.Core
{
    public class GroupReportRepository : BaseRepository<GroupReport>, IGroupReportRepository
    {
        public GroupReportRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public List<Report> FindReportsByGroup(int groupId, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            IQueryable<Report> query;
            try
            {
                if (isPublicOnly)
                {
                    query = from groupReport in Context.GroupReports
                            where groupReport.GroupId == groupId
                                //&& groupReport.Report.LifeCycleStage == Report.LifeCycle.Complete
                                && groupReport.Report.Scoping == Report.Scope.Public
                            select groupReport.Report;
                }
                else
                {
                    query = from groupReport in Context.GroupReports where groupReport.GroupId == groupId select groupReport.Report;
                }
                return ReportRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::FindReportsByGroup", ex.InnerException);
            }
        }

        public List<Group> FindGroupsByReport(int reportId)
        {
            try
            {
                var query = from groupReport in Context.GroupReports where groupReport.ReportId == reportId select groupReport.Group;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::FindGroupsByReport", ex.InnerException);
            }
        }

        public bool AddReportToGroup(int reportId, int groupId)
        {
            bool success = false;
            try
            {
                var query = from rc in Context.GroupReports where rc.GroupId == groupId && rc.ReportId == reportId select rc.Report;
                if (query.Count() == 0)
                {
                    GroupReport rcNew = Create();
                    rcNew.ReportId = reportId;
                    rcNew.GroupId = groupId;
                    Add(rcNew);
                    success = true;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::AddReportToGroup", ex.InnerException);
            }

            return success;
        }

        public bool RemoveReportFromGroup(int reportId, int groupId)
        {
            try
            {
                var query = from rc in Context.GroupReports where rc.GroupId == groupId && rc.ReportId == reportId select rc;
                foreach (var rc in query)
                {
                    // Hard Delete
                    Context.GroupReports.Remove(rc);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::RemoveReportFromGroup", ex.InnerException);
            }
            return true;
        }

        public bool RemoveReportFromAllGroups(int reportId)
        {
            try
            {
                var query = from rc in Context.GroupReports where rc.ReportId == reportId select rc;
                foreach (var rc in query)
                {
                    // Hard Delete
                    Context.GroupReports.Remove(rc);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::RemoveReportFromAllGroups", ex.InnerException);
            }
            return true;
        }

        public bool IsReportInGroup(int reportId, int groupId)
        {
            try
            {
                var query = from rc in Context.GroupReports where rc.GroupId == groupId && rc.ReportId == reportId select rc;
                return query.Count() > 0;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::IsReportInGroup", ex.InnerException);
            }
        }

        public List<SelectListItem> GetGroupsByReportAsSelectList(int reportId)
        {
            try
            {
                // Note the bodge on string conversion here - ToString has no meaning for Linq
                var query = from rc in Context.GroupReports where rc.ReportId == reportId select new SelectListItem { Text = rc.Group.Title, Value = SqlFunctions.StringConvert((double)rc.GroupId) };
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::GetGroupsByReportAsSelectList", ex.InnerException);
            }
        }

        public List<int> GetGroupIdsByReport(int reportId)
        {
            try
            {
                var query = from rc in Context.GroupReports where rc.ReportId == reportId select rc.GroupId;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::GetGroupIdsByReport", ex.InnerException);
            }
        }

        public bool DeleteByReport(int reportId)
        {
            try
            {
                var query = from rc in Context.GroupReports where rc.ReportId == reportId select rc;
                if (query.Count() > 0)
                {
                    // Hard delete
                    Context.GroupReports.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupReportRepository::DeleteByReport", ex.InnerException);
            }
        }


    }
}
