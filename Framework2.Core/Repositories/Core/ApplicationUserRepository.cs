﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        [Dependency]
        public IGroupMemberRepository GroupMemberRepository { get; set; }

        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }

        [Dependency]
        public IAlbumRepository AlbumRepository { get; set; }

        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        [Dependency]
        public ITaggingRepository TaggingRepository { get; set; }

        [Dependency]
        public IReportCategoryRepository ReportCategoryRepository { get; set; }

        [Dependency]
        public IReportRepository ReportRepository { get; set; }
        public static List<ApplicationUser> FilterSort(IQueryable<ApplicationUser> originalList, UserSortCriterion sortCriterion)
        {
            IQueryable<ApplicationUser> results = originalList;

            if ((sortCriterion & UserSortCriterion.FirstName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.FirstName);
                }
                else
                {
                    results = results.OrderByDescending(u => u.FirstName);
                }
            }
            else if ((sortCriterion & UserSortCriterion.LastName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.LastName);
                }
                else
                {
                    results = results.OrderByDescending(u => u.LastName);
                }
            }
            else if ((sortCriterion & UserSortCriterion.UserName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.UserName);
                }
                else
                {
                    results = results.OrderByDescending(u => u.UserName);
                }
            }
            else if ((sortCriterion & UserSortCriterion.LastActivity) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.LastActivity);
                }
                else
                {
                    results = results.OrderByDescending(u => u.LastActivity);
                }
            }
            else if ((sortCriterion & UserSortCriterion.DateAdded) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.CreationDate);
                }
                else
                {
                    results = results.OrderByDescending(u => u.CreationDate);
                }
            }


            return results.ToList();
        }

        protected readonly FrameworkContext _context;

        public ApplicationUserRepository(IContextManager contextManager)
        {
            _context = contextManager.Context;
        }

        public bool DecoupleUser(ApplicationUser user)
        {
            string id = user.Id;
            //TODO if we switched cascading deletes back on, would all this take care of itself?
            // We need to unhook our user from:
            // Groups via GroupMembers
            // Followers
            // Delete his Albums' media item(s) and his Albums
            // Delete his media item (stores his thumbnail/profile)
            // Delete his favourites
            // Delete his comments

            // Delete his reports
            // Delete the user himself

            // Here we go...
            // Groups
            GroupMemberRepository.RemoveUserFromAllGroups(id);
            // Followers
            FollowerRepository.RemoveUser(id);
            // Album Media Items
            AlbumRepository.RemoveAlbumsByUser(id);

            // User's thumbnails
            MediaItemRepository.RemoveMediaItemsForUser(id);

            // Favourites
            FavouriteRepository.RemoveFavouritesByUser(id);

            // Comments
            CommentRepository.RemoveCommentsByUser(id);

            // Deleting the reports should belong in ReportService - this same code exists in ReportController too
            // However I couldn't get the reportService reference to resolve
            foreach (Report report in user.Reports)
            {
                int reportId = report.ID;
                // Categories
                ReportCategoryRepository.DeleteByReport(reportId);
                // Tags
                TaggingRepository.RemoveTaggingsByReport(reportId);
                // Comments
                CommentRepository.RemoveCommentsByReport(reportId);
                // Favourites
                FavouriteRepository.RemoveFavouritesByReport(reportId);
                // Media (including blob delete)
                MediaItemRepository.RemoveMediaItemsByReport(reportId);
                // The report itself
                ReportRepository.RemoveReport(reportId);
            }

            // Now we **could** remove the user
            return true;
        }


    }
}
