﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the album
    /// </summary>
    public class AlbumRepository : BaseRepository<Album>, IAlbumRepository
    {
        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public AlbumRepository(IContextManager contextManager) 
            : base(contextManager) 
        {
        }

        /// <summary>
        /// Return all the albums created by a specific user
        /// </summary>
        public IQueryable<Album> FindAllByUserId(string userId)
        {
            IQueryable<Album> results;

            try
            {
                results = from albums in Context.Albums
                          where albums.ApplicationUserId == userId
                          select albums;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("AlbumRepository::FindAllCreatedByUserId", ex.InnerException);
            }

            return results;
        }

        public void RemoveAlbumsByUser(string userId)
        {
            try
            {
                var results = from album in Context.Albums where album.ApplicationUserId == userId select album;
                foreach(var album in results)
                {
                    // Remove the media, then the album itself
                    foreach(MediaItem mediaItem in album.MediaItems)
                    {
                        Context.MediaItems.Remove(mediaItem);
                    }
                    Context.Albums.Remove(album);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("AlbumRepository::RemoveAlbumsByUser", ex.InnerException);
            }
        }

    }
}
