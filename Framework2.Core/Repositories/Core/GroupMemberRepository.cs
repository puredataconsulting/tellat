﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the GroupMember
    /// </summary>
    public class GroupMemberRepository : BaseRepository<GroupMember>, IGroupMemberRepository
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public GroupMemberRepository(IContextManager contextManager)
            : base(contextManager)
        {
            
        }

        /// <summary>
        /// Find all members of a specific group
        /// </summary>
        public List<ApplicationUser> FindAllByGroupId(int groupId, UserSortCriterion sortCriterion)
        {
            IQueryable<ApplicationUser> results;

            try
            {
                results = from member in Context.GroupMembers
                          where member.GroupId == groupId
                          select member.ApplicationUser;

                return ApplicationUserRepository.FilterSort(results, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::FindAllByGroupId", ex.InnerException);
            }
        }

        /// <summary>
        /// Find all administrators of a specific group
        /// </summary>
        public List<ApplicationUser> FindOwnersByGroupId(int groupId)
        {
            IQueryable<ApplicationUser> results;

            try
            {
                results = from member in Context.GroupMembers
                          where member.GroupId == groupId && member.IsAdministrator
                          select member.ApplicationUser;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::FindOwnersByGroupId", ex.InnerException);
            }

            return results.ToList<ApplicationUser>();
        }

        public List<Group> FindGroupsByUserId(string userId)
        {
            IQueryable<Group> results;

            try
            {
                results = from member in Context.GroupMembers
                          where member.ApplicationUserId == userId 
                          select member.Group;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::FindGroupsByUserId", ex.InnerException);
            }
            return results.ToList<Group>();
        }

        public bool IsMember(string userId, int groupId, out bool isAdministrator)
        {
            isAdministrator = false;

            try
            {
                var query = from member in Context.GroupMembers where member.ApplicationUserId == userId && member.GroupId == groupId select member;
                if (query.Count() == 0)
                {
                    return false;
                }
                else
                {
                    GroupMember groupMember = query.First();
                    isAdministrator = groupMember.IsAdministrator;
                    return true;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::IsMember", ex.InnerException);
            }
        }

        public bool Join(string userId, int groupId, bool isAdministrator)
        {
            try
            {
                var query = from member in Context.GroupMembers where member.ApplicationUserId == userId && member.GroupId == groupId select member;
                if (query.Count() == 0)
                {
                    // He wasn't a member before
                    GroupMember groupMember = Create();
                    groupMember.ApplicationUserId = userId;
                    groupMember.GroupId = groupId;
                    groupMember.IsAdministrator = isAdministrator;
                    Add(groupMember);
                    return true;
                }
                else
                {
                    // He's already a member
                    return false;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::Join", ex.InnerException);
            }
        }

        public void RemoveUserFromGroup(string userId, int groupId)
        {
            try
            {
                var query = from member in Context.GroupMembers where member.ApplicationUserId == userId && member.GroupId == groupId select member;
                foreach(var gm in query)
                {
                    Context.GroupMembers.Remove(gm);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::RemoveUserFromGroup", ex.InnerException);
            }
        }

        public void RemoveUserFromAllGroups(string userId)
        {
            try
            {
                var query = from member in Context.GroupMembers where member.ApplicationUserId == userId select member;
                foreach (var gm in query)
                {
                    Context.GroupMembers.Remove(gm);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("GroupMemberRepository::RemoveUserFromAllGroups", ex.InnerException);
            }
        }
    }
}