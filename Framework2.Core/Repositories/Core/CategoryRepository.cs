﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the category model
    /// </summary>
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public CategoryRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        /// <summary>
        /// Return every category as a Razor select list
        /// </summary>
        public List<SelectListItem> FindAllAsSelectList(int categoryId)
        {
            List<SelectListItem> items;

            try
            {
                var teams = FindAll().ToList();
                items = teams.Select(t => new SelectListItem
                {
                    Text = t.Title,
                    Value = t.ID.ToString(),
                    Selected = t.ID == categoryId
                }).ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CategoryRepository::FindAllAsSelectList", ex.InnerException);
            }

            return items;
        }

        /// <summary>
        /// Get the first nCategories categories - these will be listed as links on the footer
        /// </summary>
        /// <param name="nCategories"></param>
        /// <returns></returns>
        public List<Category> GetFixedCategories(int nCategories)
        {
            try
            {
                var query = FindAll();
                return query.Take<Category>(nCategories).ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CategoryRepository::GetFixedCategories", ex.InnerException);
            }
        }
    }
}
