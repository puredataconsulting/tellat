﻿using System;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the log entry
    /// </summary>
    public class LogRepository : BaseRepository<Log>, ILogRepository
    {
        // TODO : Wrap the error logging in more error logging?

        /// <summary>
        /// Constructor
        /// </summary>
        public LogRepository(IContextManager contextManager) 
            : base(contextManager) 
        {
        }

        /// <summary>
        /// Log an error to the database
        /// </summary>
        public void LogError(object sender, string message)
        {
            var log = Context.Logs.Create();

            log.Sender = sender.GetType().Name;
            log.Message = message;
            log.Severity = "Error";

            Context.Logs.Add(log);
        }

        /// <summary>
        /// Log a message to the database
        /// </summary>
        public void LogMessage(object sender, string message)
        {
            var log = Context.Logs.Create();

            log.Sender = sender.GetType().Name;
            log.Message = message;
            log.Severity = "Exception";

            Context.Logs.Add(log);
        }

        /// <summary>
        /// Log an exception to the database
        /// </summary>
        public void LogException(object sender, Exception ex)
        {
            var log = Context.Logs.Create();

            log.Sender = sender.GetType().Name;
            log.Message = ex.Message;
            log.Severity = "Error";

            Context.Logs.Add(log);
        }
    }
}
