﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the comment model
    /// </summary>
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CommentRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        /// <summary>
        /// Return all the comments by a specific user id
        /// </summary>
        public IQueryable<Comment> FindAllByUserId(string userId)
        {
            IQueryable<Comment> results;

            try
            {
                results = from comments in Context.Comments
                          where comments.ApplicationUserId == userId
                          select comments;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CommentRepository::FindAllByUserId", ex.InnerException);
            }

            return results;
        }

        /// <summary>
        /// Return all the comments by a specific report id
        /// </summary>
        public IQueryable<Comment> FindAllByReportId(int reportId)
        {
            IQueryable<Comment> results;

            try
            {
                results = from comment in Context.Comments
                          where comment.ReportId == reportId
                          orderby comment.CreationDate descending
                          select comment;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CommentRepository::FindAllByReportId", ex.InnerException);
            }

            return results;
        }

        public int GetNComments(int reportId)
        {
            IQueryable<Comment> results;

            try
            {
                // This feels very inefficient
                results = from comment in Context.Comments
                          where comment.ReportId == reportId
                          select comment;

                return results.Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CommentRepository::GetNComments", ex.InnerException);
            }
        }

        public bool RemoveCommentsByReport(int reportId)
        {
            try
            {
                var query = from comment in Context.Comments where comment.ReportId == reportId select comment;
                if (query.Count() > 0)
                {
                    Context.Comments.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CommentRepository::RemoveCommentsByReport", ex.InnerException);
            }
        }

        public bool RemoveCommentsByUser(string userId)
        {

            try
            {
                var query = from comment in Context.Comments where comment.ApplicationUserId == userId select comment;
                if (query.Count() > 0)
                {
                    Context.Comments.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CommentRepository::RemoveCommentsByUser", ex.InnerException);
            }
        }
    }
}

