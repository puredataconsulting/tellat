﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;
using System;
using System.Linq;

using Framework2.Core.Extensions;
using Framework2.Azure.Config;
using Framework2.Azure.Exceptions;
using Framework2.Azure.Services.Interfaces;


namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the MediaItems
    /// </summary>
    public class MediaItemRepository : BaseRepository<MediaItem>, IMediaItemRepository
    {

        [Dependency]
        public IAzureBlobService AzureBlobService { get; set; }

        [Dependency]
        public IAzureMediaAssetService AzureMediaAssetService { get; set; }


        /// <summary>
        /// Constructor
        /// </summary>
        public MediaItemRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public bool RemoveMediaItemsByReport(int reportId)
        {
            try
            {
                var query = from mi in Context.MediaItems where mi.ReportID == reportId select mi;
                if (query.Count() > 0)
                {
                    foreach (MediaItem mediaItem in query)
                    {
                        RemoveMediaFromAzure(mediaItem);

                        Context.MediaItems.Remove(mediaItem);
                    }
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("MediaItemRepository::RemoveMediaItemsByReport", ex.InnerException);
            }

        }

        public void RemoveMediaItemsForUser(string userId)
        {
            try
            {
                var query = from mi in Context.MediaItems where mi.ApplicationUserId == userId select mi;
                if (query.Count() > 0)
                {
                    foreach (MediaItem mediaItem in query)
                    {
                        RemoveMediaFromAzure(mediaItem);

                        Context.MediaItems.Remove(mediaItem);
                    }
                    Context.SaveChanges();
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("MediaItemRepository::RemoveMediaItemsForUser", ex.InnerException);
            }
        }

        protected void RemoveMediaFromAzure(MediaItem mediaItem)
        {
                string container, item;
            ////// This delete is special - we need to unhook the item from Azure
            ////// ** Please leave this code here - it's redundant but the technique is interesting **
            ////if (!string.IsNullOrEmpty(mediaItem.Uri))
            ////{
            ////    // Which container do we need?
            ////    mediaItem.ParseMainUri(out container, out item);
            ////    AzureBlobService.RemoveFromContainer(container, item);

            ////    // There's probably only the video in this container
            ////    AzureBlobService.DeleteContainerIfEmpty(container);
            ////}
            ////if (!string.IsNullOrEmpty(mediaItem.ThumbnailUri))
            ////{
            ////    mediaItem.ParseThumbnailUri(out container, out item);
            ////    AzureBlobService.RemoveFromContainer(container, item);

            ////    // If this container was solely created to hold thumbnails (given that WAMS makes container names
            ////    // up on the hoof - we have no control), we can probably trash the other jpegs in there with impunity
            ////    // and also delete the container itself
            ////    AzureBlobService.RemoveContainerFilesByExtension(container, "jpg");
            ////    // That's probably emptied the container
            ////    AzureBlobService.DeleteContainerIfEmpty(container);

            ////}

            // When we created video media, we created encoding and thumbnail assets which we named predictably based
            // on the original media name. We remembered that name and hence can now tidy up
            if(!string.IsNullOrEmpty(mediaItem.AssociatedAssets))
            {
                AzureMediaAssetService.DeleteEncodingAndThumbnailAssets(mediaItem.AssociatedAssets);
            }
        }


    }
}
