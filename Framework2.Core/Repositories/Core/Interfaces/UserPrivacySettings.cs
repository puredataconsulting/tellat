﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// If a bit is set, the associated field will NOT be shown
    /// </summary>
    [Flags]
    public enum UserPrivacySettings
    {
        None = 0,
        YearOfBirth = 1, // To be shown as "Age 23"
        DateOfBirth = 2, // To be shown in full
        RoughLocation = 4, // E.g. Country
        ExactLocation = 8,  // E.g. Geocode off most recent report
        Gender = 16
    };
}
