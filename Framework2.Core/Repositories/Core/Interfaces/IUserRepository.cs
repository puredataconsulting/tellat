﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the UserModel
    /// </summary>
    public interface IUserRepository : IBaseRepository<User>
    {
        ///// <summary>
        ///// Finds a specific User in the database via their email address
        ///// </summary>
        //User FindByEmailAddress(string emailAddress);

        /// <summary>
        /// Finds a specific User in the database via their user name
        /// </summary>
        User FindByUserName(string userName);

        ///// <summary>
        ///// Finds a specific User in the database via their Twitter ID
        ///// </summary>
        //User FindByTwitterId(string twitterId);

        ///// <summary>
        ///// Finds a specific User in the database via the password hash
        ///// </summary>
        //User FindByPasswordHash(string passwordHash);

        /// <summary>
        /// Finds a specific User in the database via the session token
        /// </summary>
        User FindBySessionToken(string sessionToken);

        List<User> FindAllWithSort(UserSortCriterion sortCriterion);

        /// <summary>
        /// This is a HARD DELETE AND SHOULD NOT BE CALLED UNTIL THE USER HAS BEEN
        /// UNHOOKED FROM ALL RELATIONSHIPS
        /// //TODO would cascading deletes make this feasible?
        /// </summary>
        /// <param name="user"></param>
        bool RemoveUser(int userId);

    }
}
