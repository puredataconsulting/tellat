﻿using System.Collections.Generic;
using System.Web.Mvc;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the group model
    /// </summary>
    public interface ICategoryRepository : IBaseRepository<Category>
    {
        /// <summary>
        /// Return every category as a Razor select list
        /// </summary>
        List<SelectListItem> FindAllAsSelectList(int categoryId);

        /// <summary>
        /// Get the first nCategories categories - these will be listed as links on the footer
        /// </summary>
        /// <param name="nCategories"></param>
        /// <returns></returns>
        List<Category> GetFixedCategories(int nCategories);
    }
}
