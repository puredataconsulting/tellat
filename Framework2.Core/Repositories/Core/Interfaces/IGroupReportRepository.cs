﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    public interface IGroupReportRepository : IBaseRepository<GroupReport>
    {
        List<Report> FindReportsByGroup(int groupId, ReportSortCriterion sortCriterion, bool isPublicOnly);

        List<Group> FindGroupsByReport(int reportId);

        bool AddReportToGroup(int reportId, int groupId);

        bool RemoveReportFromGroup(int reportId, int groupId);

        bool RemoveReportFromAllGroups(int reportId);

        bool IsReportInGroup(int reportId, int groupId);

        List<SelectListItem> GetGroupsByReportAsSelectList(int reportId);

        List<int> GetGroupIdsByReport(int reportId);

        bool DeleteByReport(int reportId);

    }
}
