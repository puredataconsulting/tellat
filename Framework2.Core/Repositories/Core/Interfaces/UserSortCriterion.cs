﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{

    [Flags]
    public enum UserSortCriterion
    {
        None = 0,
        FirstName = 1,
        LastName = 2,
        UserName = 4,
        LastActivity = 8,
        //Acclaim = 16,
        DateAdded = 32,

        Ascending = 512,
        Descending = 1024
    };
}
