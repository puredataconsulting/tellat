﻿using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the group model
    /// </summary>
    public interface IGroupRepository : IBaseRepository<Group>
    {
       
    }
}
