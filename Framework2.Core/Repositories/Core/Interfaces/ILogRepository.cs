﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the log entry model
    /// </summary>
    public interface ILogRepository : IBaseRepository<Log>
    {
        /// <summary>
        /// Log a message to the database
        /// </summary>
        void LogMessage(object sender, string message);

        /// <summary>
        /// Log an error to the database
        /// </summary>
        void LogError(object sender, string message);

        /// <summary>
        /// Log an exception to the database
        /// </summary>
        void LogException(object sender, Exception ex);
    }
}
