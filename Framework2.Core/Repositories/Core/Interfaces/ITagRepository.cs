﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the Tag
    /// </summary>
    public interface ITagRepository : IBaseRepository<Tag>
    {
        /// <summary>
        /// Get the top nItems Tags
        /// <summary>
        List<Tag> ListByTrending(int nItems);

        /// <summary>
        /// Return every category as a Razor select list
        /// </summary>
        List<SelectListItem> FindAllAsSelectList(int selectedTagId);

        /// <summary>
        /// Does a tag with the given text already exist?
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        Tag FindByText(string text);

        /// <summary>
        /// Update the tag frequency data to reflect a hit at this time
        /// </summary>
        void RegisterTagHit(int tagId);
       
    }
}
