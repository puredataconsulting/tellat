﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the Follower
    /// </summary>
    public interface IFollowerRepository : IBaseRepository<Following>
    {
        bool CreateFollowing(string sourceId, string targetId);

        bool RemoveFollowing(string sourceId, string targetId);


        bool IsFollowing(string sourceId, string targetId);

        /// <summary>
        /// Which users are followING the user with id targetId (i.e. who is targetId followED BY)
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></returns>
        List<ApplicationUser> GetSourcesForTarget(string targetId, UserSortCriterion sortCriterion);

        /// <summary>
        /// Which users is the user with sourceId following?
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        List<ApplicationUser> GetTargetsForSource(string sourceId, UserSortCriterion sortCriterion);


        /// <summary>
        /// How many followers does targetId have?
        /// </summary>
        /// <param name="targetID"></param>
        /// <returns></returns>
        int GetNSourcesForTarget(string targetId);

        /// <summary>
        /// How many users is sourceID following?
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        int GetNTargetsForSource(string sourceId);

        /// <summary>
        /// Remove a following where this user's source or target
        /// </summary>
        /// <param name="userId"></param>
        void RemoveUser(string userId);
    }
}
