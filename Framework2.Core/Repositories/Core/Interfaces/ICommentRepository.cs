﻿using System;
using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the Comment
    /// </summary>
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        /// <summary>
        /// Return all the comments by a specific User
        /// </summary>
        IQueryable<Comment> FindAllByUserId(string userId);

        /// <summary>
        /// Return all the comments by a specific report id
        /// </summary>
        IQueryable<Comment> FindAllByReportId(int reportId);

        /// <summary>
        /// How many comments have been made on this report?
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        int GetNComments(int reportId);

        bool RemoveCommentsByReport(int reportId);
        bool RemoveCommentsByUser(string userId);
    }
}
