﻿using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the album model
    /// </summary>
    public interface IAlbumRepository : IBaseRepository<Album>
    {
        /// <summary>
        /// Return all the albums created by a specific user
        /// </summary>
        IQueryable<Album> FindAllByUserId(string userId);

        void RemoveAlbumsByUser(string userId);
    }
}
