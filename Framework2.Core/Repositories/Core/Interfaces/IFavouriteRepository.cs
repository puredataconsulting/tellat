﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;
using System.Linq;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the favourite model
    /// </summary>
    public interface IFavouriteRepository : IBaseRepository<Favourite>
    {
        /// <summary>
        /// List favourites by userId in descending order of rating
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<Favourite> FindAllByUserIdByRating(string userId);

        /// <summary>
        /// Create a rating link between the user and the report
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <param name="rating"></param>
        /// <returns></returns>
        bool AddRating(string userId, int reportId, int rating);

        /// <summary>
        /// Has this user rated this report? 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <param name="rating">Provided rating, -1 if none available</param>
        /// <returns></returns>
        bool FindRating(string userId, int reportId, out int rating);


        /// <summary>
        /// Force the favouriting between the user and the report based on isFavourite
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <param name="isFavourite"></param>
        /// <returns></returns>
        void SetFavourite(string userId, int reportId, bool isFavourite);

        /// <summary>
        /// Is this report a favourite of the user?
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        bool IsFavourite(string userId, int reportId);


        /// <summary>
        /// How many reports has this user rated?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int GetNRatedByUser(string userId);

        /// <summary>
        /// How many has the user favourited? 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int GetNFavouritesByUser(string userId);

        List<Report> GetFavouriteReportsByUser(string userId);

        /// <summary>
        /// How many times has the given report been favourited?
        /// </summary>
        /// <param name="reportId"></param>
        /// <returns></returns>
        int GetNFavouritesByReport(int reportId);


        bool RemoveFavouritesByReport(int reportId);

        bool RemoveFavouritesByUser(string userId);

    }
}
