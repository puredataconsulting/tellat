﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {

        void AddNotification(string userId, NotificationTypes notificationType, string senderId, string reference);

        List<Notification> SeekNotificationsForUser(string userId, NotificationTypes notificationType);

        bool RemoveNotification(int notificationId);

    }
}
