﻿using System.Collections.Generic;
using System.Web.Mvc;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the Category
    /// </summary>
    public interface ICountryRepository : IBaseRepository<Country>
    {
        /// <summary>
        /// Return every country as a Razor select list
        /// </summary>
        List<SelectListItem> FindAllAsSelectList(int countryId);
    }
}
