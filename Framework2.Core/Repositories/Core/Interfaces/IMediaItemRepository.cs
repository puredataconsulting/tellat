﻿using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the member model
    /// </summary>
    public interface IMediaItemRepository : IBaseRepository<MediaItem>
    {
        bool RemoveMediaItemsByReport(int reportId);

        /// <summary>
        /// The user typically has one media item that constitutes his profile image.
        /// //TODO These should really be in a default album.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        void RemoveMediaItemsForUser(string userId);
    }
}
