﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for  Tagging
    /// </summary>
    public interface ITaggingRepository : IBaseRepository<Tagging>
    {
        List<Report> FindReportsByTag(int tagId, ReportSortCriterion sortCriterion);

        List<Tag> FindTagsByReport(int reportId);


        List<Tagging> FindTaggingsByReport(int reportId);

        bool IsTaggingInPlace(int tagId, int reportID);

        bool RemoveTagging(int tagId, int reportID);

        bool RemoveTaggingsByReport(int reportID);


    }
}
