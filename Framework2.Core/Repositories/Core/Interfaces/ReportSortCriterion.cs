﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    [Flags]
    public enum ReportSortCriterion
    {
        None = 0,
        Author = 1,
        Title = 2,
        DateAdded = 4,
        NViewings = 16,
        Rating = 32,

        Public = 64,
        Private = 128,
        Final = 256,
        Draft = 512,

        Ascending = 4096,
        Descending = 8192
    };
}
