﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    public enum GenderTypes
    {
        Male,
        Female,
        Questioning
    }
}
