﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    public interface IReportCategoryRepository : IBaseRepository<ReportCategory>
    {
        List<Report> FindReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly);

        List<Report> FindFeaturedReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly);
        List<Report> FindNotFeaturedReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly);


        List<Category> FindCategoriesByReport(int reportId);

         /// <summary>
        /// Find the first featured report in this category, or in the absence of a featured report 
        /// find the most viewed
        /// MAY RETURN NULL if neither condition applies
        /// 
        /// </summary>
        Report FindPrimaryReportByCategoryId(int categoryId);

        bool AddReportToCategory(int reportId, int categoryId);

        bool RemoveReportFromCategory(int reportId, int categoryId);

        bool RemoveReportFromAllCategories(int reportId);

        bool IsReportInCategory(int reportId, int categoryId);


        List<SelectListItem> GetCategoriesByReportAsSelectList(int reportId);

        List<int> GetCategoryIdsByReport(int reportId);

        bool DeleteByReport(int reportId);

    }
}
