﻿using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the member model
    /// </summary>
    public interface IGroupMemberRepository : IBaseRepository<GroupMember>
    {
       /// <summary>
        /// Find all members of a specific group
        /// </summary>
        List<ApplicationUser> FindAllByGroupId(int groupId, UserSortCriterion sortCriterion);
        

        /// <summary>
        /// Find all administrators of a specific group
        /// </summary>
        List<ApplicationUser> FindOwnersByGroupId(int groupId);

        /// <summary>
        /// Find all the groups a given user features in
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<Group> FindGroupsByUserId(string userId);

        /// <summary>
        /// Is this user a member of this group?
        /// **If** the return code is true, isAdministrator contains the user's status in the group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        bool IsMember(string userId, int groupId, out bool isAdministrator);

        /// <summary>
        /// Makes a link between the given user and the given group. 
        /// IF the user already belongs to the group or the operation fails this returns false
        /// otherwise true
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <param name="isAdministrator"></param>
        /// <returns></returns>
        bool Join(string userId, int groupId, bool isAdministrator);

        void RemoveUserFromGroup(string userId, int groupId);

        void RemoveUserFromAllGroups(string userId);

    }
}
