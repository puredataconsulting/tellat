﻿using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core.Interfaces
{
public interface IApplicationUserRepository
    {
        // This has to unhook the user gracefully, effectively doing the job of the cascading delete
        // IT DOES NOT DELETE THE USER, merely extracting him from all relationships
        bool DecoupleUser(ApplicationUser user);


    }
}
