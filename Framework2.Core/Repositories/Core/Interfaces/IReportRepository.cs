﻿using System.Linq;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core.Interfaces
{
    /// <summary>
    /// Provides the database functionality for the video model
    /// </summary>
    public interface IReportRepository : IBaseRepository<Report>
    {

        /// <summary>
        /// Return the current featured reports
        /// </summary>
        List<Report> FindByIsFeatured(int? count);

        /// <summary>
        /// Return all reports by a specific user id
        /// </summary>
        List<Report> FindAllByUserId(string userId, ReportSortCriterion sortCriterion, bool isPublicOnly);

        /// <summary>
        /// Return all reports sorted
        /// </summary>
        List<Report> FindAllSorted(ReportSortCriterion sortCriterion, bool isPublicOnly);

        /// <summary>
        /// Find the most viewed report
        /// </summary>
        /// <returns></returns>
        Report FindMostViewed();

        /// <summary>
        /// Return a list of reports that match the string(s) encoded in searchTerms
        /// and return them sorted as requested
        /// </summary>
        /// <param name="searchTerms"></param>
        /// <returns></returns>
        List<Report> DoSearch(string searchTerms, ReportSortCriterion sortCriterion, bool isPublicOnly);

        /// <summary>
        /// We keep data on the Report that we could derive from the Favourite relationship - this is for efficiency
        /// After a rating Favourite link has been added, a call to this updates the Report's running totals
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="rating"></param>
        void RegisterNewRating(int reportId, int rating);

        /// <summary>
        /// How many of my reports are published (i.e. complete AND public)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lifeCycleStage"></param>
        /// <returns></returns>
        int GetNPublished(string userId);

        /// <summary>
        /// How many of my reports are unpublished (i.e. incomplete OR private)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lifeCycleStage"></param>
        /// <returns></returns>
        int GetNUnpublished(string userId);

        /// <summary>
        /// For all the reports this user has, how many ratings are there in total?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int CountRatingsForUser(string userId);

        /// <summary>
        /// For all the reports this user has, how many viewings are there in total?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        int CountViewingsForUser(string userId);

        /// <summary>
        /// Find reports which share a tag or tags with this one and if all else fails a category
        /// Returns up to nReports reports
        /// </summary>
        /// <param name="reportID"></param>
        /// <returns></returns>
        List<Report> FindRelatedReports(int reportId, int nReports);

        bool RemoveReport(int reportId);


    }
}
