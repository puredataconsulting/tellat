﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the User
    /// </summary>
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public UserRepository(IContextManager contextManager)
            : base(contextManager) 
        {
        }
        
        ///// <summary>
        ///// Finds a specific User in the database via their email address
        ///// </summary>
        //public User FindByEmailAddress(string emailAddress)
        //{
        //    //TODO
        //    User user = null;

        //    //try
        //    //{
        //    //    user = Context.Users.FirstOrDefault(u => u.EmailAddress == emailAddress);
        //    //}
        //    //catch (InvalidOperationException ex)
        //    //{
        //    //    throw new RepositoryException("UserRepository::FindByEmailAddress", ex.InnerException);
        //    //}

        //    return user;
        //}

        /// <summary>
        /// Finds a specific User in the database via their user name
        /// </summary>
        public User FindByUserName(string userName)
        {
            User user = null;

            try
            {
                user = Context.Users.FirstOrDefault(u => u.Username == userName);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("UserRepository::FindByUserName", ex.InnerException);
            }

            return user;
        }

        ///// <summary>
        ///// Finds a specific User in the database via their Twitter ID
        ///// </summary>
        //public User FindByTwitterId(string twitterId)
        //{
        //    User user = null;

        //    //try
        //    //{
        //    //    user = Context.Users.FirstOrDefault(u => u.TwitterId == twitterId);
        //    //}
        //    //catch (InvalidOperationException ex)
        //    //{
        //    //    throw new RepositoryException("UserRepository::FindByFacebookId", ex.InnerException);
        //    //}

        //    return user;
        //}

        ///// <summary>
        ///// Finds a specific User in the database via the password hash
        ///// </summary>
        //public User FindByPasswordHash(string passwordHash)
        //{
        //    User user = null;

        //    //try
        //    //{
        //    //    user = Context.Users.FirstOrDefault(u => u.PasswordHash == passwordHash);
        //    //}
        //    //catch (InvalidOperationException ex)
        //    //{
        //    //    throw new RepositoryException("UserRepository::FindByPasswordHash", ex.InnerException);
        //    //}

        //    return user;
        //}

        /// <summary>
        /// Finds a specific User in the database via the session token
        /// </summary>
        public User FindBySessionToken(string sessionToken)
        {
            User user;

            try
            {
                user = Context.Users.FirstOrDefault(u => u.SessionToken == sessionToken);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("UserRepository::SessionToken", ex.InnerException);
            }

            return user;
        }
        /*
         None = 0,
            FirstName = 1,
            LastName = 2,
            UserName = 4,
            LastActivity = 8,
            //Acclaim = 16,
            DateAdded = 32,

            Ascending = 512,
            Descending = 1024
         */
        public static List<User> FilterSort(IQueryable<User> originalList, UserSortCriterion sortCriterion)
        {
            IQueryable<User> results = originalList;

            if ((sortCriterion & UserSortCriterion.FirstName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.FirstName);
                }
                else
                {
                    results = results.OrderByDescending(u => u.FirstName);
                }
            }
            else if ((sortCriterion & UserSortCriterion.LastName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.LastName);
                }
                else
                {
                    results = results.OrderByDescending(u => u.LastName);
                }
            }
            else if ((sortCriterion & UserSortCriterion.UserName) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.Username);
                }
                else
                {
                    results = results.OrderByDescending(u => u.Username);
                }
            }
            else if ((sortCriterion & UserSortCriterion.LastActivity) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.LastActivity);
                }
                else
                {
                    results = results.OrderByDescending(u => u.LastActivity);
                }
            }
            else if ((sortCriterion & UserSortCriterion.DateAdded) != 0)
            {
                if ((sortCriterion & UserSortCriterion.Ascending) != 0)
                {
                    results = results.OrderBy(u => u.CreationDate);
                }
                else
                {
                    results = results.OrderByDescending(u => u.CreationDate);
                }
            }


            return results.ToList();
        }

        public List<User> FindAllWithSort(UserSortCriterion sortCriterion)
        {
            return FilterSort(FindAll(), sortCriterion);
        }

        public bool RemoveUser(int userId)
        {
            bool success = false;
            try
            {
                var query = from user in Context.Users where user.ID == userId select user;
                if(query.Count() > 0)
                {
                    Context.Users.RemoveRange(query);
                    Context.SaveChanges();
                    success = true;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("UserRepository::RemoveUser", ex.InnerException);
            }

            return success;
        }

    }
}
