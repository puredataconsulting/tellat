﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the Country
    /// </summary>
    public class CountryRepository : BaseRepository<Country>, ICountryRepository
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public CountryRepository(IContextManager contextManager)
            : base(contextManager) 
        {
        }

        /// <summary>
        /// Return every country as a Razor select list
        /// </summary>
        public List<SelectListItem> FindAllAsSelectList(int countryId)
        {
            List<SelectListItem> items;

            try
            {
                var teams = FindAll().ToList();
                items = teams.Select(t => new SelectListItem
                {
                    Text = t.Name,
                    Value = t.ID.ToString(),
                    Selected = t.ID == countryId
                }).ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("CountryRepository::FindAllAsSelectList", ex.InnerException);
            }

            return items;
        }
    }
}
