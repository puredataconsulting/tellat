﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Repositories.Core
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {

        public NotificationRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public List<Notification> SeekNotificationsForUser(string userId, NotificationTypes notificationType)
        {
            try
            {
                var query = from note in Context.Notifications
                            where note.ApplicationUserId == userId && note.NotificationType == notificationType
                            select note;
                return query.ToList();
            }
            catch(Exception ex)
            {
                throw new RepositoryException("NotificationRepository::SeekNotificationsForUser", ex.InnerException);
            }
        }

        public void AddNotification(string userId, NotificationTypes notificationType, string senderId, string reference)
        {
            try
            {
                Notification notification = Create();
                notification.ApplicationUserId = userId;
                notification.Sender = senderId;
                notification.Reference = reference;
                notification.NotificationType = notificationType;
                Add(notification);
            }
            catch (Exception ex)
            {
                throw new RepositoryException("NotificationRepository::AddNotification", ex.InnerException);
            }

        }

        /// <summary>
        /// Hard delete. Be careful.
        /// </summary>
        /// <param name="notificationId"></param>
        /// <returns></returns>
        public bool RemoveNotification(int notificationId)
        {
            try
            {
                var query = from note in Context.Notifications where note.ID == notificationId select note;
                if (query.Count() > 0)
                {
                    Context.Notifications.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("NotificationRepository::RemoveNotification", ex.InnerException);
            }
        }
    }
}
