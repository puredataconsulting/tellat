﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Framework2.Core.Repositories.Core
{
    public class ReportCategoryRepository : BaseRepository<ReportCategory>, IReportCategoryRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ReportCategoryRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public List<Category> FindCategoriesByReport(int reportId)
        {
            try
            {
                var query = from reportCategory in Context.ReportCategories where reportCategory.ReportID == reportId select reportCategory.Category;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::FindCategoriesByReport", ex.InnerException);
            }
        }


        public List<Report> FindReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            IQueryable<Report> query;
            try
            {
                if (isPublicOnly)
                {
                    query = from reportCategory in Context.ReportCategories where reportCategory.CategoryID == categoryId 
                            && reportCategory.Report.LifeCycleStage == Report.LifeCycle.Complete
                            && reportCategory.Report.Scoping == Report.Scope.Public
                            select reportCategory.Report;
                }
                else
                {
                    query = from reportCategory in Context.ReportCategories where reportCategory.CategoryID == categoryId select reportCategory.Report;
                }
                return ReportRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::FindReportsByCategory", ex.InnerException);
            }
        }

        public List<Report> FindFeaturedReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            IQueryable<Report> query;
            try
            {
                if (isPublicOnly)
                {
                    query = from reportCategory in Context.ReportCategories 
                            where reportCategory.CategoryID == categoryId && reportCategory.Report.IsFeatured
                                && reportCategory.Report.LifeCycleStage == Report.LifeCycle.Complete
                                && reportCategory.Report.Scoping == Report.Scope.Public
                            select reportCategory.Report;
                }
                else
                {
                    query = from reportCategory in Context.ReportCategories where reportCategory.CategoryID == categoryId 
                            && reportCategory.Report.IsFeatured
                            select reportCategory.Report;
                }
                return ReportRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::FindFeaturedReportsByCategory", ex.InnerException);
            }

        }
        public List<Report> FindNotFeaturedReportsByCategory(int categoryId, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            IQueryable<Report> query;
            try
            {
                if (isPublicOnly)
                {
                    query = from reportCategory in Context.ReportCategories
                            where reportCategory.CategoryID == categoryId && !reportCategory.Report.IsFeatured
                                && reportCategory.Report.LifeCycleStage == Report.LifeCycle.Complete
                                && reportCategory.Report.Scoping == Report.Scope.Public
                            select reportCategory.Report;
                }
                else
                {
                    query = from reportCategory in Context.ReportCategories where reportCategory.CategoryID == categoryId 
                            && !reportCategory.Report.IsFeatured
                            select reportCategory.Report;
                }
                return ReportRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::FindNotFeaturedReportsByCategory", ex.InnerException);
            }
        }

        /// <summary>
        /// Find the first featured report in this category, or in the absence of a featured report 
        /// find the most viewed
        /// MAY RETURN NULL if neither condition applies
        /// 
        /// </summary>
        public Report FindPrimaryReportByCategoryId(int categoryId)
        {
            Report result = null;

            List<Report> reports = FindReportsByCategory(categoryId, ReportSortCriterion.None, true);
            try
            {
                var query = from report in reports where report.IsFeatured select report;

                if (query.Count() == 0)
                {
                    var secondaryQuery = from report in reports orderby report.NViewings descending select report;
                    result = secondaryQuery.FirstOrDefault();
                }
                else result = query.First();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::FindPrimaryReportByCategoryId", ex.InnerException);
            }

            return result;
        }

        public bool AddReportToCategory(int reportId, int categoryId)
        {
            bool success = false;
            try
            {
                var query = from rc in Context.ReportCategories where rc.CategoryID == categoryId && rc.ReportID == reportId select rc.Report;
                if (query.Count() == 0)
                {
                    ReportCategory rcNew = Create();
                    rcNew.ReportID = reportId;
                    rcNew.CategoryID = categoryId;
                    Add(rcNew);
                    success = true;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::AddReportToCategory", ex.InnerException);
            }

            return success;
        }

        public bool RemoveReportFromCategory(int reportId, int categoryId)
        {
            try
            {
                var query = from rc in Context.ReportCategories where rc.CategoryID == categoryId && rc.ReportID == reportId select rc;
                foreach (var rc in query)
                {
                    // Hard Delete
                    Context.ReportCategories.Remove(rc);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::RemoveReportFromCategory", ex.InnerException);
            }
            return true;
        }

        public bool RemoveReportFromAllCategories(int reportId)
        {
            try
            {
                var query = from rc in Context.ReportCategories where rc.ReportID == reportId select rc;
                foreach (var rc in query)
                {
                    // Hard Delete
                    Context.ReportCategories.Remove(rc);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::RemoveReportFromAllCategories", ex.InnerException);
            }
            return true;
        }

        public bool IsReportInCategory(int reportId, int categoryId)
        {
            try
            {
                var query = from rc in Context.ReportCategories where rc.CategoryID == categoryId && rc.ReportID == reportId select rc;
                return query.Count() > 0;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::IsReportInCategory", ex.InnerException);
            }
        }
        public List<SelectListItem> GetCategoriesByReportAsSelectList(int reportId)
        {
            try
            {
                // Note the bodge on string conversion here - ToString has no meaning for Linq
                var query = from rc in Context.ReportCategories where rc.ReportID == reportId select new SelectListItem { Text = rc.Category.Title, Value = SqlFunctions.StringConvert((double)rc.CategoryID) };
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::GetCategoriesByReportAsSelectList", ex.InnerException);
            }
        }

        public List<int> GetCategoryIdsByReport(int reportId)
        {
            try
            {
                var query = from rc in Context.ReportCategories where rc.ReportID == reportId select rc.CategoryID;
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::GetCategoryIdsByReport", ex.InnerException);
            }
        }

        public bool DeleteByReport(int reportId)
        {
            try
            {
                var query = from rc in Context.ReportCategories where rc.ReportID == reportId select rc;
                if (query.Count() > 0)
                {
                    // Hard delete
                     Context.ReportCategories.RemoveRange(query);
                     Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportCategoryRepository::DeleteByReport", ex.InnerException);
            }
        }
    }
}
