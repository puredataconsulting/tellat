﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the favourites
    /// </summary>
    public class FavouriteRepository : BaseRepository<Favourite>, IFavouriteRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public FavouriteRepository(IContextManager contextManager)
            : base(contextManager) 
        {
        }

        public bool AddRating(string userId, int reportId, int rating)
        {
            if(rating < 0 || rating > 5)
            {
                throw new ArgumentOutOfRangeException("Rating");
            }
            IQueryable<Favourite> query;
            bool success = false;
            try
            {
                query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.ReportId == reportId select fave;
                if(query.Count() == 0)
                {
                    // There wasn't an existing rating
                    Favourite link = Create();
                    link.ApplicationUserId = userId;
                    link.ReportId = reportId;
                    link.Rating = rating;
                    Add(link);
                    success = true;
                }
                else
                {
                    // Update the existing rating
                    Favourite link = query.First();
                    link.Rating = rating;
                    Update(link);
                    success = true;
                }
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::AddRating", ex.InnerException);
            }
            return success;
        }

        public List<Favourite> FindAllByUserIdByRating(string userId)
        {
            IQueryable<Favourite> query;
            try
            {
                query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.Rating >= 0 orderby fave.Rating descending select fave;
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::FindAllByUserIdByRating", ex.InnerException);
            }
            return query.ToList();
        }

        public bool FindRating(string userId, int reportId, out int rating)
        {
            IQueryable<Favourite> query;
            Favourite link = null;
            try
            {
                query = from fave in Context.Favourites 
                        where fave.ApplicationUserId == userId && fave.ReportId == reportId && fave.Rating >= 0
                        select fave;
                link = query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::FindRating", ex.InnerException);
            }

            if(link == null)
            {
                rating = -1;
                return false;
            }
            else
            {
                rating = link.Rating;
                return true;
            }
        }


        public void SetFavourite(string userId, int reportId, bool isFavourite)
        {
            IQueryable<Favourite> query;
            try
            {
                query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.ReportId == reportId select fave;
                if (query.Count() == 0)
                {
                    if (isFavourite)
                    {
                        // There wasn't an existing link to this report - if we're setting a fave we need to create one
                        Favourite link = Create();
                        link.ApplicationUserId = userId;
                        link.ReportId = reportId;
                        link.IsFavourite = isFavourite;
                        Add(link);
                    }
                }
                else
                {
                    // Modify the existing link
                    Favourite favourite = query.First();
                    favourite.IsFavourite = isFavourite;
                    Update(favourite);
                }
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::SetFavourite", ex.InnerException);
            }

        }


        /// <summary>
        /// Is this report a favourite of the user?
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="reportId"></param>
        /// <returns></returns>
        public bool IsFavourite(string userId, int reportId)
        {
            IQueryable<Favourite> query;
            Favourite link = null;
            try
            {
                query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.ReportId == reportId select fave;
                link = query.FirstOrDefault();
                if (link == null) return false;
                else return link.IsFavourite;
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::IsFavourite", ex.InnerException);
            }
        }

        /// <summary>
        /// How many reports has this user rated?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
       public int GetNRatedByUser(string userId)
        {
            int nRated = 0;
            IQueryable<Favourite> query;
            try
            {
                query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.Rating >= 0 select fave;
                nRated = query.Count();
            }
            catch (Exception ex)
            {
                throw new RepositoryException("FavouriteRepository::GetNRatedByUser", ex.InnerException);
            }
            return nRated;
        }

       public int GetNFavouritesByUser(string userId)
       {
           int nRated = 0;
           IQueryable<Favourite> query;
           try
           {
               query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.IsFavourite select fave;
               nRated = query.Count();
           }
           catch (Exception ex)
           {
               throw new RepositoryException("FavouriteRepository::GetNFavouritesByUser", ex.InnerException);
           }
           return nRated;
       }

       public List<Report> GetFavouriteReportsByUser(string userId)
       {
           IQueryable<Report> query;
           try
           {
               query = from fave in Context.Favourites where fave.ApplicationUserId == userId && fave.IsFavourite select fave.Report;
               return query.ToList();
           }
           catch (Exception ex)
           {
               throw new RepositoryException("FavouriteRepository::GetFavouritesByUser", ex.InnerException);
           }

       }

       public bool RemoveFavouritesByReport(int reportId)
       {
           try
           {
               var query = from fave in Context.Favourites where fave.ReportId == reportId select fave;
               if (query.Count() > 0)
               {
                   Context.Favourites.RemoveRange(query);
                   Context.SaveChanges();
                   return true;
               }
               else return false;
           }
           catch (InvalidOperationException ex)
           {
               throw new RepositoryException("FavouriteRepository::RemoveFavouritesByReport", ex.InnerException);
           }
       }

       public int GetNFavouritesByReport(int reportId)
       {
           int nRated = 0;
           IQueryable<Favourite> query;
           try
           {
               query = from fave in Context.Favourites where fave.ReportId == reportId && fave.IsFavourite select fave;
               nRated = query.Count();
           }
           catch (Exception ex)
           {
               throw new RepositoryException("FavouriteRepository::GetNFavouritesByReport", ex.InnerException);
           }
           return nRated;
       }

       public bool RemoveFavouritesByUser(string userId)
       {
           try
           {
               var query = from fave in Context.Favourites where fave.ApplicationUserId == userId select fave;
               if (query.Count() > 0)
               {
                   Context.Favourites.RemoveRange(query);
                   Context.SaveChanges();
                   return true;
               }
               else return false;
           }
           catch (InvalidOperationException ex)
           {
               throw new RepositoryException("FavouriteRepository::RemoveFavouritesByUser", ex.InnerException);
           }

       }
    }
}
