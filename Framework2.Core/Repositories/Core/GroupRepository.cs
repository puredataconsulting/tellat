﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System.Collections.Generic;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the Group
    /// </summary>
    public class GroupRepository : BaseRepository<Group>, IGroupRepository
    {
       
        /// <summary>
        /// Default constructor
        /// </summary>
        public GroupRepository(IContextManager contextManager)
            : base(contextManager) 
        {
        }

    }
}
