﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the Tag
    /// </summary>
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TagRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        public List<Tag> ListByTrending(int nItems)
        {
            try
            {
                var query = (from tag in Context.Tags orderby tag.HitRate descending select tag).Take(nItems);
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TagRepository::ListByTrending", ex.InnerException);
            }

        }

        /// <summary>
        /// Return every Tag as a Razor select list
        /// </summary>
        public List<SelectListItem> FindAllAsSelectList(int selectedTagId)
        {
            try
            {
                return Context.Tags.Select(tag => new SelectListItem()
                            {
                                Text = tag.Text,
                                Value = tag.ID.ToString(),
                                Selected = tag.ID == selectedTagId
                            }).ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TagRepository::FindAllAsSelectList", ex.InnerException);
            }
        }


        /// <summary>
        /// Does a tag with the given text already exist? Case insensitive. Note we should also weed out common variants, plurals, misspellings etc
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public Tag FindByText(string text)
        {
            if (string.IsNullOrEmpty(text)) return null;
            try
            {
                // The # is an artificial construct and has no place in the data!
                string searchText = text[0] == '#' ? text.Substring(1) : text;
                var query = from tag in Context.Tags where string.Compare(text, tag.Text, true) == 0 select tag;
                return query.FirstOrDefault();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TagRepository::FindByText", ex.InnerException);
            }
        }

        /// <summary>
        /// Update the tag frequency data to reflect a hit at this time
        /// // Every time we register a hit on a tag, we update NHitsInCurrentInterval IF that hit falls within the current interval
        /// If it falls after the current interval, we copy the hit count from the current interval into the hit count for the previous interval
        /// and set NHitsInCurrentInterval to 1
        /// This allows us to cross interval boundaries with no loss of continuity
        /// In more detail: if our interval is one day, and we're registering a hit on a tag, and that hit is today, we add one to NHitsInCurrentInterval
        /// When we calculate the hit rate it's (NHitsInPreviousInterval + NHitsInCurrentInterval) / (2 * intervalSeconds)
        /// If the hit is later than LastIntervalBoundary + one day, we've started a new interval
        /// We need to avoid the situation where a hit after midnight looks like one hit in the last 24 hours
        /// hence we keep track of the hits in the previous interval and serve up an average hit rate based on the two intervals
        /// When we start a new interval, we copy NHitsInCurrentInterval onto NHitsInPreviousInterval and continue thus with some "memory"
        /// of what happened in the last 24 hours.
        /// If a whole interval goes by with no hits and then we get one, NHitsInPreviousInterval should be set 0
        /// LastIntervalBoundary might be midnight this morning, so the last interval corresponds to yesterday and the current interval to today
        /// </summary>
        public void RegisterTagHit(int tagId)
        {
            try
            {
                Tag hitTag = FindById(tagId);
                if (hitTag == null) return;

                DateTime hitTime = DateTime.Now;
                // Does this fall in the current interval?
                double nIntervals = (hitTime - hitTag.LastIntervalBoundary).TotalHours / Tag.Interval.TotalHours;
                if (nIntervals < 1)
                {
                    // Yip, it's a "modern" hit
                    hitTag.NHitsInCurrentInterval++;
                }
                else if (nIntervals >= 1 && nIntervals < 2)
                {
                    // It's more than one interval but less than 2 since it was hit
                    // Take the data from what was the current interval into the previous one
                    hitTag.NHitsInPreviousInterval = hitTag.NHitsInCurrentInterval;
                    hitTag.NHitsInCurrentInterval = 1;
                    hitTag.LastIntervalBoundary = DateTime.Now;
                }
                else
                {
                    // It's been a long while since it was hit
                    hitTag.NHitsInPreviousInterval = 0;
                    hitTag.NHitsInCurrentInterval = 1;
                    hitTag.LastIntervalBoundary = DateTime.Now;
                }

                // Update the frequency calculation
                hitTag.HitRate = (float)((hitTag.NHitsInPreviousInterval + hitTag.NHitsInCurrentInterval) / (2 * Tag.Interval.TotalHours));
                Update(hitTag);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("TagRepository::RegisterTagHit", ex.InnerException);
            }
        }

    }
}
