﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System.Collections.Generic;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the Follower
    /// </summary>
    public class FollowerRepository : BaseRepository<Following>, IFollowerRepository
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public FollowerRepository(IContextManager contextManager)
            : base(contextManager) 
        {
        }

        public bool CreateFollowing(string sourceId, string targetId)
        {
            try
            {
                if(sourceId == targetId)return false;

                // If there's already an establised following or I'm trying to follow myself, just say no.
                var query = from follower in Context.Followings 
                            where follower.ApplicationUserId == sourceId && follower.TargetId == targetId 
                            select follower;

                if (query.Count() > 0) return false;

                Following following = Context.Followings.Create();
                following.ApplicationUserId = sourceId;
                following.TargetId = targetId;
                this.Add(following);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::CreateFollowing", ex.InnerException);
            }
            return true;
            
        }

        public bool IsFollowing(string sourceId, string targetId)
        {
            try
            {
                var query = from follower in Context.Followings where follower.TargetId == targetId && follower.ApplicationUserId == sourceId select follower;
                return query.Count() > 0;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::IsFollowing", ex.InnerException);
            }
        }

        public bool RemoveFollowing(string sourceId, string targetId)
        {
            try
            {
                var query = from follower in Context.Followings where follower.TargetId == targetId && follower.ApplicationUserId == sourceId select follower;
                Following following = query.FirstOrDefault();
                if (following != null)
                {
                    Context.Followings.Remove(following);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::IsFollowing", ex.InnerException);
            }
        }


        /// <summary>
        /// Which users are followING the user with id targetId (i.e. who is targetId followED BY)
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></r
        public List<ApplicationUser> GetSourcesForTarget(string targetId, UserSortCriterion sortCriterion)
        {
            try
            {
                var query = from follower in Context.Followings where follower.TargetId == targetId select follower.ApplicationUser;
                return ApplicationUserRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::GetSourcesForTarget", ex.InnerException);
            }
        }
        /// <summary>
        /// Which users is the user with sourceId following?
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public List<ApplicationUser> GetTargetsForSource(string sourceId, UserSortCriterion sortCriterion)
        {
            try
            {
                var query = from follower in Context.Followings where follower.ApplicationUserId == sourceId select follower.Target;
                return ApplicationUserRepository.FilterSort(query, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::GetTargetsForSource", ex.InnerException);
            }
        }


        /// <summary>
        /// How many followers does targetId have?
        /// </summary>
        /// <param name="targetID"></param>
        /// <returns></returns>
        public int GetNSourcesForTarget(string targetId)
        {
            try
            {
                var query = from follower in Context.Followings where follower.TargetId == targetId select follower.ApplicationUserId;
                return query.Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::GetNSourcesForTarget", ex.InnerException);
            }
        }

        /// <summary>
        /// How many users is sourceID following?
        /// </summary>
        /// <param name="sourceId"></param>
        /// <returns></returns>
        public int GetNTargetsForSource(string sourceId)
        {
            try
            {
                var query = from follower in Context.Followings where follower.ApplicationUserId == sourceId select follower.Target;
                return query.Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::GetNTargetsForSource", ex.InnerException);
            }
        }

        public void RemoveUser(string userId)
        {
            try
            {
                var query = from follower in Context.Followings where follower.ApplicationUserId == userId || follower.TargetId == userId select follower;
                foreach(var follower in query)
                {
                    Context.Followings.Remove(follower);
                }
                Context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("FollowerRepository::RemoveUser", ex.InnerException);
            }
        }

    }
}
