﻿using System;
using System.Linq;
using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System.Collections.Generic;

using Framework2.Core.Extensions;

namespace Framework2.Core.Repositories.Core
{
    /// <summary>
    /// Provides the database functionality for the videos
    /// </summary>
    public class ReportRepository : BaseRepository<Report>, IReportRepository
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ReportRepository(IContextManager contextManager)
            : base(contextManager)
        {
        }

        /// <summary>
        /// Return the current featured report
        /// </summary>
        public List<Report> FindByIsFeatured(int? count)
        {

            try
            {
                var query = from report in Context.Reports where report.IsFeatured select report;
                if(count.HasValue)
                {
                    query = query.Take(count.Value);
                }
                return query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindByIsFeatured", ex.InnerException);
            }
        }

        public static List<Report> FilterSort(IQueryable<Report> results, ReportSortCriterion sortCriterion)
        {
            try
            {
             if((sortCriterion & ReportSortCriterion.Author) != 0)
                {
                    if ((sortCriterion & ReportSortCriterion.Ascending) != 0)
                    {
                        results = results.OrderBy(m => m.User.UserName);
                    }
                    else
                    {
                        results = results.OrderByDescending(m => m.User.UserName);
                    }
                }
                else if ((sortCriterion & ReportSortCriterion.Title) != 0)
                {
                    if ((sortCriterion & ReportSortCriterion.Ascending) != 0)
                    {
                        results = results.OrderBy(m => m.Title);
                    }
                    else
                    {
                        results = results.OrderByDescending(m => m.Title);
                    }
                }
                else if ((sortCriterion & ReportSortCriterion.DateAdded) != 0)
                {

                    if ((sortCriterion & ReportSortCriterion.Ascending) != 0)
                    {
                        results = results.OrderBy(m => m.CreationDate);
                    }
                    else
                    {
                        results = results.OrderByDescending(m => m.CreationDate);
                    }
                }
                else if ((sortCriterion & ReportSortCriterion.NViewings) != 0)
                {

                    if ((sortCriterion & ReportSortCriterion.Ascending) != 0)
                    {
                        results = results.OrderBy(m => m.NViewings);
                    }
                    else
                    {
                        results = results.OrderByDescending(m => m.NViewings);
                    }
                }
                else if ((sortCriterion & ReportSortCriterion.Rating) != 0)
                {

                    if ((sortCriterion & ReportSortCriterion.Ascending) != 0)
                    {
                        results = results.OrderBy(m => m.NRatings);
                    }
                    else
                    {
                        results = results.OrderByDescending(m => m.NRatings);
                    }
                }

                else if ((sortCriterion & ReportSortCriterion.Public) != 0)
                {
                    results = results.Where(m => (m.Scoping == Report.Scope.Public));
                }

                else if ((sortCriterion & ReportSortCriterion.Private) != 0)
                {
                    results = results.Where(m => (m.Scoping == Report.Scope.Private));
                }
                else if ((sortCriterion & ReportSortCriterion.Final) != 0)
                {
                    results = results.Where(m => (m.LifeCycleStage == Report.LifeCycle.Complete));
                }
                else if ((sortCriterion & ReportSortCriterion.Draft) != 0)
                {
                    results = results.Where(m => (m.LifeCycleStage == Report.LifeCycle.Incomplete));
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindAllByUserId", ex.InnerException);
            }

            return results.ToList();
        }

        /// <summary>
        /// Return all reports by a specific user id
        /// </summary>
        public List<Report> FindAllByUserId(string userId, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            try
            {
                var results = from reports in Context.Reports where reports.ApplicationUserId == userId select reports;
                if (isPublicOnly)
                {
                    results = results.Where(r => r.Scoping == Report.Scope.Public && r.LifeCycleStage == Report.LifeCycle.Complete);
                }
                return FilterSort(results, sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindAllByUserId", ex.InnerException);
            }
        }

        /// <summary>
        /// Find all reports sorted to the given scheme
        /// </summary>
        /// <param name="sortCriterion"></param>
        /// <returns></returns>
        public List<Report> FindAllSorted(ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            try
            {
                var results = FindAll();
                if (isPublicOnly)
                {
                    results = results.Where(r => r.Scoping == Report.Scope.Public && r.LifeCycleStage == Report.LifeCycle.Complete);
                }
                return FilterSort(FindAll(), sortCriterion);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindAllSorted", ex.InnerException);
            }
        }

        public Report FindMostViewed()
        {
            Report result = null;

            try
            {
                var query = from report in Context.Reports
                            orderby report.NViewings descending
                            select report;
                result = query.FirstOrDefault();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindMostViewed", ex.InnerException);
            }

            return result;
        }

        /// <summary>
        /// Return a list of reports that match the string(s) encoded in searchTerms
        /// </summary>
        /// <param name="searchTerms"></param>
        /// <returns></returns>
        public List<Report> DoSearch(string searchTerms, ReportSortCriterion sortCriterion, bool isPublicOnly)
        {
            if (string.IsNullOrEmpty(searchTerms)) return new List<Report>();
            List<Report> results = null;

            try
            {
                // Seek the search terms in the text of each report
                string[] fields = searchTerms.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                var query = from report in Context.Reports
                            where 
                            fields.Any(val => report.Text.Contains(val)) || 
                            fields.Any(val => report.Title.Contains(val) ) ||
                            fields.Any(val => report.User.UserName.Contains(val)) ||
                            fields.Any(val => report.User.FirstName.Contains(val)) ||
                            fields.Any(val => report.User.LastName.Contains(val))
                            select report;

                if(isPublicOnly)
                {
                    query = query.Where(r => r.Scoping == Report.Scope.Public && r.LifeCycleStage == Report.LifeCycle.Complete);
                }
                results = query.ToList();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindMostViewed", ex.InnerException);
            }

            return results;
        }

        /// <summary>
        /// We keep data on the Report that we could derive from the Favourite relationship - this is for efficiency
        /// After a rating Favourite link has been added, a call to this updates the Report's running totals
        /// </summary>
        /// <param name="reportId"></param>
        /// <param name="rating"></param>
        public void RegisterNewRating(int reportId, int rating)
        {
            if (rating < 0) return;

            try
            {
                Report report = FindById(reportId);
                if (report != null)
                {
                    report.TotalRating += rating;
                    report.NRatings++;
                    Update(report);
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::RegisterNewRating", ex.InnerException);
            }
        }

        //TODO Is the sum of published and unpublished the total number? Are there limbo states e..g complete but only for friends?

        /// <summary>
        /// How many of my reports are published (i.e. complete AND public)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lifeCycleStage"></param>
        /// <returns></returns>
        public int GetNPublished(string userId)
        {
            int nPublished = 0;
            try
            {
                IQueryable<Report> query = from report in Context.Reports where report.Scoping == Report.Scope.Public && report.LifeCycleStage == Report.LifeCycle.Complete && report.ApplicationUserId == userId select report;
                nPublished = query.Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::GetNPublished", ex.InnerException);
            }
            return nPublished;
        }

        /// <summary>
        /// How many of my reports are unpublished (i.e. incomplete OR private)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="lifeCycleStage"></param>
        /// <returns></returns>
        public int GetNUnpublished(string userId)
        {
            int nUnpublished = 0;
            try
            {
                //TODO currently this is just the NOT of the is-published query. Is that real?
                IQueryable<Report> query = from report in Context.Reports 
                                           where (report.Scoping != Report.Scope.Public || report.LifeCycleStage != Report.LifeCycle.Complete) 
                                           && report.ApplicationUserId == userId select report;
                nUnpublished = query.Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::GetNUnpublished", ex.InnerException);
            }
            return nUnpublished;
        }

        /// <summary>
        /// For all the reports this user has, how many ratings are there in total?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CountRatingsForUser(string userId)
        {
            int nRatings = 0;

            try
            {
                var query = from report in Context.Reports where report.ApplicationUserId == userId select report;
                if (query.Count() > 0)
                {
                    nRatings = query.Sum(r => r.NRatings);
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::CountRatingsForUser", ex.InnerException);
            }

            return nRatings;
        }

        /// <summary>
        /// For all the reports this user has, how many viewings are there in total?
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int CountViewingsForUser(string userId)
        {
            int nViewings = 0;

            try
            {
                var query = from report in Context.Reports where report.ApplicationUserId == userId select report;
                if (query.Count() > 0)
                {
                    nViewings = query.Sum(r => r.NViewings);
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::CountRatingsForUser", ex.InnerException);
            }

            return nViewings;
        }

        /// <summary>
        /// Find reports which share a tag or tags with this one and if all else fails a category
        /// </summary>
        /// <param name="reportID"></param>
        /// <returns></returns>
        public List<Report> FindRelatedReports(int reportID, int nReports)
        {
            // This needs to be smart about tags

            try
            {
                //TODO I'm not enough of a database guy to see a neater way to do this
                // Get a list of the tag ID's this report is linked to
                var tagInner = from tagging in Context.Taggings where tagging.ReportID == reportID select tagging.TagId;

                // Get a list of all reports which are tagged to any of those ids; order it by descreasing views
                var tagOuter = from tagging in Context.Taggings where tagInner.Contains(tagging.TagId) && tagging.ReportID != reportID orderby tagging.Report.NViewings descending select tagging.Report;

                if(tagOuter.Count() >= nReports)
                {
                    // We're done - we found enough related by tag
                    return tagOuter.Take<Report>(nReports).ToList();
                }
                else
                {
                    List<Report> relatedReports = tagOuter.ToList();

                    // Let's add in linkage via category
                    var categoryInner = from rc in Context.ReportCategories where rc.ReportID == reportID select rc.CategoryID;
                    var categoryOuter = from rc in Context.ReportCategories where categoryInner.Contains(rc.CategoryID) && rc.ReportID != reportID orderby rc.Report.NViewings descending select rc.Report;
                    relatedReports.AddRange(categoryOuter.ToList().Take<Report>(nReports - relatedReports.Count));
                    return relatedReports;
                }
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::FindRelatedReports", ex.InnerException);
            }
        }

        public bool RemoveReport(int reportId)
        {
            // DO NOT DO THIS WITHOUT UNHOOKING EVERYTHING ELSE FIRST
            try
            {
                var query = from report in Context.Reports where report.ID == reportId select report;
                if (query.Count() > 0)
                {
                    Context.Reports.RemoveRange(query);
                    Context.SaveChanges();
                    return true;
                }
                else return false;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("ReportRepository::RemoveReport", ex.InnerException);
            }
        }
    }
}
