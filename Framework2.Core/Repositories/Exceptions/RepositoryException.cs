﻿using System;
using System.Runtime.Serialization;

namespace Framework2.Core.Repositories.Base
{
    /// <summary>
    /// A set of custom exceptions relating to the Perspectives repository classes
    /// </summary>
    [Serializable]
    public class RepositoryException : Exception
    {
        public RepositoryException()
            : base() { }

        public RepositoryException(string message)
            : base(message) { }

        public RepositoryException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public RepositoryException(string message, Exception innerException)
            : base(message, innerException) { }

        public RepositoryException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected RepositoryException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
