﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

using Framework2.Core.Context;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Repositories.Base
{
    /// <summary>
    /// The base for all model repositories
    /// </summary>
    public abstract class BaseRepository<T> : IDisposable where T : BaseModel
    {
        protected readonly FrameworkContext Context;

        /// <summary>
        /// Default constructor
        /// </summary>
        protected BaseRepository(IContextManager contextManager)
        {
            Context = contextManager.Context;
        }

        /// <summary>
        /// Retrieve a model via it's database id.
        /// </summary>
        public virtual T FindById(int id)
        {
            try
            {
                return Context.Set<T>().Find(id);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::Find", ex);
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::Find", ex);
            }
        }

        /// <summary>
        /// Fetch all models
        /// </summary>
        public virtual IQueryable<T> FindAll()
        {
            try
            {
                return Context.Set<T>();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::FindAll", ex);
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::FindAll", ex);
            }
        }

        /// <summary>
        /// Return a count of all rows for a given model
        /// </summary>
        public virtual int Count()
        {
            try
            {
                return Context.Set<T>().Count();
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::Count", ex);
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::Count", ex);
            }
        }

        /// <summary>
        /// Fetch all models ordered by the descending date
        /// </summary>
        public virtual IQueryable<T> FindAllOrderedByCreationDate()
        {
            try
            {
                return from models in Context.Set<T>()
                       orderby models.CreationDate descending 
                       select models;
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::FindAllOrderedByDate", ex);
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::FindAllOrderedByDate", ex);
            }
        }

        /// <summary>
        /// Updates a specific model in the database
        /// </summary>
        public virtual T Update(T model)
        {

            var entry = Context.Entry(model);

            try
            {
                entry.State = EntityState.Modified;
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::Update", ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::Update", ex);
            }

            return model;
        }

        /// <summary>
        /// Create a new model
        /// </summary>
        public T Create()
        {
            T model;

            try
            {
                model = Context.Set<T>().Create();
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::Create", ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::Create", ex);
            }

            return model;
        }

        /// <summary>
        /// Add a new model
        /// </summary>
        public T Add(T model)
        {
            try
            {
                Context.Set<T>().Add(model);
                Context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                throw new RepositoryException("Repository::Add", ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new RepositoryException("Repository::Add", ex);
            }

            return model;
        }

        /// <summary>
        /// Ensure we dispose of the current context at the end of every request
        /// </summary>
        public void Dispose()
        {
            Context.Dispose();
        }
    }
}
