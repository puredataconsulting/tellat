﻿using System.Linq;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Repositories.Base
{
    /// <summary>
    /// Provides the core database functionality
    /// </summary>
    public interface IBaseRepository<T> where T : BaseModel
    {

        /// <summary>
        /// Retrieve a model via it's id
        /// </summary>
        T FindById(int id);

        /// <summary>
        /// Fetch all models
        /// </summary>
        IQueryable<T> FindAll();

        /// <summary>
        /// Return a count of all rows for a given model
        /// </summary>
        int Count();

        /// <summary>
        /// Fetch all models ordered by date
        /// </summary>
        IQueryable<T> FindAllOrderedByCreationDate();

        /// <summary>
        /// Updates a specific model in the database
        /// </summary>
        T Update(T model);

        /// <summary>
        /// Create a new database record
        /// </summary>
        T Create();

        /// <summary>
        /// Add a database record
        /// </summary>
        T Add(T model);
    }
}
