﻿using System;
using System.Runtime.Serialization;

namespace Framework2.Core.Services.Base
{
    /// <summary>
    /// A set of custom exceptions relating to the Perspectives service classes
    /// </summary>
    [Serializable]
    public class ServiceException : Exception
    {
        public ServiceException()
            : base() { }

        public ServiceException(string message)
            : base(message) { }

        public ServiceException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public ServiceException(string message, Exception innerException)
            : base(message, innerException) { }

        public ServiceException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected ServiceException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
