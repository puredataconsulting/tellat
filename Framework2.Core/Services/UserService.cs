﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Framework2.Utils;
using Microsoft.Practices.Unity;
using System.Web.Security;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides ApplicationUser related services.  Registration, login, etc..
    /// </summary>
    public class UserService : BaseService, IUserService
    {
        //[Dependency]
        //public IApplicationUserRepository UserRepository { get; set; }

        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        /// <summary>
        /// Register a new user
        /// </summary>
        public ApplicationUser Register(string username, string emailAddress, string password,
                             string twitterId = "", string facebookId = "")
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(password))
            {
                throw new ServiceException("One or more of the provided arguments is invalid");
            }

            //// Does the ApplicationUser already exist?
            //var existingUser = UserRepository.FindByEmailAddress(emailAddress);
            //if (existingUser != null)
            //{
            //    return null;
            //}

            // Generate the salt for the password
            var salt = Cryptography.GenerateSalt();

            // Create a new user
            ApplicationUser user = null;
            //try
            //{
            //    user = UserRepository.Create();
            //}
            //catch (RepositoryException ex)
            //{
            //    throw new ServiceException("Unable to create the new user in the database", ex);
            //}

            //// Populate the user record
            ////user.EmailAddress = emailAddress;
            //user.Username = username;
            ////user.PasswordSalt = salt;
            ////user.PasswordHash = Cryptography.HashPassword(password, salt);
            //user.SessionToken = Cryptography.GenerateSessionToken();
            //user.LastActivity = DateTime.Now;
            

            //// Associate the Facebook id
            //if (!string.IsNullOrEmpty(facebookId))
            //{
            //    //user.FacebookId = facebookId;
            //}

            //// Associate the Twitter id
            //if (!string.IsNullOrEmpty(twitterId))
            //{
            //   // user.TwitterId = twitterId;
            //}

            //// Just a normal user
            ////user.IsAdministrator = false;

            ////// Does this site require account approval?
            ////user.IsApproved = !WebsiteConfig.RegistrationRequiresApproval;

            ////// Does this site require email verification?
            ////user.IsVerified = !WebsiteConfig.RegistrationRequiresVerification;

            //// Add to the database
            //try
            //{
            //    UserRepository.Add(user);
            //}
            //catch (RepositoryException ex)
            //{
            //    throw new ServiceException("Unable to add the new user in the database", ex);
            //}

            //// Add a media item to hold the user's thumbnail (these will have to be off in Azure!)

            //MediaItem mediaItem = MediaItemRepository.Create();
            //mediaItem.MediaType = MediaItem.MediaTypes.Content;
            //mediaItem.ThumbnailUri = user.GenderType == GenderTypes.Male ? "profile-blue.png" : "profile-pink.png";
            //mediaItem.Uri = "generic-person.png";

            //mediaItem.ApplicationUser = user;
            //// Add to the database
            //try
            //{
            //    MediaItemRepository.Add(mediaItem);
            //}
            //catch (RepositoryException ex)
            //{
            //    throw new ServiceException("Unable to add the new media item in the database", ex);
            //}

            return user;
        }

        /// <summary>
        /// Verify a user, usually as a result of an email response
        /// </summary>
        public ApplicationUser Verify(string emailAddress, string passwordHash)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(passwordHash))
            {
                throw new ServiceException("One or more of the arguments are invalid");
            }

            ApplicationUser userModel = null;
            // Fetch the ApplicationUser via the supplied hash
            // userModel = UserRepository.FindByPasswordHash(passwordHash);
            //if (userModel.EmailAddress != emailAddress)
            //{
            //    return null;
            //}

            //// Verify the user
            //userModel.IsVerified = true;

            //// Does our site require users to be approved by an administrator?
            //userModel.IsApproved = WebsiteConfig.RegistrationRequiresApproval;

            // Update our user
            //UserRepository.Update(userModel);

            return userModel;
        }

        /// <summary>
        /// Reset a users password and generate a new temporary one
        /// </summary>
        /// <returns></returns>
        public ApplicationUser ResetPassword(string emailAddress)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(emailAddress))
            {
                throw new ServiceException("One or more of the arguments are invalid");
            }



            ApplicationUser userModel = null;

            //// Find the ApplicationUser
            //userModel = UserRepository.FindByEmailAddress(emailAddress);

            //// Does the ApplicationUser exist
            //if (userModel == null)
            //{
            //    return null;
            //}

            //// Generate a new temporary password
            //var password = Cryptography.GenerateAplhaNumericString(8);

            //// Generate a salt and hash the password
            //var salt = Cryptography.GenerateSalt();
            //var passwordHash = Cryptography.HashPassword(password, salt);

            ////// Update the ApplicationUser with the new password
            ////userModel.PasswordSalt = salt;
            ////userModel.PasswordHash = passwordHash;

            //// Update the ApplicationUser
            //UserRepository.Update(userModel);

            return userModel;
        }

        /// <summary>
        /// Logout and clear the session token
        /// </summary>
        public bool Logout(string sessionToken)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(sessionToken))
            {
                throw new ServiceException("One or more of the arguments are invalid");
            }

            //// Find the ApplicationUser
            //var userModel = UserRepository.FindBySessionToken(sessionToken);

            //// Does the ApplicationUser exist
            //if (userModel == null)
            //{
            //    return false;
            //}

            //// Delete the current ApplicationUser session token from the database
            //userModel.SessionToken = string.Empty;

            //// Persist to the database
            //UserRepository.Update(userModel);

            return true;
        }

        #region Email

        /// <summary>
        /// Login a user via an email and password pair
        /// </summary>
        public ApplicationUser LoginByEmailAddress(string emailAddress, string password, bool? keepLoggedIn)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(password))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Attempt to login
            ApplicationUser user = null;
            //try
            //{
            //    user = UserRepository.FindByEmailAddress(emailAddress);
            //}
            //catch (Exception ex)
            //{
            //    throw new ServiceException("An internal server error has occured whilst retrieving the user from the database", ex.InnerException);
            //}

            //// Does the ApplicationUser exist
            //if (user == null)
            //{
            //    return null;
            //}

            ////// Hash and check the provided password
            ////if (user.PasswordHash != Cryptography.HashPassword(password, user.PasswordSalt))
            ////{
            ////    return null;
            ////}

            ////// Check weve validated the email address via the registration email
            ////if (user.IsVerified == false)
            ////{
            ////    return null;
            ////}

            ////// Does our site require users to be approved by an administrator?
            ////if (WebsiteConfig.RegistrationRequiresApproval && user.IsApproved == false)
            ////{
            ////    return null;
            ////}

            //// Generate a new session token and persist it to the database
            //user.SessionToken = Cryptography.GenerateSessionToken();
            //UserRepository.Update(user);

            //if(keepLoggedIn.HasValue)
            //{
            //    if(keepLoggedIn.Value)
            //    {
            //        FormsAuthentication.SetAuthCookie(emailAddress, true);
            //    }
            //}

            // Success
            return user;
        }

        #endregion

        #region Facebook

        /// <summary>
        /// Login a user via a Facebook ID
        /// </summary>
        public ApplicationUser LoginByFacebookId(string facebookId)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(facebookId))
            {
                throw new ServiceException("One or more of the arguments are invalid");
            }

            ApplicationUser userModel = null;

            //// Attempt to login
            //var userModel = UserRepository.FindByFacebookId(facebookId);

            //// Does the ApplicationUser exist
            //if (userModel == null)
            //{
            //    return null;
            //}

            ////// Does our site require users to be approved by an administrator?
            ////if (WebsiteConfig.RegistrationRequiresApproval && userModel.IsApproved == false)
            ////{
            ////    return null;
            ////}

            //// Generate a new session token and persist it to the database
            //userModel.SessionToken = Cryptography.GenerateSessionToken();
            //UserRepository.Update(userModel);

            // Success
            return userModel;
        }

        #endregion

        #region Twitter

        /// <summary>
        /// Login a user via a Twitter ID
        /// </summary>
        public ApplicationUser LoginByTwitterId(string twitterId)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(twitterId))
            {
                throw new ServiceException("One or more of the arguments are invalid");
            }

            ApplicationUser userModel = null;

            //// Attempt to login
            // userModel = UserRepository.FindByTwitterId(twitterId);

            //// Does the ApplicationUser exist
            //if (userModel == null)
            //{
            //    return null;
            //}

            ////// Does our site require users to be approved by an administrator?
            ////if (WebsiteConfig.RegistrationRequiresApproval && userModel.IsApproved == false)
            ////{
            ////    return null;
            ////}

            //// Generate a new session token and persist it to the database
            //userModel.SessionToken = Cryptography.GenerateSessionToken();
            //UserRepository.Update(userModel);

            // Success
            return userModel;
        }

        #endregion

    }
}
