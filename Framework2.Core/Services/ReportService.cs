﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides report related services
    /// </summary>
    public class ReportService : BaseService, IReportService
    {
        [Dependency]
        private IReportRepository ReportRepository { get; set; }

        [Dependency]
        private IMediaItemRepository MediaItemRepository { get; set; }

        [Dependency]
        private IReportCategoryRepository ReportCategoryRepository { get; set; }

        public ReportService()
        {

        }

        /// <summary>
        /// Create a new report record and add it to the database
        /// </summary>
        public Report Create(string userId, int categoryId, string filename, 
                             string title, string text)
        {
            // Validate the arugments
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(title) || 
                               string.IsNullOrEmpty(filename) || string.IsNullOrEmpty(text))
            {
                throw new ServiceException("One or more of the provided arguments is null or invalid");
            }

            // Create the record
            Report report;
            try
            {
                report = ReportRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating the report in the database", ex.InnerException);
            }

            // Populate the new record
            //report.Uri = filename;
            report.Title = title;
            report.Text = text;
            

            // Add the foreign keys
            report.ApplicationUserId = userId;

            // Add it to the database
            try
            {
                // Link up an initial media item; one day we'll allow for the addition of new ones too
                MediaItem mediaItem = MediaItemRepository.Create();
                mediaItem.Uri = filename;
                MediaItemRepository.Add(mediaItem);

                report.MediaItems.Add(mediaItem);

                ReportRepository.Add(report);


                // We need to wire it to its category too
                //TODO Soon this will be categorIES
                ReportCategory reportCategory = ReportCategoryRepository.Create();
                reportCategory.ReportID = report.ID;
                reportCategory.CategoryID = categoryId;
                ReportCategoryRepository.Add(reportCategory);
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the report to the database", ex.InnerException); 
            }

            return report;
        }
    }
}
