﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides Tagging related services
    /// </summary>
    public class TaggingService : BaseService, ITaggingService
    {
        [Dependency]
        public ITaggingRepository TaggingRepository { get; set; }

        /// <summary>
        /// Register a new Tagging and adds it to the database
        /// </summary>
        public Tagging Create(Report report, Tag tag)
        {
            // Validate the arguments
            if (report == null || tag == null)
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Tagging tagging;
            try
            {
                tagging = TaggingRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating a Tagging in the database", ex.InnerException);
            }

            // Populate the record
            tagging.Report = report;
            tagging.Tag = tag;

            // Add to the database
            try
            {
                TaggingRepository.Add(tagging);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the Tagging to the database", ex.InnerException);
            }

            return tagging;
        }
    }
}
