﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides member related services
    /// </summary>
    public class GroupMemberService : BaseService, IGroupMemberService
    {
        [Dependency]
        private IGroupMemberRepository GroupMemberRepository { get; set; }
        public GroupMember Create(ApplicationUser user, Group group, bool isAdministrator)
        {
            // Create the record
            GroupMember groupMember = null;
            try
            {
                groupMember = GroupMemberRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating the group member in the database", ex.InnerException);
            }

            groupMember.ApplicationUser = user;
            groupMember.Group = group;
            groupMember.IsAdministrator = isAdministrator;

            return groupMember;
        }

    }
}
