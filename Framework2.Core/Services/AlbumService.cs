﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides album related services
    /// </summary>
    public class AlbumService : BaseService, IAlbumService
    {
        [Dependency]
        public IAlbumRepository AlbumRepository { get; set; }

        /// <summary>
        /// Create a new group and add it to the database
        /// </summary>
        public Album Create(ApplicationUser user, string title, string description)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(title))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            var album = AlbumRepository.Create();

            // Populate the record
            album.Title = title;
            album.Description = description;
            album.ApplicationUser = user;

            // Add the record to the database
            try
            {
                AlbumRepository.Add(album);
            }
            catch (RepositoryException ex)
            {
                LogRepository.LogException(this, ex);
                throw new ServiceException("An internal server error has occured whilst adding the album to the database", ex.InnerException); 
            }

            return album;
        }
    }
}
