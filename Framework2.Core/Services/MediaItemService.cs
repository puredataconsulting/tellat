﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides MediaItem related services
    /// </summary>
    public class MediaItemService : BaseService, IMediaItemService
    {
        [Dependency]
        public IMediaItemRepository MediaItemRepository { get; set; }

        /// <summary>
        /// Register a new MediaItem and add it to the database
        /// </summary>
        public MediaItem Create(Report report, string filename, string title)
        {
            // Validate the arguments
            if (report == null || string.IsNullOrEmpty(filename))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            MediaItem MediaItem;
            try
            {
                MediaItem = MediaItemRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating a Media Item in the database", ex.InnerException);
            }

            // Populate the record
            
            MediaItem.Report = report;

            // Add to the database
            try
            {
                MediaItemRepository.Add(MediaItem);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the Media Item to the database", ex.InnerException);
            }

            return MediaItem;
        }
    }
}
