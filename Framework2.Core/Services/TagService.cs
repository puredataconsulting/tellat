﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides Tag related services
    /// </summary>
    public class TagService : BaseService, ITagService
    {
        [Dependency]
        public ITagRepository TagRepository { get; set; }

        /// <summary>
        /// Register a new Tag and adds it to the database
        /// </summary>
        public Tag Create(string text)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(text))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Tag tag;
            try
            {
                tag = TagRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating a Tag in the database", ex.InnerException);
            }

            // Populate the record
            tag.Text = text;

            // Add to the database
            try
            {
                TagRepository.Add(tag);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the Tag to the database", ex.InnerException);
            }

            return tag;
        }
    }
}
