﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides Favourite related services
    /// </summary>
    public class FavouriteService : BaseService, IFavouriteService
    {
        [Dependency]
        public IFavouriteRepository FavouriteRepository { get; set; }

        /// <summary>
        /// Register a new Favourite and adds it to the database
        /// </summary>
        public Favourite Create(string userId, int reportId, int rating)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(userId) || reportId == 0 || rating < 0 || rating > 10)
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Favourite Favourite;
            try
            {
                Favourite = FavouriteRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating a favourite in the database", ex.InnerException);
            }

            // Populate the record
            Favourite.ApplicationUserId = userId;
            Favourite.ReportId = reportId;
            Favourite.Rating = rating;

            // Add to the database
            try
            {
                FavouriteRepository.Add(Favourite);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the favourite to the database", ex.InnerException);
            }

            return Favourite;
        }
    }
}
