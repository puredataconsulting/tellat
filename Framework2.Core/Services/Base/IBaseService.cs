﻿using Framework2.Core.Models.Base;

namespace Framework2.Core.Services.Base
{
    /// <summary>
    /// The base for all services, also encapsulates the core logging code
    /// </summary>
    public interface IBaseService
    {
      
    }
}
