﻿using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services.Base
{
    /// <summary>
    /// The base for all services, also encapsulates the core logging code
    /// </summary>
    public abstract class BaseService : IBaseService
    {
        [Dependency]
        public ILogRepository LogRepository { get; set; }
    }
}
