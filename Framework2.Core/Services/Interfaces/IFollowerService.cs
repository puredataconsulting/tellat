﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Services.Interfaces
{
    public interface IFollowerService : IBaseService
    {
        /// <summary>
        /// Register a new comment and adds it to the database
        /// </summary>
        Following Create(ApplicationUser followingUser, ApplicationUser followedUser);
    }
}
