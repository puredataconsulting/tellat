﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to media items
    /// </summary>
    public interface IMediaItemService : IBaseService
    {
        /// <summary>
        /// Create a new media item record and add it to the database
        /// </summary>
        MediaItem Create(Report report, string filename, string title);
    }
}
