﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to comments
    /// </summary>
    public interface ICommentService : IBaseService
    {
        /// <summary>
        /// Register a new comment and adds it to the database
        /// </summary>
        Comment Create(string userId, int reportId, string text);
    }
}
