﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to groups
    /// </summary>
    public interface IGroupService : IBaseService
    {
        /// <summary>
        /// Create a new group and add it to the database
        /// </summary>
        Group Create(ApplicationUser creator, string title, string description);

    }
}
