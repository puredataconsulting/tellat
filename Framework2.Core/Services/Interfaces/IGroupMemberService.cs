﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to members
    /// </summary>
    public interface IGroupMemberService : IBaseService
    {
        GroupMember Create(ApplicationUser user, Group group, bool isAdministrator);
    }
}
