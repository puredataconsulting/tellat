﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to albums
    /// </summary>
    public interface IAlbumService : IBaseService
    {
        /// <summary>
        /// Create a new album and add it to the database
        /// </summary>
        Album Create(ApplicationUser user, string title, string description);
    }
}
