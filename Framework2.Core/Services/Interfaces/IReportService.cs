﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services related to videos
    /// </summary>
    public interface IReportService : IBaseService
    {
        /// <summary>
        /// Create a new report record and add it to the database
        /// </summary>
        Report Create(string userId, int categoryId, string filename, string title, string text);
    }
}
