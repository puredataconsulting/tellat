﻿using Framework2.Core.Models.Core;
using Framework2.Core.Services.Base;

namespace Framework2.Core.Services.Interfaces
{
    /// <summary>
    /// Provides services ralated to users, registration, login, etc..
    /// </summary>
    public interface IUserService : IBaseService
    {
        /// <summary>
        /// Register a new user
        /// </summary>
        ApplicationUser Register(string username, string emailAddress, string password, string twitterId = "", string facebookId = "");

        /// <summary>
        /// Login via an email address and password
        /// </summary>
        ApplicationUser LoginByEmailAddress(string emailAddress, string password, bool? keepLoggedIn);

        /// <summary>
        /// Reset a users password and generate a temporary one
        /// </summary>
        /// <returns></returns>
        ApplicationUser ResetPassword(string emailAddress);

        /// <summary>
        /// Verify a user usually via a registration email
        /// </summary>
        ApplicationUser Verify(string emailAddress, string passwordHash);

        /// <summary>
        /// Login via a Facebook ID
        /// </summary>
        ApplicationUser LoginByFacebookId(string facebookId);

        /// <summary>
        /// Login via a Twitter ID
        /// </summary>
        ApplicationUser LoginByTwitterId(string twitterId);

        /// <summary>
        /// Logout and clear the session token
        /// </summary>
        bool Logout(string sessionToken);
    }
}
