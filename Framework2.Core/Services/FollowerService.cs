﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides Follower related services
    /// </summary>
    public class FollowerService : BaseService, IFollowerService
    {
        [Dependency]
        public IFollowerRepository FollowerRepository { get; set; }

        /// <summary>
        /// Register a new Follower and adds it to the database
        /// </summary>
        public Following Create(ApplicationUser followingUser, ApplicationUser followedUser)
        {
            // Validate the arguments
            if (followingUser == null || followedUser == null)
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Following follower;
            try
            {
                follower = FollowerRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating a Follower in the database", ex.InnerException);
            }

            // Populate the record
            follower.ApplicationUser = followingUser;
            follower.Target = followedUser;

            // Add to the database
            try
            {
                FollowerRepository.Add(follower);
            }
            catch (Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the Follower to the database", ex.InnerException);
            }

            return follower;
        }
    }
}
