﻿using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;
using System.Linq;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides group related services
    /// </summary>
    public class GroupService : BaseService, IGroupService
    {
        [Dependency]
        private IGroupRepository GroupRepository { get; set; }

        [Dependency]
        private IGroupMemberRepository GroupMemberRepository { get; set; }

        /// <summary>
        /// Create a new group and add it to the database
        /// </summary>
        public Group Create(ApplicationUser creator, string title, string description)
        {
            // Validate the arguments
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(description))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Group group;
            try
            {
                group = GroupRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating the group in the database", ex.InnerException);
            }

            // Populate the record
            group.Title = title;
            group.Description = description;

            GroupMember groupMember = GroupMemberRepository.Create();
            groupMember.ApplicationUser = creator;
            groupMember.Group = group;
            groupMember.IsAdministrator = true;

            // Add the records to the database
            try
            {
                GroupRepository.Add(group);
                GroupMemberRepository.Add(groupMember);
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the user to the database", ex.InnerException);
            }

            return group;
        }

    }
}
