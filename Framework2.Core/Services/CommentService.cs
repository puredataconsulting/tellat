﻿using System;
using Framework2.Core.Models.Core;
using Framework2.Core.Repositories.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using Framework2.Core.Services.Base;
using Framework2.Core.Services.Interfaces;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Services
{
    /// <summary>
    /// Provides comment related services
    /// </summary>
    public class CommentService : BaseService, ICommentService
    {
        [Dependency]
        public ICommentRepository CommentRepository { get; set; }

        /// <summary>
        /// Register a new comment and adds it to the database
        /// </summary>
        public Comment Create(string userId, int reportId, string text)
        {
            // Validate the arguments
            if (reportId == 0 || string.IsNullOrEmpty(text))
            {
                throw new ServiceException("One or more of the provided arguments is empty or invalid");
            }

            // Create the record
            Comment comment;
            try
            {
                comment = CommentRepository.Create();
            }
            catch (RepositoryException ex)
            {
                throw new ServiceException("An internal server error has occured whilst creating the comment in the database", ex.InnerException);   
            }

            // Populate the record
            comment.ApplicationUserId = userId;
            comment.ReportId = reportId;
            comment.Text = Utils.InputCleaning.StripHtmlRegex(text);

            // Add to the database
            try
            {
                CommentRepository.Add(comment);
            }
            catch(Exception ex)
            {
                throw new ServiceException("An internal server error has occured whilst adding the comment to the database", ex.InnerException); 
            }

            return comment; 
        }
    }
}
