﻿using Framework2.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    // A report can have many tags and a tag can be linked from many reports
    // This is the associative table to resolve the M:M between Report and Tag
    public class Tagging : BaseModel
    {
        #region Foreign Keys

        public int ReportID { get; set; }
        public virtual Report Report { get; set; }

        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }
        #endregion

        #region Members
        #endregion
    }
}
