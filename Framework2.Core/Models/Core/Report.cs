﻿using System.ComponentModel.DataAnnotations;
using Framework2.Core.Models.Base;
using System.Collections.Generic;
using System;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The video model
    /// </summary>
    public class Report : ModelActive
    {
        public enum LifeCycle
        {
            Incomplete = 1,
            Complete = 2
        };

        public enum Scope
        {
            Private = 1,
            Limited =  2,
            Public = 32
        };


        [Required(ErrorMessage = @"Enter a title")]
        public string Title { get; set; }

        [Required(ErrorMessage = @"Enter some report text")]
        public string Text { get; set; }

        public bool IsFeatured { get; set; }

        // We're going to need the means to mark stuff for removal
        public bool IsReportedAbusive { get; set; }

        public int NViewings { get; set; }

        // We're keeping summation data on the report so we don't need to run the whole rating table to work
        // out the average rating
        public int NRatings { get; set; }
        public long TotalRating { get; set; }

        // Allow for unfinished reports
        public LifeCycle LifeCycleStage { get; set; }

        // How visible is this report
        public Scope Scoping { get; set; }

        // Geo location
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        // Reports:Categories is a M:M
        public virtual List<ReportCategory> ReportCategories { get; set; }

        // Reports:Groups is a M:M
        public virtual List<GroupMember> GroupMembers { get; set; }

        public virtual List<Favourite> Favourites { get; set; }

        // NB One report, many media items (multiple videos, images etc)
        public virtual List<MediaItem> MediaItems { get; set; }

        public virtual List<Tagging> Tags { get; set; }
        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Report()
        {
            IsReportedAbusive = false;
            LifeCycleStage = Report.LifeCycle.Complete;
            Scoping = Report.Scope.Public;
            NViewings = 0;
            NRatings = 0;
            TotalRating = 0;
        }

        #endregion
    }
}