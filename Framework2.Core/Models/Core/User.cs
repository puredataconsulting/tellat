﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework2.Core.Models.Base;
using Framework2.Core.Repositories.Core.Interfaces;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The user model
    /// </summary>
    public class User : ModelActive
    {


        //public bool IsAdministrator { get; set; }

        //[DataType(DataType.EmailAddress)]
        //public string EmailAddress { get; set; }

        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ProfileText { get; set; }

        public DateTime Birthday { get; set; }

        public GenderTypes GenderType { get; set; }

        public UserPrivacySettings PrivacyOptions { get; set; }
        
        //public string FacebookId { get; set; }

        //public string TwitterId { get; set; }

        //public bool IsVerified { get; set; }

        //public bool IsApproved { get; set; }

        //public string PasswordHash { get; set; }

        //public string PasswordSalt { get; set; }

        public string SessionToken { get; set; }

        #region Foreign Keys

        // Country
        public int? CountryId { get; set; }

        public virtual Country Country { get; set; }

        // User's Thumbnail
        public virtual List<MediaItem> MediaItems { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public virtual List<GroupMember> GroupMembers { get; set; }

        public virtual List<Favourite> Favourites { get; set; }

        public virtual List<Report> Reports { get; set; }

        public virtual List<Follower> Following { get; set; }

        public virtual List<Follower> FollowedBy { get; set; }


        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public User()
        {
            GenderType = GenderTypes.Male;
            Birthday = new DateTime(1970, 8, 29);
            PrivacyOptions = 
               UserPrivacySettings.DateOfBirth | 
               UserPrivacySettings.ExactLocation | 
               UserPrivacySettings.Gender;
        }

        #endregion
    }
}