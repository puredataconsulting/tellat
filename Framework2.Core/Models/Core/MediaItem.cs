﻿using Framework2.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    public class MediaItem : BaseModel
    {
        [Flags]
        public enum MediaTypes
        {
            Video = 0,
            Image = 1,
            Audio = 2,
            Text = 4,
            Content = 8,
            H264 = 32,
            // Video which has been copied from Azure storage into WAMS behaves differently from stock uploaded video
            MediaServicesTransferred = 256,
            // Video which has been encoded as H.264 by WAMS again may need a different treatment
            MediaServicesEncoded = 512,
            EncoderLowBitrate = 4096,
            EncoderMediumBitrate =8192,
            EncoderHighBitrate = 16384,
        };

        public enum MediaStatusTypes
        {
            // We may want to mark some media as being generally inaccessible except to the owner
            Public,
            Private,
            // We may also want to mark some media (i.e. standard icons) as being for internal use
            System

        };
        public string Title { get; set; }
        public string Description { get; set; }

        public string Uri { get; set; }

        public string ThumbnailUri { get; set; }

        public MediaTypes MediaType { get; set; }

        public MediaStatusTypes MediaStatusType { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        /// <summary>
        /// We create numbers of assets along the way in Azure - this is to keep track of the ones we can't delete
        /// until we're ready to lose everything associated
        /// </summary>
        public string AssociatedAssets { get; set; }

        #region Foreign Keys

        // In general media items are linked to reports
        public int? ReportID { get; set; }

        virtual public Report Report { get; set; }

        // We also use media item to hold thumbnails
        public string ApplicationUserId { get; set; }

        virtual public ApplicationUser ApplicationUser { get; set; }

        // Media can be in Albums
        public int? AlbumID { get; set; }
        virtual public Album Album { get; set; }

        #endregion

        #region Members
        public MediaItem()
        {
            Description = string.Empty;
            Uri = string.Empty;
            ThumbnailUri = string.Empty;
            MediaType = MediaTypes.Video;
            MediaStatusType = MediaStatusTypes.Public;
            Report = null;
            ApplicationUser = null;
            AssociatedAssets = string.Empty;
        }
        #endregion

         
    }
}