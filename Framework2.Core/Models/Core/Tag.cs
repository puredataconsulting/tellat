﻿using Framework2.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{

    public class Tag : BaseModel
    {
        public string Text { get; set; }

        // Every time we register a hit on a tag, we update NHitsInCurrentInterval IF that hit falls within the current interval
        // If it falls after the current interval, we copy the hit count from the current interval into the hit count for the previous interval
        // and set NHitsInCurrentInterval to 1
        // This allows us to cross interval boundaries with no loss of continuity
        // In more detail: if our interval is one day, and we're registering a hit on a tag, and that hit is today, we add one to NHitsInCurrentInterval
        // When we calculate the hit rate it's (NHitsInPreviousInterval + NHitsInCurrentInterval) / (2 * intervalSeconds)
        // If the hit is later than LastIntervalBoundary + one day, we've started a new interval
        // We need to avoid the situation where a hit after midnight looks like one hit in the last 24 hours
        // hence we keep track of the hits in the previous interval and serve up an average hit rate based on the two intervals
        // When we start a new interval, we copy NHitsInCurrentInterval onto NHitsInPreviousInterval and continue thus with some "memory"
        // of what happened in the last 24 hours.
        // If a whole interval goes by with no hits and then we get one, NHitsInPreviousInterval should be set 0
        // LastIntervalBoundary might be midnight this morning, so the last interval corresponds to yesterday and the current interval to today

        public int NHitsInPreviousInterval { get; set; }


        public int NHitsInCurrentInterval { get; set; }

        public DateTime LastIntervalBoundary { get; set; }

        // Hits per HOUR
        public float HitRate { get; set; }

        #region Foreign Keys

        public virtual List<Tagging> Reports { get; set; }
        #endregion

        #region Members
        #endregion

        #region Non-Mapped Entries
        static public TimeSpan Interval = new TimeSpan(24, 0, 0);
        #endregion
    }
}
