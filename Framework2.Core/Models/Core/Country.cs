﻿using System;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The Country model
    /// </summary>
    [Serializable]
    public class Country : BaseModel
    {
        public string IsoValue { get; set; }

        public string Name { get; set; }

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Country()
        {
        }

        #endregion
    }
}