﻿using System.Collections.Generic;
using Framework2.Core.Models.Base;
using System;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The Group model, used to store User groups in the database
    /// </summary>
    public class Group : ModelActive
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string ThumbnailUri { get; set; }

        public string Uri { get; set; }


        #region Foreign Keys

        virtual public List<GroupMember> GroupMembers { get; set; }

        virtual public List<GroupReport> GroupReports { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Default constructor
        /// </summary>
        public Group()
        {
            Title = string.Empty;
            Description = string.Empty;
            ThumbnailUri = string.Empty;
            Uri = string.Empty;
        }

        #endregion
    }
}