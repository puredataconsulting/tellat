﻿using Framework2.Core.Models.Base;
using Framework2.Core.Repositories.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    public class Notification : BaseModel
    {
        public NotificationTypes NotificationType { get; set; }

        // This is the ID of the sender but we don't need to track the relationship
        public string Sender { get; set; }

        // This is "additional data" - in the case of a joining request for a group it's the ID
        // of the group Sender wants to join
        public string Reference { get; set; }

        [NotMapped]
        public string SenderName { get; set; }

        [NotMapped]
        public string GroupName { get; set; }

         #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Notification()
        {
        }

        #endregion
    }
}
