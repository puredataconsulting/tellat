﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The favourite model
    /// </summary>
    public class Favourite : BaseModel
    {
        public int Rating { get; set; }

        public bool IsFavourite { get; set; }

        #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int ReportId { get; set; }
        public virtual Report Report { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Favourite()
        {
            Rating = -1;
            IsFavourite = false;
        }

        #endregion
    }
}