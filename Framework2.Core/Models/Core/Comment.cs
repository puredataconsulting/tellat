﻿using System.ComponentModel.DataAnnotations;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The comment class
    /// </summary>
    public class Comment : BaseModel
    {
        [Required]
        public string Text { get; set; }

        #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int ReportId { get; set; }
        public virtual Report Report { get; set; }
        
        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Comment()
        {
        }

        #endregion
    }
}