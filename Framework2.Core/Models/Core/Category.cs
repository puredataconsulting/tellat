﻿using Framework2.Core.Models.Base;
using System.Collections.Generic;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The category model, used to store categorys in the database
    /// </summary>
    public class Category : BaseModel
    {
        public string ThumbnailUri { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        #region Foreign Keys

        public virtual List<ReportCategory> ReportCategories { get; set; }
        #endregion


        #region Members

        /// <summary>
        /// Default constructor
        /// </summary>
        public Category()
        {
        }

        #endregion
    }
}