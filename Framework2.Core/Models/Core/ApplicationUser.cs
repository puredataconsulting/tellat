﻿using Framework2.Core.Repositories.Core.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime CreationDate { get; set; }
        public DateTime LastActivity { get; set; }

        public string ProfileText { get; set; }

        public DateTime Birthday { get; set; }

        public GenderTypes GenderType { get; set; }

        public UserPrivacySettings PrivacyOptions { get; set; }
                
        public string SessionToken { get; set; }

        #region Foreign Keys

        // Country
        public int? CountryId { get; set; }

        public virtual Country Country { get; set; }

        public virtual List<MediaItem> MediaItems { get; set; }

        public virtual List<Comment> Comments { get; set; }

        public virtual List<GroupMember> GroupMembers { get; set; }

        public virtual List<Favourite> Favourites { get; set; }

        public virtual List<Following> Followings { get; set; }

        public virtual List<Report> Reports { get; set; }

        public virtual List<Album> Albums { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public ApplicationUser()
        {
            GenderType = GenderTypes.Male;
            Birthday = new DateTime(1970, 8, 29);
            CreationDate = DateTime.Now;
            LastActivity = DateTime.Now;
            PrivacyOptions = UserPrivacySettings.DateOfBirth | UserPrivacySettings.ExactLocation | UserPrivacySettings.Gender;
        }

        #endregion
    }
}