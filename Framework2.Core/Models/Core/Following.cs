﻿using Framework2.Core.Models.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The Follower model, used to store followers of users
    /// </summary>
    public class Following : BaseModel
    {
        #region Foreign Keys

        // Follower
        public string ApplicationUserId { get; set; }

        // EF gets confused here unless we give it a hand using the attributes
        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        // Followed

        public string TargetId { get; set; }

        [ForeignKey("TargetId")]
        public virtual ApplicationUser Target { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Default constructor
        /// </summary>
        public Following()
        {
        }

        #endregion
    }
}