﻿using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The GroupMember model, used to track User membership to groups
    /// </summary>
    public class GroupMember : BaseModel
    {
        public bool IsAdministrator { get; set; }

        #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public int GroupId { get; set; }
        public virtual Group Group { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public GroupMember()
        {
        }

        #endregion
    }
}