﻿using Framework2.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    // Associative table to resolve the M:M between groups and reports
    public class GroupReport : BaseModel
    {
        #region Foreign Keys

        public int ReportId { get; set; }
        public virtual Report Report { get; set; }

        public int GroupId { get; set; }
        public virtual Group Group { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public GroupReport()
        {
        }

        #endregion
    }
}
