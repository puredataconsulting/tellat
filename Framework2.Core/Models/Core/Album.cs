﻿using System.Collections.Generic;
using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The album model, used to store sets of images in the database
    /// </summary>
    public class Album : BaseModel
    {
        public string Title { get; set; }

        public string Description { get; set; }

        #region Foreign Keys

        public string ApplicationUserId { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual List<MediaItem> MediaItems { get; set; }

        #endregion

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Album()
        {
        }

        #endregion
    }
}