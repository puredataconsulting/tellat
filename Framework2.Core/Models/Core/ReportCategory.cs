﻿using Framework2.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Core
{
    public class ReportCategory : BaseModel
    {

        #region Foreign Keys

        public int ReportID { get; set; }

        [ForeignKey("ReportID")]
        public virtual Report Report { get; set; }

        public int CategoryID { get; set; }

        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }
        #endregion

        #region Members
        public ReportCategory()
        {

        }
        #endregion
    }
}
