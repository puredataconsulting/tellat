﻿using Framework2.Core.Models.Base;

namespace Framework2.Core.Models.Core
{
    /// <summary>
    /// The log entry model, used to store logging messages in the database
    /// </summary>
    public class Log : BaseModel
    {
        public string Sender { get; set; }

        public string Severity { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        #region Members

        /// <summary>
        /// Constructor
        /// </summary>
        public Log()
        {
        }

        #endregion
    }
}