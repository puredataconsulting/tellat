﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Framework2.Core.Models.Base
{
    public class ModelActive : BaseModel
    {
        public DateTime LastActivity { get; set; }
    }
}
