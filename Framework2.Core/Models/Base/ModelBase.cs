﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Framework2.Core.Models.Base
{
    /// <summary>
    /// base model for all entity models
    /// </summary>
    [Serializable]
    public class BaseModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Default constructor, used to initialise the date and time field as the suggested 
        /// DatabaseGeneratedAttribute doesn't seem to work for DateTime fields
        /// </summary>
        protected BaseModel()
        {
            CreationDate = DateTime.Now;
        }
    }
}
