﻿using System;
using System.Data.Common;

namespace Framework2.Core.Context
{
    /// <summary>
    /// Singleton to manage the database context on a per request basis
    /// </summary>
    public class ContextManager : IContextManager, IDisposable
    {
        private FrameworkContext _context;
        public FrameworkContext Context
        {
            get { return _context ?? (_context = new FrameworkContext()); }
            set { _context = value; }
        }

        public DbTransaction Transaction { get; set; }

        public void Commit()
        {
            if (Context == null)
            {
                return;
            }

            Context.SaveChanges();
            Transaction.Commit();
        }

        public void Dispose()
        {
            if (Context == null)
            {
                return;
            }

            Transaction.Dispose();
            Transaction = null;
            Context.Dispose();
            Context = null;
        }
    }
}
