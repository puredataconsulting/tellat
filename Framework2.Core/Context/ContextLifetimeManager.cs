﻿using System.Web;
using Microsoft.Practices.Unity;

namespace Framework2.Core.Context
{
    /// <summary>
    /// A custom life time manager, used to store a database context for the life cycle of a
    /// single request
    /// </summary>
    public class ContextLifetimeManager : LifetimeManager
    {
        private readonly object _key = new object();

        /// <summary>
        /// Retrieve the database context from the HttpContext.Current.Items collection
        /// </summary>
        public override object GetValue()
        {
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(_key))
            {
                return HttpContext.Current.Items[_key];
            }
            
            return null;
        }

        /// <summary>
        /// Remove the database context from the HttpContext.Current.Items collection
        /// </summary>
        public override void RemoveValue()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items.Remove(_key);
            }
        }

        /// <summary>
        /// Add the database context from the HttpContext.Current.Items collection
        /// </summary>
        public override void SetValue(object newValue)
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Items[_key] = newValue;
            }
        }
    }
}
