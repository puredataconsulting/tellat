﻿namespace Framework2.Core.Context
{
    /// <summary>
    /// Singleton to manage the database context on a per request basis
    /// </summary>
    public interface IContextManager
    {
        FrameworkContext Context { get; }
    }
}
