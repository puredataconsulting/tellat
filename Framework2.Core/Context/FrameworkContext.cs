﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Framework2.Core.Models.Core;
using Microsoft.AspNet.Identity.EntityFramework;
using Framework2.Core.Migrations;

namespace Framework2.Core.Context
{
    public class FrameworkContext : IdentityDbContext<ApplicationUser>
    {
        public FrameworkContext() : base("FrameworkContext", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }

        static FrameworkContext()
        {
            // Set the database intializer which is run once during application start
            // Setting this null should kill any auto updating
            Database.SetInitializer<FrameworkContext>(null);
        }

        public static FrameworkContext Create()
        {
            return new FrameworkContext();
        }
    
        // Core
        public DbSet<Log> Logs { get; set; }
        
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<GroupMember> GroupMembers { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Favourite> Favourites { get; set; }

        // Reports are M:M with Category and Group
        public DbSet<ReportCategory> ReportCategories { get; set; }
        public DbSet<GroupReport> GroupReports { get; set; }

        // Media
        public DbSet<MediaItem> MediaItems { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Album> Albums { get; set;}
        
        // Tag tracking
        public DbSet<Tagging> Taggings { get; set; }
        public DbSet<Tag> Tags { get; set; }

        // Followers and Followed
        public DbSet<Following> Followings { get; set; }

        // Notifications (E.g Blah wants to join your group)
        public DbSet<Notification> Notifications { get; set; }

        /// <summary>
        /// Switch off cascading deletes
        /// </summary>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Switch off one-to-many cascading deletes
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            // This prevents a bogus relationship being built for the Followings table. The table should have two keys
            // into ApplcationUsers but by default gets 3 because of the way EF sees the world. This piece of magic
            // fixes that. 
            modelBuilder.Entity<Following>()
                            .HasRequired(x => x.ApplicationUser).WithMany()
                            .HasForeignKey(x => x.ApplicationUserId)
                            .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
                            .HasOptional(x => x.ApplicationUser)
                            .WithMany(x => x.Comments)
                            .HasForeignKey(x => x.ApplicationUserId);
        }
    }
}   
