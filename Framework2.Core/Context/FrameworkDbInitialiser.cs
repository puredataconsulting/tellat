﻿using Framework2.Core.Context;
using Framework2.Core.Models.Core;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// This brings in AddOrUpdate
using System.Data.Entity.Migrations;

namespace Framework2.Core.Migrations
{
    public class FrameworkDbInitialiser : DropCreateDatabaseIfModelChanges<FrameworkContext>
    {
        #region Construction
        public FrameworkDbInitialiser()
        {
            Initialise();
        }

        private void Initialise()
        {
        }
        #endregion

        #region Variables
        #endregion

        #region Properties
        #endregion

        #region Methods
        public static void InitialiseFrameworkforEF(FrameworkContext context)
        {
//            // Seed the category data
//            var categoryModels = new List<Category>
//            {
//                new Category { ThumbnailUri = "category-business@2x.jpg",      Title = "Business",      Description = ""},
//                new Category { ThumbnailUri = "category-culture@2x.jpg",       Title = "Culture",       Description = ""},
//                new Category { ThumbnailUri = "category-lifestyle@2x.jpg",     Title = "Lifestyle",     Description = ""},
//                new Category { ThumbnailUri = "category-money@2x.jpg",         Title = "Money",         Description = ""},
//                new Category { ThumbnailUri = "category-news@2x.jpg",          Title = "News",          Description = ""},
//                new Category { ThumbnailUri = "category-sports@2x.jpg",        Title = "Sports",        Description = ""},
//                new Category { ThumbnailUri = "category-technology@2x.jpg",    Title = "Technology",    Description = ""},
//                new Category { ThumbnailUri = "category-travel@2x.jpg",        Title = "Travel",        Description = ""},
//            };
//            categoryModels.ForEach(c => context.Categories.AddOrUpdate(c));

//#if LOADREPORTS
//            // Seed the report data
//            var reportModels = new List<Report>
//            {
//                // News
//                new Report { User = userModels[0], NViewings = 123, TotalRating = 321, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 01",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[1], NViewings = 234, TotalRating = 431, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 02",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[2], NViewings = 456, TotalRating = 121, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,    Title = "Tellat Sample Video 03",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[3], NViewings = 154, TotalRating = 241, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 04",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[4], NViewings = 546, TotalRating = 453, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 05",    IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Sport
//                new Report { User = userModels[0], NViewings = 123,  TotalRating = 321, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 06",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[1], NViewings = 234,  TotalRating = 431, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 07",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[2], NViewings = 456,  TotalRating = 121, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 08",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[3], NViewings = 154,  TotalRating = 241, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 09",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[4], NViewings = 546,  TotalRating = 453, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 10",    IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo."  },

//                // Fashion
//                new Report { User = userModels[0], NViewings = 123, TotalRating = 321, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 11",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[1], NViewings = 234, TotalRating = 431, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 12",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[2], NViewings = 456, TotalRating = 121, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 13",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[3], NViewings = 154, TotalRating = 241, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 14",    IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[4], NViewings = 546, TotalRating = 453, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 15",    IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Technology
//                new Report { User = userModels[0], NViewings = 123, TotalRating = 321, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 16",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[1], NViewings = 234, TotalRating = 431, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 17",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[2], NViewings = 456, TotalRating = 121, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 18",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[3], NViewings = 154, TotalRating = 241, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 19",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[4], NViewings = 546, TotalRating = 453, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 20",   IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Money
//                new Report { User = userModels[5], NViewings = 123,  TotalRating = 321, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 21",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[6], NViewings = 234,  TotalRating = 431, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 22",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[7], NViewings = 456,  TotalRating = 121, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 23",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[8], NViewings = 154,  TotalRating = 241, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 24",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[9], NViewings = 546,  TotalRating = 453, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 25",   IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Culture
//                new Report { User = userModels[5], NViewings = 123, TotalRating = 321, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 26",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[6], NViewings = 234, TotalRating = 431, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 27",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[7], NViewings = 456, TotalRating = 121, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 28",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[8], NViewings = 154, TotalRating = 241, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 29",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[9], NViewings = 546, TotalRating = 453, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 30",   IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Life & Style
//                new Report { User = userModels[5], NViewings = 123,  TotalRating = 321, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 31",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[6], NViewings = 234,  TotalRating = 431, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 32",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[7], NViewings = 456,  TotalRating = 121, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 33",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[8], NViewings = 154,  TotalRating = 241, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 34",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[9], NViewings = 546,  TotalRating = 453, NRatings = 100,   LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 35",   IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },

//                // Environment
//                new Report { User = userModels[5], NViewings = 123,  TotalRating = 321, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 36",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[6], NViewings = 234,  TotalRating = 431, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Incomplete, Scoping = Report.Scope.Public,   Title = "Tellat Sample Video 37",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[7], NViewings = 456,  TotalRating = 121, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Private,   Title = "Tellat Sample Video 38",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[8], NViewings = 154,  TotalRating = 241, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 39",   IsFeatured = false, Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//                new Report { User = userModels[9], NViewings = 546,  TotalRating = 453, NRatings = 100,  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  LifeCycleStage = Report.LifeCycle.Complete, Scoping = Report.Scope.Public,     Title = "Tellat Sample Video 40",   IsFeatured = true,  Text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vel tincidunt elit. Ut id elit sit amet purus interdum blandit. Aenean facilisis feugiat urna, id pretium purus feugiat ut. Vivamus augue lacus, mattis et dolor at, posuere molestie magna. Aenean ut aliquet dolor. Sed aliquet rhoncus aliquet. Aliquam luctus purus nec pellentesque commodo." },
//            };
//            reportModels.ForEach(c => context.Reports.AddOrUpdate(c));


//            Random rnd = new Random();

//            // Link categories to reports via ReportCategory instances
//            int nCategories = categoryModels.Count;
//            foreach (Report report in reportModels)
//            {
//                // Add 1 or 2 categories at random to each report
//                for (int i = 0; i < rnd.Next(1) + 1; i++)
//                {
//                    var rc = new ReportCategory { Report = report, Category = categoryModels[rnd.Next(nCategories)] };
//                    context.ReportCategories.AddOrUpdate(rc);
//                }
//            }

//            int iReport = 0;
//            var mediaModels = new List<MediaItem>
//            {
//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},
                
//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//                new MediaItem {Title = "Tellat Sample Video 01", Report = reportModels[iReport++], Uri = "video01.mp4",  ThumbnailUri = "thumbnail01.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 02", Report = reportModels[iReport++], Uri = "video02.mp4",  ThumbnailUri = "thumbnail02.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 03", Report = reportModels[iReport++], Uri = "video03.mp4",  ThumbnailUri = "thumbnail03.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 04", Report = reportModels[iReport++], Uri = "video04.mp4",  ThumbnailUri = "thumbnail04.jpg"},
//                new MediaItem {Title = "Tellat Sample Video 05", Report = reportModels[iReport++], Uri = "video05.mp4",  ThumbnailUri = "thumbnail05.jpg"},

//            };

//            mediaModels.ForEach(c => context.MediaItems.AddOrUpdate(c));

//            // Seed the group data
//            var groupModels = new List<Group>
//            {
//                new Group { LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Football",               Description = "All the latest football news"},
//                new Group { LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Sussex",                 Description = "Everything Sussex"},
//                new Group { LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Pole Position",          Description = "Welcome to Pole Position, the only channel on YouTube that has its foot to the floor ensuring you get coverage of the biggest names and events from the World of Motorsport."},
//                new Group { LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "On Air Video",           Description = "On Air Video, Inc. is a video production company specializing in instructional videos for the fine arts and the arts & crafts. We are also the creators of the Chubby Hubby Workout and Desk."},
//                new Group { LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "UFC Select",             Description = "The all new UFC Select channel provides access to full event replays, classic fights, and full episodes of your favorite UFC shows such as UFC Unleashed, The Best of Pride Fighting."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "The Syndicate Project",  Description = "I'm Syndicate, I love gaming & enjoy life to the maximum! Come take a peek into what i do! I love traveling the world and thanks to each and every amazing Subscriber I am where I am today!"},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Hollyoaks",              Description = "This is the official channel for Hollyoaks. Come see us... Channel 4: Weekdays, 6.30pm E4: Weekdays, 7pm"},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Speed Painting",         Description = "Speed painting is a method of painting or illustration, primarily a digital art form. It is not so much about the absolute speed or a limited time budget, as it is about the techniques involved."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Origami",                Description = "Origami is the traditional Japanese art of paper folding, which started in the 17th century AD at the latest and was popularized outside of Japan in the mid-1900s."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Silly Cyclists",         Description = "Silly Cyclists is a top 10 countdown of cyclists making mistakes on the roads around the world. As vulnerable road users we need to make sure we keep safe on the roads."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Animation",              Description = "Watch classic animations from Aardman's Oscar-Winning studio, including world-renowned Wallace & Gromit, Creature Comforts and the Amazing Adventures of Morph."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Liverpool FC",           Description = "Liverpool Football Club is an English Premier League football club based in Liverpool. Liverpool F.C. is one of the most successful clubs in England."},
//                new Group {  LastActivity = new DateTime(2014, 1, 12, 12, 0, 0),  Title = "Lazy Little Me",         Description = "Lazy Little Me is a hybrid of a video channel and a blog, both of which focus on makeup, fashion and hair advice, hosted by Emma Blackery and Cherry Wallis."},
//            };
//            groupModels.ForEach(c => context.Groups.AddOrUpdate(c));

//            // Seed the member models
//            var memberModels = new List<GroupMember>
//            {
//                // Every group must be owned
//                new GroupMember {User = userModels[0], Group = groupModels[0], IsAdministrator = true},
//                new GroupMember {User = userModels[0], Group = groupModels[1], IsAdministrator = true},
//                new GroupMember {User = userModels[1], Group = groupModels[2], IsAdministrator = true},
//                new GroupMember {User = userModels[1], Group = groupModels[3], IsAdministrator = true},
//                new GroupMember {User = userModels[2], Group = groupModels[4], IsAdministrator = true},
//                new GroupMember {User = userModels[2], Group = groupModels[5], IsAdministrator = true},
//                new GroupMember {User = userModels[3], Group = groupModels[6], IsAdministrator = true},
//                new GroupMember {User = userModels[3], Group = groupModels[7], IsAdministrator = true},
//                new GroupMember {User = userModels[4], Group = groupModels[8], IsAdministrator = true},
//                new GroupMember {User = userModels[4], Group = groupModels[9], IsAdministrator = true},
//                new GroupMember {User = userModels[5], Group = groupModels[10], IsAdministrator = true},
//                new GroupMember {User = userModels[5], Group = groupModels[11], IsAdministrator = true},
//                new GroupMember {User = userModels[6], Group = groupModels[12], IsAdministrator = true},
//                // 2nd user is non-admin member of the first two groups
//                new GroupMember {User = userModels[1], Group = groupModels[0], IsAdministrator = false},
//                new GroupMember {User = userModels[1], Group = groupModels[1], IsAdministrator = false},
//            };
//            memberModels.ForEach(c => context.GroupMembers.AddOrUpdate(c));

//            // Seed the comment models
//            int nUsers = userModels.Count;
//            foreach(Report report in reportModels)
//            {
//                context.Comments.AddOrUpdate(new Comment { Report = report, Text = "Stunning, beautiful, made me weep", User = userModels[rnd.Next(nUsers)] });
//                context.Comments.AddOrUpdate(new Comment { Report = report, Text = "Dude you're a master of the lens!", User = userModels[rnd.Next(nUsers)] });
//                context.Comments.AddOrUpdate(new Comment { Report = report, Text = "Don't give up the day job good buddy...", User = userModels[rnd.Next(nUsers)] });
//            }
            

//            // Seed the ratings/favourites

//            var favouriteModels = new List<Favourite>
//            {
//                new Favourite {User = userModels[0], Report = reportModels[0], Rating = 2 },
//                new Favourite {User = userModels[0], Report = reportModels[1], Rating = 3 },
//                new Favourite {User = userModels[0], Report = reportModels[2], Rating = 4 },
//                new Favourite {User = userModels[0], Report = reportModels[3], Rating = 5 },
//                new Favourite {User = userModels[1], Report = reportModels[0], Rating = 5 },
//                new Favourite {User = userModels[1], Report = reportModels[1], Rating = 4 },
//                new Favourite {User = userModels[1], Report = reportModels[2], Rating = 3 },
//                new Favourite {User = userModels[1], Report = reportModels[3], Rating = 2 },
//            };

//            favouriteModels.ForEach(c => context.Favourites.AddOrUpdate(c));

//            var albumModels = new List<Album>();
//            foreach(User user in userModels)
//            {
//                Album album = new Album() { User = user, Title = "Default Album", Description = "My best stuff" };
//                context.Albums.AddOrUpdate(album);
//            }

//            DateTime intervalBoundary = DateTime.Now - new TimeSpan(24, 0, 0);
//            // Wire up some tags
//            var tagModels = new List<Tag>
//            {
//                new Tag { Text = "Ukraine", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate = 0 , LastIntervalBoundary = intervalBoundary},
//                new Tag { Text = "Cameron", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate = 0f, LastIntervalBoundary = intervalBoundary},
//                new Tag { Text = "Devolution", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate = 0f, LastIntervalBoundary = intervalBoundary},
//                new Tag { Text = "Scotland", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate = 0f, LastIntervalBoundary = intervalBoundary},
//                new Tag { Text = "Germany", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate =0f, LastIntervalBoundary = intervalBoundary},
//                new Tag { Text = "Oil", NHitsInCurrentInterval = 0, NHitsInPreviousInterval = 0, HitRate = 0, LastIntervalBoundary = intervalBoundary}
//            };
//            tagModels.ForEach(c => context.Tags.AddOrUpdate(c));

//            // Link tags to reports via Tagging instances
//            int nTags = tagModels.Count;
//            foreach(Report report in reportModels)
//            {
//                // Add 3 tags at random to each report
//                for(int i = 0 ; i < 3 ; i++)
//                {
//                    var tagging = new Tagging { Report = report, Tag = tagModels[rnd.Next(nTags)] };
//                    context.Taggings.AddOrUpdate(tagging);
//                }
//            }

//#endif

//            var countryModels = new List<Country>()
//            {
//                new Country { IsoValue = "af", Name = "Afghanistan" },
//                new Country { IsoValue = "ax", Name = "Åland Islands" },
//                new Country { IsoValue = "al", Name = "Albania" },
//                new Country { IsoValue = "dz", Name = "Algeria" },
//                new Country { IsoValue = "as", Name = "American Samoa" },
//                new Country { IsoValue = "ad", Name = "Andorra" },
//                new Country { IsoValue = "ao", Name = "Angola" },
//                new Country { IsoValue = "ai", Name = "Anguilla" },
//                new Country { IsoValue = "aq", Name = "Antarctica" },
//                new Country { IsoValue = "ag", Name = "Antigua and Barbuda" },
//                new Country { IsoValue = "ar", Name = "Argentina" },
//                new Country { IsoValue = "am", Name = "Armenia" },
//                new Country { IsoValue = "aw", Name = "Aruba" },
//                new Country { IsoValue = "au", Name = "Australia" },
//                new Country { IsoValue = "at", Name = "Austria" },
//                new Country { IsoValue = "az", Name = "Azerbaijan" },
//                new Country { IsoValue = "bs", Name = "Bahamas" },
//                new Country { IsoValue = "bh", Name = "Bahrain" },
//                new Country { IsoValue = "bd", Name = "Bangladesh" },
//                new Country { IsoValue = "bb", Name = "Barbados" },
//                new Country { IsoValue = "by", Name = "Belarus" },
//                new Country { IsoValue = "be", Name = "Belgium" },
//                new Country { IsoValue = "bz", Name = "Belize" },
//                new Country { IsoValue = "bj", Name = "Benin" },
//                new Country { IsoValue = "bm", Name = "Bermuda" },
//                new Country { IsoValue = "bt", Name = "Bhutan" },
//                new Country { IsoValue = "bo", Name = "Bolivia" },
//                new Country { IsoValue = "ba", Name = "Bosnia and Herzegovina" },
//                new Country { IsoValue = "bw", Name = "Botswana" },
//                new Country { IsoValue = "bv", Name = "Bouvet Island" },
//                new Country { IsoValue = "br", Name = "Brazil" },
//                new Country { IsoValue = "io", Name = "British Indian Ocean Territory" },
//                new Country { IsoValue = "bn", Name = "Brunei Darussalam" },
//                new Country { IsoValue = "bg", Name = "Bulgaria" },
//                new Country { IsoValue = "bf", Name = "Burkina Faso" },
//                new Country { IsoValue = "bi", Name = "Burundi" },
//                new Country { IsoValue = "kh", Name = "Cambodia" },
//                new Country { IsoValue = "cm", Name = "Cameroon" },
//                new Country { IsoValue = "ca", Name = "Canada" },
//                new Country { IsoValue = "cv", Name = "Cape Verde" },
//                new Country { IsoValue = "ky", Name = "Cayman Islands" },
//                new Country { IsoValue = "cf", Name = "Central African Republic" },
//                new Country { IsoValue = "td", Name = "Chad" },
//                new Country { IsoValue = "cl", Name = "Chile" },
//                new Country { IsoValue = "cn", Name = "China" },
//                new Country { IsoValue = "cx", Name = "Christmas Island" },
//                new Country { IsoValue = "cc", Name = "Cocos (Keeling) Islands" },
//                new Country { IsoValue = "co", Name = "Colombia" },
//                new Country { IsoValue = "km", Name = "Comoros" },
//                new Country { IsoValue = "cg", Name = "Congo" },
//                new Country { IsoValue = "cd", Name = "Congo, The Democratic Republic of The" },
//                new Country { IsoValue = "ck", Name = "Cook Islands" },
//                new Country { IsoValue = "cr", Name = "Costa Rica" },
//                new Country { IsoValue = "ci", Name = "Cote D'ivoire" },
//                new Country { IsoValue = "hr", Name = "Croatia" },
//                new Country { IsoValue = "cu", Name = "Cuba" },
//                new Country { IsoValue = "cy", Name = "Cyprus" },
//                new Country { IsoValue = "cz", Name = "Czech Republic" },
//                new Country { IsoValue = "dk", Name = "Denmark" },
//                new Country { IsoValue = "dj", Name = "Djibouti" },
//                new Country { IsoValue = "dm", Name = "Dominica" },
//                new Country { IsoValue = "do", Name = "Dominican Republic" },
//                new Country { IsoValue = "ec", Name = "Ecuador" },
//                new Country { IsoValue = "eg", Name = "Egypt" },
//                new Country { IsoValue = "sv", Name = "El Salvador" },
//                new Country { IsoValue = "gq", Name = "Equatorial Guinea" },
//                new Country { IsoValue = "er", Name = "Eritrea" },
//                new Country { IsoValue = "ee", Name = "Estonia" },
//                new Country { IsoValue = "et", Name = "Ethiopia" },
//                new Country { IsoValue = "fk", Name = "Falkland Islands (Malvinas)" },
//                new Country { IsoValue = "fo", Name = "Faroe Islands" },
//                new Country { IsoValue = "fj", Name = "Fiji" },
//                new Country { IsoValue = "fi", Name = "Finland" },
//                new Country { IsoValue = "fr", Name = "France" },
//                new Country { IsoValue = "gf", Name = "French Guiana" },
//                new Country { IsoValue = "pf", Name = "French Polynesia" },
//                new Country { IsoValue = "tf", Name = "French Southern Territories" },
//                new Country { IsoValue = "ga", Name = "Gabon" },
//                new Country { IsoValue = "gm", Name = "Gambia" },
//                new Country { IsoValue = "ge", Name = "Georgia" },
//                new Country { IsoValue = "de", Name = "Germany" },
//                new Country { IsoValue = "gh", Name = "Ghana" },
//                new Country { IsoValue = "gi", Name = "Gibraltar" },
//                new Country { IsoValue = "gr", Name = "Greece" },
//                new Country { IsoValue = "gl", Name = "Greenland" },
//                new Country { IsoValue = "gd", Name = "Grenada" },
//                new Country { IsoValue = "gp", Name = "Guadeloupe" },
//                new Country { IsoValue = "gu", Name = "Guam" },
//                new Country { IsoValue = "gt", Name = "Guatemala" },
//                new Country { IsoValue = "gg", Name = "Guernsey" },
//                new Country { IsoValue = "gn", Name = "Guinea" },
//                new Country { IsoValue = "gw", Name = "Guinea-bissau" },
//                new Country { IsoValue = "gy", Name = "Guyana" },
//                new Country { IsoValue = "ht", Name = "Haiti" },
//                new Country { IsoValue = "hm", Name = "Heard Island and Mcdonald Islands" },
//                new Country { IsoValue = "va", Name = "Holy See (Vatican City State)" },
//                new Country { IsoValue = "hn", Name = "Honduras" },
//                new Country { IsoValue = "hk", Name = "Hong Kong" },
//                new Country { IsoValue = "hu", Name = "Hungary" },
//                new Country { IsoValue = "is", Name = "Iceland" },
//                new Country { IsoValue = "in", Name = "India" },
//                new Country { IsoValue = "id", Name = "Indonesia" },
//                new Country { IsoValue = "ir", Name = "Iran, Islamic Republic of" },
//                new Country { IsoValue = "iq", Name = "Iraq" },
//                new Country { IsoValue = "ie", Name = "Ireland" },
//                new Country { IsoValue = "im", Name = "Isle of Man" },
//                new Country { IsoValue = "il", Name = "Israel" },
//                new Country { IsoValue = "it", Name = "Italy" },
//                new Country { IsoValue = "jm", Name = "Jamaica" },
//                new Country { IsoValue = "jp", Name = "Japan" },
//                new Country { IsoValue = "je", Name = "Jersey" },
//                new Country { IsoValue = "jo", Name = "Jordan" },
//                new Country { IsoValue = "kz", Name = "Kazakhstan" },
//                new Country { IsoValue = "ke", Name = "Kenya" },
//                new Country { IsoValue = "ki", Name = "Kiribati" },
//                new Country { IsoValue = "kp", Name = "Korea, Democratic People's Republic of" },
//                new Country { IsoValue = "kr", Name = "Korea, Republic of" },
//                new Country { IsoValue = "kw", Name = "Kuwait" },
//                new Country { IsoValue = "kg", Name = "Kyrgyzstan" },
//                new Country { IsoValue = "la", Name = "Lao People's Democratic Republic" },
//                new Country { IsoValue = "lv", Name = "Latvia" },
//                new Country { IsoValue = "lb", Name = "Lebanon" },
//                new Country { IsoValue = "ls", Name = "Lesotho" },
//                new Country { IsoValue = "lr", Name = "Liberia" },
//                new Country { IsoValue = "ly", Name = "Libya" },
//                new Country { IsoValue = "li", Name = "Liechtenstein" },
//                new Country { IsoValue = "lt", Name = "Lithuania" },
//                new Country { IsoValue = "lu", Name = "Luxembourg" },
//                new Country { IsoValue = "mo", Name = "Macao" },
//                new Country { IsoValue = "mk", Name = "Macedonia, The Former Yugoslav Republic of" },
//                new Country { IsoValue = "mg", Name = "Madagascar" },
//                new Country { IsoValue = "mw", Name = "Malawi" },
//                new Country { IsoValue = "my", Name = "Malaysia" },
//                new Country { IsoValue = "mv", Name = "Maldives" },
//                new Country { IsoValue = "ml", Name = "Mali" },
//                new Country { IsoValue = "mt", Name = "Malta" },
//                new Country { IsoValue = "mh", Name = "Marshall Islands" },
//                new Country { IsoValue = "mq", Name = "Martinique" },
//                new Country { IsoValue = "mr", Name = "Mauritania" },
//                new Country { IsoValue = "mu", Name = "Mauritius" },
//                new Country { IsoValue = "yt", Name = "Mayotte" },
//                new Country { IsoValue = "mx", Name = "Mexico" },
//                new Country { IsoValue = "fm", Name = "Micronesia, Federated States of" },
//                new Country { IsoValue = "md", Name = "Moldova, Republic of" },
//                new Country { IsoValue = "mc", Name = "Monaco" },
//                new Country { IsoValue = "mn", Name = "Mongolia" },
//                new Country { IsoValue = "me", Name = "Montenegro" },
//                new Country { IsoValue = "ms", Name = "Montserrat" },
//                new Country { IsoValue = "ma", Name = "Morocco" },
//                new Country { IsoValue = "mz", Name = "Mozambique" },
//                new Country { IsoValue = "mm", Name = "Myanmar" },
//                new Country { IsoValue = "na", Name = "Namibia" },
//                new Country { IsoValue = "nr", Name = "Nauru" },
//                new Country { IsoValue = "np", Name = "Nepal" },
//                new Country { IsoValue = "nl", Name = "Netherlands" },
//                new Country { IsoValue = "an", Name = "Netherlands Antilles" },
//                new Country { IsoValue = "nc", Name = "New Caledonia" },
//                new Country { IsoValue = "nz", Name = "New Zealand" },
//                new Country { IsoValue = "ni", Name = "Nicaragua" },
//                new Country { IsoValue = "ne", Name = "Niger" },
//                new Country { IsoValue = "ng", Name = "Nigeria" },
//                new Country { IsoValue = "nu", Name = "Niue" },
//                new Country { IsoValue = "nf", Name = "Norfolk Island" },
//                new Country { IsoValue = "mp", Name = "Northern Mariana Islands" },
//                new Country { IsoValue = "no", Name = "Norway" },
//                new Country { IsoValue = "om", Name = "Oman" },
//                new Country { IsoValue = "pk", Name = "Pakistan" },
//                new Country { IsoValue = "pw", Name = "Palau" },
//                new Country { IsoValue = "ps", Name = "Palestinian Territory, Occupied" },
//                new Country { IsoValue = "pa", Name = "Panama" },
//                new Country { IsoValue = "pg", Name = "Papua New Guinea" },
//                new Country { IsoValue = "py", Name = "Paraguay" },
//                new Country { IsoValue = "pe", Name = "Peru" },
//                new Country { IsoValue = "ph", Name = "Philippines" },
//                new Country { IsoValue = "pn", Name = "Pitcairn" },
//                new Country { IsoValue = "pl", Name = "Poland" },
//                new Country { IsoValue = "pt", Name = "Portugal" },
//                new Country { IsoValue = "pr", Name = "Puerto Rico" },
//                new Country { IsoValue = "qa", Name = "Qatar" },
//                new Country { IsoValue = "re", Name = "Reunion" },
//                new Country { IsoValue = "ro", Name = "Romania" },
//                new Country { IsoValue = "ru", Name = "Russian Federation" },
//                new Country { IsoValue = "rw", Name = "Rwanda" },
//                new Country { IsoValue = "sh", Name = "Saint Helena" },
//                new Country { IsoValue = "kn", Name = "Saint Kitts and Nevis" },
//                new Country { IsoValue = "lc", Name = "Saint Lucia" },
//                new Country { IsoValue = "pm", Name = "Saint Pierre and Miquelon" },
//                new Country { IsoValue = "vc", Name = "Saint Vincent and The Grenadines" },
//                new Country { IsoValue = "ws", Name = "Samoa" },
//                new Country { IsoValue = "sm", Name = "San Marino" },
//                new Country { IsoValue = "st", Name = "Sao Tome and Principe" },
//                new Country { IsoValue = "sa", Name = "Saudi Arabia" },
//                new Country { IsoValue = "sn", Name = "Senegal" },
//                new Country { IsoValue = "rs", Name = "Serbia" },
//                new Country { IsoValue = "sc", Name = "Seychelles" },
//                new Country { IsoValue = "sl", Name = "Sierra Leone" },
//                new Country { IsoValue = "sg", Name = "Singapore" },
//                new Country { IsoValue = "sk", Name = "Slovakia" },
//                new Country { IsoValue = "si", Name = "Slovenia" },
//                new Country { IsoValue = "sb", Name = "Solomon Islands" },
//                new Country { IsoValue = "so", Name = "Somalia" },
//                new Country { IsoValue = "za", Name = "South Africa" },
//                new Country { IsoValue = "gs", Name = "South Georgia and The South Sandwich Islands" },
//                new Country { IsoValue = "es", Name = "Spain" },
//                new Country { IsoValue = "lk", Name = "Sri Lanka" },
//                new Country { IsoValue = "sd", Name = "Sudan" },
//                new Country { IsoValue = "sr", Name = "Suriname" },
//                new Country { IsoValue = "sj", Name = "Svalbard and Jan Mayen" },
//                new Country { IsoValue = "sz", Name = "Swaziland" },
//                new Country { IsoValue = "se", Name = "Sweden" },
//                new Country { IsoValue = "ch", Name = "Switzerland" },
//                new Country { IsoValue = "sy", Name = "Syrian Arab Republic" },
//                new Country { IsoValue = "tw", Name = "Taiwan, Province of China" },
//                new Country { IsoValue = "tj", Name = "Tajikistan" },
//                new Country { IsoValue = "tz", Name = "Tanzania, United Republic of" },
//                new Country { IsoValue = "th", Name = "Thailand" },
//                new Country { IsoValue = "tl", Name = "Timor-leste" },
//                new Country { IsoValue = "tg", Name = "Togo" },
//                new Country { IsoValue = "tk", Name = "Tokelau" },
//                new Country { IsoValue = "to", Name = "Tonga" },
//                new Country { IsoValue = "tt", Name = "Trinidad and Tobago" },
//                new Country { IsoValue = "tn", Name = "Tunisia" },
//                new Country { IsoValue = "tr", Name = "Turkey" },
//                new Country { IsoValue = "tm", Name = "Turkmenistan" },
//                new Country { IsoValue = "tc", Name = "Turks and Caicos Islands" },
//                new Country { IsoValue = "tv", Name = "Tuvalu" },
//                new Country { IsoValue = "ug", Name = "Uganda" },
//                new Country { IsoValue = "ua", Name = "Ukraine" },
//                new Country { IsoValue = "ae", Name = "United Arab Emirates" },
//                new Country { IsoValue = "gb", Name = "United Kingdom" },
//                new Country { IsoValue = "us", Name = "United States" },
//                new Country { IsoValue = "um", Name = "United States Minor Outlying Islands" },
//                new Country { IsoValue = "uy", Name = "Uruguay" },
//                new Country { IsoValue = "uz", Name = "Uzbekistan" },
//                new Country { IsoValue = "vu", Name = "Vanuatu" },
//                new Country { IsoValue = "ve", Name = "Venezuela" },
//                new Country { IsoValue = "vn", Name = "Viet Nam" },
//                new Country { IsoValue = "vg", Name = "Virgin Islands, British" },
//                new Country { IsoValue = "vi", Name = "Virgin Islands, U.S." },
//                new Country { IsoValue = "wf", Name = "Wallis and Futuna" },
//                new Country { IsoValue = "eh", Name = "Western Sahara" },
//                new Country { IsoValue = "ye", Name = "Yemen" },
//                new Country { IsoValue = "zm", Name = "Zambia" },
//                new Country { IsoValue = "zw", Name = "Zimbabwe" }
//            };
//            countryModels.ForEach(c => context.Countries.AddOrUpdate(c));

        }
        #endregion

        #region Overrides
        protected override void Seed(FrameworkContext context)
        {
            base.Seed(context);
            InitialiseFrameworkforEF(context);
        }
        #endregion

        #region Handlers
        #endregion
        
    }
}
