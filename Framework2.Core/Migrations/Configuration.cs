
/* Database migration commands for Package Manager Console:
 
 add-migration initial
 update-database
 
 */

namespace Framework2.Core.Migrations
{
    using Framework2.Core.Models.Core;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Framework2.Core.Context.FrameworkContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Framework2.Core.Context.FrameworkContext context)
        {
            // This is the trick to getting a debugger to run in this code! I love stackoverflow
            //if (System.Diagnostics.Debugger.IsAttached == false)System.Diagnostics.Debugger.Launch();

            string roleName = "Admin";
            string password = "Password@1234";

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }
            IdentityRole adminRole = roleManager.FindByName(roleName);

            var store = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(store);


            SeedAdminUser("RichardAdams", "richard@digicave.com", password, userManager, adminRole, context);
            SeedAdminUser("AlTredinnick", "al@digicave.com", password, userManager, adminRole, context);
            SeedAdminUser("MikePelton", "mike@digicave.com", password, userManager, adminRole, context);
            SeedAdminUser("MarkKeville", "mark@tellat.com", password, userManager, adminRole, context);
            SeedAdminUser("QuentinAustralia", "quentinseik@gmail.com", password, userManager, adminRole, context);
            SeedAdminUser("FelipColumbia", "afhurtado@gmail.com", password, userManager, adminRole, context);
            SeedAdminUser("ThomasEngland", "thomas@simplyicf.co.uk", password, userManager, adminRole, context);
            SeedAdminUser("VeraSerbia", "vera@libya-design.com", password, userManager, adminRole, context);
            SeedAdminUser("MohammedLibya", "it@allabina.com", password, userManager, adminRole, context);
            SeedAdminUser("VictoriaQuae", "infovictoriawilson@gmail.com", password, userManager, adminRole, context);
            SeedAdminUser("AndrewIreland", "andrewblanch9@hotmail.com", password, userManager, adminRole, context);
            SeedAdminUser("PatIreland", "pomalley@iol.ie", password, userManager, adminRole, context);

            // Default Categories
            AddOrUpdateCategory("category-business@2x.jpg", "Business", context);
            AddOrUpdateCategory("category-culture@2x.jpg", "Culture", context);
            AddOrUpdateCategory("category-lifestyle@2x.jpg", "Lifestyle", context);
            AddOrUpdateCategory("category-money@2x.jpg", "Money", context);
            AddOrUpdateCategory("category-news@2x.jpg", "News", context);
            AddOrUpdateCategory("category-sports@2x.jpg", "Sports", context);
            AddOrUpdateCategory("category-technology@2x.jpg", "Technology", context);
            AddOrUpdateCategory("category-travel@2x.jpg", "Travel", context);

            // Let's add countries too
            AddOrUpdateCountry("af", "Afghanistan", context);
            AddOrUpdateCountry("ax", "�land Islands", context);
            AddOrUpdateCountry("al", "Albania", context);
            AddOrUpdateCountry("dz", "Algeria", context);
            AddOrUpdateCountry("as", "American Samoa", context);
            AddOrUpdateCountry("ad", "Andorra", context);
            AddOrUpdateCountry("ao", "Angola", context);
            AddOrUpdateCountry("ai", "Anguilla", context);
            AddOrUpdateCountry("aq", "Antarctica", context);
            AddOrUpdateCountry("ag", "Antigua and Barbuda", context);
            AddOrUpdateCountry("ar", "Argentina", context);
            AddOrUpdateCountry("am", "Armenia", context);
            AddOrUpdateCountry("aw", "Aruba", context);
            AddOrUpdateCountry("au", "Australia", context);
            AddOrUpdateCountry("at", "Austria", context);
            AddOrUpdateCountry("az", "Azerbaijan", context);
            AddOrUpdateCountry("bs", "Bahamas", context);
            AddOrUpdateCountry("bh", "Bahrain", context);
            AddOrUpdateCountry("bd", "Bangladesh", context);
            AddOrUpdateCountry("bb", "Barbados", context);
            AddOrUpdateCountry("by", "Belarus", context);
            AddOrUpdateCountry("be", "Belgium", context);
            AddOrUpdateCountry("bz", "Belize", context);
            AddOrUpdateCountry("bj", "Benin", context);
            AddOrUpdateCountry("bm", "Bermuda", context);
            AddOrUpdateCountry("bt", "Bhutan", context);
            AddOrUpdateCountry("bo", "Bolivia", context);
            AddOrUpdateCountry("ba", "Bosnia and Herzegovina", context);
            AddOrUpdateCountry("bw", "Botswana", context);
            AddOrUpdateCountry("bv", "Bouvet Island", context);
            AddOrUpdateCountry("br", "Brazil", context);
            AddOrUpdateCountry("io", "British Indian Ocean Territory", context);
            AddOrUpdateCountry("bn", "Brunei Darussalam", context);
            AddOrUpdateCountry("bg", "Bulgaria", context);
            AddOrUpdateCountry("bf", "Burkina Faso", context);
            AddOrUpdateCountry("bi", "Burundi", context);
            AddOrUpdateCountry("kh", "Cambodia", context);
            AddOrUpdateCountry("cm", "Cameroon", context);
            AddOrUpdateCountry("ca", "Canada", context);
            AddOrUpdateCountry("cv", "Cape Verde", context);
            AddOrUpdateCountry("ky", "Cayman Islands", context);
            AddOrUpdateCountry("cf", "Central African Republic", context);
            AddOrUpdateCountry("td", "Chad", context);
            AddOrUpdateCountry("cl", "Chile", context);
            AddOrUpdateCountry("cn", "China", context);
            AddOrUpdateCountry("cx", "Christmas Island", context);
            AddOrUpdateCountry("cc", "Cocos (Keeling) Islands", context);
            AddOrUpdateCountry("co", "Colombia", context);
            AddOrUpdateCountry("km", "Comoros", context);
            AddOrUpdateCountry("cg", "Congo", context);
            AddOrUpdateCountry("cd", "Congo, The Democratic Republic of The", context);
            AddOrUpdateCountry("ck", "Cook Islands", context);
            AddOrUpdateCountry("cr", "Costa Rica", context);
            AddOrUpdateCountry("ci", "Cote D'ivoire", context);
            AddOrUpdateCountry("hr", "Croatia", context);
            AddOrUpdateCountry("cu", "Cuba", context);
            AddOrUpdateCountry("cy", "Cyprus", context);
            AddOrUpdateCountry("cz", "Czech Republic", context);
            AddOrUpdateCountry("dk", "Denmark", context);
            AddOrUpdateCountry("dj", "Djibouti", context);
            AddOrUpdateCountry("dm", "Dominica", context);
            AddOrUpdateCountry("do", "Dominican Republic", context);
            AddOrUpdateCountry("ec", "Ecuador", context);
            AddOrUpdateCountry("eg", "Egypt", context);
            AddOrUpdateCountry("sv", "El Salvador", context);
            AddOrUpdateCountry("gq", "Equatorial Guinea", context);
            AddOrUpdateCountry("er", "Eritrea", context);
            AddOrUpdateCountry("ee", "Estonia", context);
            AddOrUpdateCountry("et", "Ethiopia", context);
            AddOrUpdateCountry("fk", "Falkland Islands (Malvinas)", context);
            AddOrUpdateCountry("fo", "Faroe Islands", context);
            AddOrUpdateCountry("fj", "Fiji", context);
            AddOrUpdateCountry("fi", "Finland", context);
            AddOrUpdateCountry("fr", "France", context);
            AddOrUpdateCountry("gf", "French Guiana", context);
            AddOrUpdateCountry("pf", "French Polynesia", context);
            AddOrUpdateCountry("tf", "French Southern Territories", context);
            AddOrUpdateCountry("ga", "Gabon", context);
            AddOrUpdateCountry("gm", "Gambia", context);
            AddOrUpdateCountry("ge", "Georgia", context);
            AddOrUpdateCountry("de", "Germany", context);
            AddOrUpdateCountry("gh", "Ghana", context);
            AddOrUpdateCountry("gi", "Gibraltar", context);
            AddOrUpdateCountry("gr", "Greece", context);
            AddOrUpdateCountry("gl", "Greenland", context);
            AddOrUpdateCountry("gd", "Grenada", context);
            AddOrUpdateCountry("gp", "Guadeloupe", context);
            AddOrUpdateCountry("gu", "Guam", context);
            AddOrUpdateCountry("gt", "Guatemala", context);
            AddOrUpdateCountry("gg", "Guernsey", context);
            AddOrUpdateCountry("gn", "Guinea", context);
            AddOrUpdateCountry("gw", "Guinea-bissau", context);
            AddOrUpdateCountry("gy", "Guyana", context);
            AddOrUpdateCountry("ht", "Haiti", context);
            AddOrUpdateCountry("hm", "Heard Island and Mcdonald Islands", context);
            AddOrUpdateCountry("va", "Holy See (Vatican City State)", context);
            AddOrUpdateCountry("hn", "Honduras", context);
            AddOrUpdateCountry("hk", "Hong Kong", context);
            AddOrUpdateCountry("hu", "Hungary", context);
            AddOrUpdateCountry("is", "Iceland", context);
            AddOrUpdateCountry("in", "India", context);
            AddOrUpdateCountry("id", "Indonesia", context);
            AddOrUpdateCountry("ir", "Iran, Islamic Republic of", context);
            AddOrUpdateCountry("iq", "Iraq", context);
            AddOrUpdateCountry("ie", "Ireland", context);
            AddOrUpdateCountry("im", "Isle of Man", context);
            AddOrUpdateCountry("il", "Israel", context);
            AddOrUpdateCountry("it", "Italy", context);
            AddOrUpdateCountry("jm", "Jamaica", context);
            AddOrUpdateCountry("jp", "Japan", context);
            AddOrUpdateCountry("je", "Jersey", context);
            AddOrUpdateCountry("jo", "Jordan", context);
            AddOrUpdateCountry("kz", "Kazakhstan", context);
            AddOrUpdateCountry("ke", "Kenya", context);
            AddOrUpdateCountry("ki", "Kiribati", context);
            AddOrUpdateCountry("kp", "Korea, Democratic People's Republic of", context);
            AddOrUpdateCountry("kr", "Korea, Republic of", context);
            AddOrUpdateCountry("kw", "Kuwait", context);
            AddOrUpdateCountry("kg", "Kyrgyzstan", context);
            AddOrUpdateCountry("la", "Lao People's Democratic Republic", context);
            AddOrUpdateCountry("lv", "Latvia", context);
            AddOrUpdateCountry("lb", "Lebanon", context);
            AddOrUpdateCountry("ls", "Lesotho", context);
            AddOrUpdateCountry("lr", "Liberia", context);
            AddOrUpdateCountry("ly", "Libya", context);
            AddOrUpdateCountry("li", "Liechtenstein", context);
            AddOrUpdateCountry("lt", "Lithuania", context);
            AddOrUpdateCountry("lu", "Luxembourg", context);
            AddOrUpdateCountry("mo", "Macao", context);
            AddOrUpdateCountry("mk", "Macedonia, The Former Yugoslav Republic of", context);
            AddOrUpdateCountry("mg", "Madagascar", context);
            AddOrUpdateCountry("mw", "Malawi", context);
            AddOrUpdateCountry("my", "Malaysia", context);
            AddOrUpdateCountry("mv", "Maldives", context);
            AddOrUpdateCountry("ml", "Mali", context);
            AddOrUpdateCountry("mt", "Malta", context);
            AddOrUpdateCountry("mh", "Marshall Islands", context);
            AddOrUpdateCountry("mq", "Martinique", context);
            AddOrUpdateCountry("mr", "Mauritania", context);
            AddOrUpdateCountry("mu", "Mauritius", context);
            AddOrUpdateCountry("yt", "Mayotte", context);
            AddOrUpdateCountry("mx", "Mexico", context);
            AddOrUpdateCountry("fm", "Micronesia, Federated States of", context);
            AddOrUpdateCountry("md", "Moldova, Republic of", context);
            AddOrUpdateCountry("mc", "Monaco", context);
            AddOrUpdateCountry("mn", "Mongolia", context);
            AddOrUpdateCountry("me", "Montenegro", context);
            AddOrUpdateCountry("ms", "Montserrat", context);
            AddOrUpdateCountry("ma", "Morocco", context);
            AddOrUpdateCountry("mz", "Mozambique", context);
            AddOrUpdateCountry("mm", "Myanmar", context);
            AddOrUpdateCountry("na", "Namibia", context);
            AddOrUpdateCountry("nr", "Nauru", context);
            AddOrUpdateCountry("np", "Nepal", context);
            AddOrUpdateCountry("nl", "Netherlands", context);
            AddOrUpdateCountry("an", "Netherlands Antilles", context);
            AddOrUpdateCountry("nc", "New Caledonia", context);
            AddOrUpdateCountry("nz", "New Zealand", context);
            AddOrUpdateCountry("ni", "Nicaragua", context);
            AddOrUpdateCountry("ne", "Niger", context);
            AddOrUpdateCountry("ng", "Nigeria", context);
            AddOrUpdateCountry("nu", "Niue", context);
            AddOrUpdateCountry("nf", "Norfolk Island", context);
            AddOrUpdateCountry("mp", "Northern Mariana Islands", context);
            AddOrUpdateCountry("no", "Norway", context);
            AddOrUpdateCountry("om", "Oman", context);
            AddOrUpdateCountry("pk", "Pakistan", context);
            AddOrUpdateCountry("pw", "Palau", context);
            AddOrUpdateCountry("ps", "Palestinian Territory, Occupied", context);
            AddOrUpdateCountry("pa", "Panama", context);
            AddOrUpdateCountry("pg", "Papua New Guinea", context);
            AddOrUpdateCountry("py", "Paraguay", context);
            AddOrUpdateCountry("pe", "Peru", context);
            AddOrUpdateCountry("ph", "Philippines", context);
            AddOrUpdateCountry("pn", "Pitcairn", context);
            AddOrUpdateCountry("pl", "Poland", context);
            AddOrUpdateCountry("pt", "Portugal", context);
            AddOrUpdateCountry("pr", "Puerto Rico", context);
            AddOrUpdateCountry("qa", "Qatar", context);
            AddOrUpdateCountry("re", "Reunion", context);
            AddOrUpdateCountry("ro", "Romania", context);
            AddOrUpdateCountry("ru", "Russian Federation", context);
            AddOrUpdateCountry("rw", "Rwanda", context);
            AddOrUpdateCountry("sh", "Saint Helena", context);
            AddOrUpdateCountry("kn", "Saint Kitts and Nevis", context);
            AddOrUpdateCountry("lc", "Saint Lucia", context);
            AddOrUpdateCountry("pm", "Saint Pierre and Miquelon", context);
            AddOrUpdateCountry("vc", "Saint Vincent and The Grenadines", context);
            AddOrUpdateCountry("ws", "Samoa", context);
            AddOrUpdateCountry("sm", "San Marino", context);
            AddOrUpdateCountry("st", "Sao Tome and Principe", context);
            AddOrUpdateCountry("sa", "Saudi Arabia", context);
            AddOrUpdateCountry("sn", "Senegal", context);
            AddOrUpdateCountry("rs", "Serbia", context);
            AddOrUpdateCountry("sc", "Seychelles", context);
            AddOrUpdateCountry("sl", "Sierra Leone", context);
            AddOrUpdateCountry("sg", "Singapore", context);
            AddOrUpdateCountry("sk", "Slovakia", context);
            AddOrUpdateCountry("si", "Slovenia", context);
            AddOrUpdateCountry("sb", "Solomon Islands", context);
            AddOrUpdateCountry("so", "Somalia", context);
            AddOrUpdateCountry("za", "South Africa", context);
            AddOrUpdateCountry("gs", "South Georgia and The South Sandwich Islands", context);
            AddOrUpdateCountry("es", "Spain", context);
            AddOrUpdateCountry("lk", "Sri Lanka", context);
            AddOrUpdateCountry("sd", "Sudan", context);
            AddOrUpdateCountry("sr", "Suriname", context);
            AddOrUpdateCountry("sj", "Svalbard and Jan Mayen", context);
            AddOrUpdateCountry("sz", "Swaziland", context);
            AddOrUpdateCountry("se", "Sweden", context);
            AddOrUpdateCountry("ch", "Switzerland", context);
            AddOrUpdateCountry("sy", "Syrian Arab Republic", context);
            AddOrUpdateCountry("tw", "Taiwan, Province of China", context);
            AddOrUpdateCountry("tj", "Tajikistan", context);
            AddOrUpdateCountry("tz", "Tanzania, United Republic of", context);
            AddOrUpdateCountry("th", "Thailand", context);
            AddOrUpdateCountry("tl", "Timor-leste", context);
            AddOrUpdateCountry("tg", "Togo", context);
            AddOrUpdateCountry("tk", "Tokelau", context);
            AddOrUpdateCountry("to", "Tonga", context);
            AddOrUpdateCountry("tt", "Trinidad and Tobago", context);
            AddOrUpdateCountry("tn", "Tunisia", context);
            AddOrUpdateCountry("tr", "Turkey", context);
            AddOrUpdateCountry("tm", "Turkmenistan", context);
            AddOrUpdateCountry("tc", "Turks and Caicos Islands", context);
            AddOrUpdateCountry("tv", "Tuvalu", context);
            AddOrUpdateCountry("ug", "Uganda", context);
            AddOrUpdateCountry("ua", "Ukraine", context);
            AddOrUpdateCountry("ae", "United Arab Emirates", context);
            AddOrUpdateCountry("gb", "United Kingdom", context);
            AddOrUpdateCountry("us", "United States", context);
            AddOrUpdateCountry("um", "United States Minor Outlying Islands", context);
            AddOrUpdateCountry("uy", "Uruguay", context);
            AddOrUpdateCountry("uz", "Uzbekistan", context);
            AddOrUpdateCountry("vu", "Vanuatu", context);
            AddOrUpdateCountry("ve", "Venezuela", context);
            AddOrUpdateCountry("vn", "Viet Nam", context);
            AddOrUpdateCountry("vg", "Virgin Islands, British", context);
            AddOrUpdateCountry("vi", "Virgin Islands, U.S.", context);
            AddOrUpdateCountry("wf", "Wallis and Futuna", context);
            AddOrUpdateCountry("eh", "Western Sahara", context);
            AddOrUpdateCountry("ye", "Yemen", context);
            AddOrUpdateCountry("zm", "Zambia", context);
            AddOrUpdateCountry("zw", "Zimbabwe", context);
        }

        private static ApplicationUser SeedAdminUser(string userName, string email, string password, UserManager<ApplicationUser> userManager, IdentityRole adminRole,
            Framework2.Core.Context.FrameworkContext context)
        {
            var user = userManager.FindByEmail(email);
            if (user == null)
            {
                // If we don't set the email confirmed flag you can't do a password reset
                user = new ApplicationUser { UserName = userName, Email = email, EmailConfirmed = true};
                var result = userManager.Create(user, password);
                result = userManager.SetLockoutEnabled(user.Id, false);

                Console.Write("Added user" + userName);

                // Add a zeroth media item
                MediaItem mediaItem = new MediaItem { MediaType = MediaItem.MediaTypes.Content, ApplicationUserId = user.Id, Uri = "generic-person.png", ThumbnailUri = "profile-blue.png" };
                context.MediaItems.Add(mediaItem);
            }
            else
            {
                Console.Write("Found existing user" + userName);
            }
            // Add user admin to Role Admin if not already added
            IList<string> rolesForUser = userManager.GetRoles(user.Id);
            if (!rolesForUser.Contains(adminRole.Name))
            {
                var result = userManager.AddToRole(user.Id, adminRole.Name);
                Console.WriteLine(" ...added to admin role");
            }
            else
            {
                Console.WriteLine(" ...already in admin role");
            }

            return user;
        }

        private void AddOrUpdateCategory(string thumbnail, string title, Framework2.Core.Context.FrameworkContext context)
        {
            var cat = context.Categories.FirstOrDefault(c => c.Title == title);
            if (cat == null)
            {
                context.Categories.Add(new Category { ThumbnailUri = thumbnail, Title = title, Description = "" });
            }
        }

        private void AddOrUpdateCountry(string iso, string name, Framework2.Core.Context.FrameworkContext context)
        {
            var cat = context.Countries.FirstOrDefault(c => c.IsoValue == iso);
            if (cat == null)
            {
                context.Countries.Add(new Country { IsoValue = iso, Name = name });
            }
        }


    }
}
